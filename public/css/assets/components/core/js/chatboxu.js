lastReceived=0;

// Sign in and Out
function signInOut(id){
	$('input[name=receiver_id]').val(id);
	if (signInForm.userName.value=="" || signInForm.userName.value.indexOf(" ")>-1){
	alert("Not valid user name\nPlease make sure your user name didn't contains a space\nOr it's not empty.");
	signInForm.userName.focus();
	return false;
}

// Sign in
if (signInForm.signInButt.name=="signIn"){

	data="user=" + signInForm.userName.value +"&oper=Chat"
	Ajax_Send("POST","/users/dashboard/userinit",data,checkSignIn);
	return false;
}

// Sign out
if (signInForm.signInButt.name=="signOut"){
	data="user=" + signInForm.userName.value +"&oper=signout"
	Ajax_Send("POST","/users/dashboard/userinit",data,checkSignOut);
	return false;
}
}

// Sign in response
function checkSignIn(res){
	if(res=="userexist"){
	alert("The user name you typed is already exist\nPlease try another one");
	return false;
	}
	//updateInfo1();
	updateInterval=setInterval("updateInfo1()",3000);
}

// Sign out response
function checkSignOut(res){
	if(res=="usernotfound"){
		serverRes.innerHTML="Sign out error";
		res="signout"
	}
	if(res=="signout"){
		hideShow("hide")
		signInForm.userName.focus()
		clearInterval(updateInterval)
		serverRes.innerHTML="Sign out"
		return false;
	}
}

// Update info
function updateInfo1(){
	rec_id = $('input[name=receiver_id]').val();
	//Ajax_Send("POST","http://"+document.location.hostname+"/vendor/dashboard/userinit","",showUsers)
	Ajax_Send("POST","/users/dashboard/receive","lastreceived="+lastReceived+"&receiver="+rec_id,showMessages)
}

// update online users
function showUsers(res){
	console.log(res);
//usersOnLine.innerHTML=res
}

// Update messages view
function showMessages(res){
	//console.log(res, "itsok");
	if(res) {
		msgTmArr=res.split("<SRVTM>")
		lastReceived=msgTmArr[1]
		messages=document.createElement("span")
		messages.innerHTML=msgTmArr[0]
		chat_div.appendChild(messages)
		chat_div.scrollTop=chat_div.scrollHeight
	}
}

// Send message
 // function sendMessage(){
	// data="message="+messageForm.sendmsg.value+"&user="+signInForm.userName.value
 // 	Ajax_Send("POST","http://"+document.location.hostname+"/vendor/dashboard/send",data)
 // }
