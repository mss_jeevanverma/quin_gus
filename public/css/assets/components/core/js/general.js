// @mssjeevan custom js library to manupulate admin on ajax context
jQuery(document).ready(function($){

	$(".orders_table").find('.delete_order').each(function(index){
		
		$(this).click(function(){
			var answer = confirm("Are you sure to delete the order ?");
			if (answer) {				
				var id = $(this).attr('id');
				$.ajax({
					type: 'GET',
					url: 'http://'+document.location.hostname+'/za/public/admin/orders/index/od/format/html',
					data: {id:id}
				}).done(function(result) {
					window.location.reload();
				});
				
			}			
			
		});
		
	});
	
	$("select#parent_type").change(function(){				
		var id = $(this).val();
		$.ajax({
			type: 'POST',
			url: 'http://'+document.location.hostname+'/za/public/admin/gallery/parent/format/html',
			data: {id:id}
		}).done(function(result) {
			$("select#post_parent").html(result);
		});			
	});

	$("a#removeMessage").click(function(e) {
		var id = $(e.target).attr("data-id");
		$.ajax({
			type: 'GET',
			url: '/admin/messages/remove/format/html',
			data: {id:id}
		}).done(function(result) {
			if ( result.resp == 'success' ) {
				$("#"+id).hide();
			}
		});	
	});
	
	$("a.delete_graphic").click(function(e) {
		var id = $(this).attr("id");
		var answer = confirm("Are you sure you want to delete the record !!!!");
		if (answer) {
			$.ajax({
				type: 'GET',
				url: 'http://'+document.location.hostname+'/za/public/admin/media-uploader/delimage/format/html',
				data: {id:id}
			}).done(function(result) {
				$("#graphic_"+id).hide();
			});
		}	
	});

	$("#menuOrder").blur(function(e){
		var order = $(e.target).val();
		if (order) {
			$.ajax({
				type: 'GET',
				url: 'http://'+document.location.hostname+'/za/public/admin/gallery/checkorder/format/html',
				data: { order: order }
			}).done(function(result) {
				if ( result.resp == 'success' ) {
					$("#menuOrder").val("");
					$(".order-check").show();
					setTimeout(function(){
						$(".order-check").hide();
				        }, 3000);
				}
			});
		}
	});
	
	


	$('.check').click(function() {
		var check = $(this).attr("checked") ? "checked" : "unchecked";
    	if(check == "checked"){
    		var flag = 1;
    	}else{
			var flag = 0;
    	}
    	var media = $(this).attr('media-id');
        $.ajax({
			type: 'POST',
			url: '/admin/gallery/media-status/format/html',
			data: { id : media , flag : flag }
		}).done(function(result) {
			
		});
	});
});
