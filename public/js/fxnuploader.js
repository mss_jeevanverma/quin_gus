/********************************************************************************

* This script is brought to you by Vasplus Programming Blog

* Website: www.vasplus.info

* Email: info@vasplus.info

*********************************************************************************/



//This does the file Uploads

function attachFileupload( eID , location , fxnPath , filetype ){
	var  uploaded_files_location = location || base_url+"app/webroot/files/blog_uploads/blog_images";
	if($('#'+eID).length>0){ 
		$(document).ready(function()

		{

			//uploaded_files_location = location || base_url+"app/webroot/files/blog_uploads/blog_images";
			new AjaxUpload($('#'+eID ), 

			{

				action: fxnPath || base_url+'vendors/homeimgupload',

				name: eID,

				onSubmit: function(file, file_extensions)

				{ 

					$('.vpb_main_demo_wrapper').show(); //This is the main wrapper for the uploaded items which is hidden by default

					

					//Allowed file formats jpg|png|jpeg|gif|pdf|docx|doc|rtf|txt - You can add as more file formats or remove as you wish.

					if (!(file_extensions && /^(jpg|png|jpeg|gif)$/.test(file_extensions)))

					{

						//If file format is not allowed then, display an error message to the user

						$('#vpb_uploads_error_displayer').html('<div class="vpb_error_info" align="left">Sorry, you can only upload the following file formats: JPG, PNG and GIF. Thanks...</div>');

						return false;

					}

					else{
                      if(filetype=='logo'){
								showSpinner();
						  }else if(filetype=='banner'){
							  eID.replace("image","")%2==0? browseClass = 'right' : browseClass = 'left' ;
							 $('#'+eID).replaceWith('<div id="'+eID+'" class="'+browseClass+' uplading_image">Uploading <img src="'+base_url+'app/webroot/images/loadings.gif" align="absmiddle" /></div>');
						     return true;  
						  }else{
						  eID.replace("image","")%2==0? browseClass = 'right' : browseClass = 'left' ;
						  $('#'+eID).replaceWith('<div id="'+eID+'" class="'+browseClass+' uplading_image">Uploading <img src="'+base_url+'app/webroot/images/loadings.gif" align="absmiddle" /></div>');
						  return true;
						}
					}

				},

				onComplete: function(file, response)

				{  //alert(uploaded_files_location);
					// alert(JSON.parse(response).message);
					 if(JSON.parse(response).message === "file_uploaded_successfully"){

						$('#vpb_uploads_error_displayer').html(''); //Empty the error message box
						
						//Check the type of file uploaded and display it rightly on the screen to the user and that's cool man

						var type_of_file_uploaded = file.substring(file.lastIndexOf('.') + 1); //Get files extensions

					

						if(type_of_file_uploaded == "gif" || type_of_file_uploaded == "GIF" || type_of_file_uploaded == "JPEG" || type_of_file_uploaded == "jpeg" || type_of_file_uploaded == "jpg" || type_of_file_uploaded == "JPG" || type_of_file_uploaded == "png" || type_of_file_uploaded == "PNG")

						{
							var img_nm = JSON.parse(response).filename;
							if(filetype=='logo'){  fadeoutSpinner();
								$('div.uploadlogo_image img').attr('src',uploaded_files_location+'/'+img_nm); 
								$('#logoImg').val(img_nm); 
								$('#'+eID).replaceWith('<input type="button" id="rm_btn_upload" onclick="rm_unwanted_file(\''+eID+'\',\''+img_nm+'\',\''+base_url+'vendors/delCompInfo\',\'logo\');" class="logo_btns" value="Remove"/>');
								
							}else if(filetype=='banner'){
								// alert(uploaded_files_location);
							 var thumb_name = img_nm.substr(0, img_nm.lastIndexOf('.')) || img_nm;
                                                          thumb_name += "_thumb."+type_of_file_uploaded;
							eID.replace("image","")%2==0? browseClass = 'left' : browseClass = 'left' ;
							$('#'+eID).replaceWith('<img height="50px;" class="'+browseClass+'" id="rm_img_'+eID+'" src="'+uploaded_files_location+'/'+thumb_name+'" /><div onclick="rm_unwanted_file(\''+eID+'\',\''+img_nm+'\',\'\',\'banner\');" class="'+browseClass+' rm_ajax_upload_btn" id="rm_btn_'+eID+'">Remove</div>');
							
							$('#slide_'+eID).attr('src',uploaded_files_location+'/'+thumb_name);
						     
						  }else{
							var thumb_name = img_nm.substr(0, img_nm.lastIndexOf('.')) || img_nm;
                                                          thumb_name += "_thumb."+type_of_file_uploaded;
							eID.replace("image","")%2==0? browseClass = 'left' : browseClass = 'left' ;
							
							$('#'+eID).replaceWith('<img height="50px;" class="'+browseClass+'" id="rm_img_'+eID+'" src="'+uploaded_files_location+'/'+thumb_name+'" /><div onclick="rm_unwanted_file(\''+eID+'\',\''+img_nm+'\');" class="'+browseClass+' rm_ajax_upload_btn" id="rm_btn_'+eID+'">Remove</div>');

							}

						}

					} 

					else

					{

						$('#vpb_uploads_error_displayer').html('<div class="vpb_error_info" align="left">Sorry, your file upload was unsuccessful. Please reduce the size of your file and try again or contact this site admin to report this error message if the problem persist. Thanks...</div>');

					}
					

				}

			});

			

		});
	}

}


function attachVideoupload( eID , location , fxnPath , filetype ){
	var  uploaded_files_location= location || base_url+"app/webroot/files/vendor_videos";
	if($('#'+eID).length>0){ 
		$(document).ready(function(){
			//uploaded_files_location = location ;
			new AjaxUpload($('#'+eID ), 

			{

				action: fxnPath ,

				name: eID,

				onSubmit: function(file, file_extensions)

				{ 

					$('.vpb_main_demo_wrapper').show(); //This is the main wrapper for the uploaded items which is hidden by default

					//Allowed file formats jpg|png|jpeg|gif|pdf|docx|doc|rtf|txt - You can add as more file formats or remove as you wish.

					if (!(file_extensions && /^(mp4)$/.test(file_extensions)))

					{

						//If file format is not allowed then, display an error message to the user

						$('#vpb_uploads_error_displayer').html('<div class="vpb_error_info" align="left">Sorry, you can only upload the following file formats: JPG, PNG and GIF. Thanks...</div>');

						return false;

					}

					else{
                      
						 // eID.replace("image","")%2==0? browseClass = 'right' : browseClass = 'left' ;
						//  $('#'+eID).replaceWith('<div id="'+eID+'" class="'+browseClass+' uplading_image">Uploading <img src="'+base_url+'app/webroot/images/loadings.gif" align="absmiddle" /></div>');
						  return true;
						
					}

				},

				onComplete: function(file, response)

				{  
					// alert(JSON.parse(response).message);
					 if(JSON.parse(response).message === "file_uploaded_successfully"){

						$('#vpb_uploads_error_displayer').html(''); //Empty the error message box
						
						//Check the type of file uploaded and display it rightly on the screen to the user and that's cool man

						var type_of_file_uploaded = file.substring(file.lastIndexOf('.') + 1); //Get files extensions

					

						if(type_of_file_uploaded == "mp4" || type_of_file_uploaded == "GIF")

						{
							var img_nm = JSON.parse(response).filename;
			
			
							$('ul.browse_vdbtn li:first').html('<div class="flowplayer" id="div_vd_video1" data-swf="flowplayer.swf" ><video id="video_player1"><source type="video/'+type_of_file_uploaded+'" src="'+uploaded_files_location+'/'+img_nm+'"></video></div>');	
							
							bindFlowplayer( "flowplayer" );
							
							$('#'+eID).replaceWith('<input type="button" value="Remove" name="remove1" id="remove1" class="logo_btns" onclick="rm_unwanted_video(\''+eID+'\',\''+img_nm+'\',\'\',\'homevideo\');"/>');
							


						}

					} 

					else

					{

						$('#vpb_uploads_error_displayer').html('<div class="vpb_error_info" align="left">Sorry, your file upload was unsuccessful. Please reduce the size of your file and try again or contact this site admin to report this error message if the problem persist. Thanks...</div>');

					}
					

				}

			});

			

		});
	}

}

function bindFlowplayer( vidClass ){

      jQuery("."+vidClass ).flowplayer({ swf: "/swf/5.4.6/flowplayer.swf" });

}
