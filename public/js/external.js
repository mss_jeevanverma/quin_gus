$(function() {
	// Initialize toltip 
	$(".todoli").tipTip();
	$(".fancyTitle").tipTip();
	$(".pop").colorbox({width:"30%", height:"50%"});
	// Initialize spinner
	var spinner = $( "input[name=guest_spinner]" ).spinner();
	// alert(base_url);
	// close pop up 
	$(document).on("click","div#black_box_pop",function(){
		$(this).css('display','none');
		$('div.custom_popup').css('display','none');
	});
	
	// Initialize datepicker on calender icon
	$( ".hasDatePickerWithCal" ).datepicker({
			showOn: "button",
			buttonImage: base_url+"app/webroot/images/calendar.gif",
			buttonImageOnly: true,
			dateFormat: 'MM dd, yy',
			changeMonth: true,
			changeYear: true
	});
	
	
	// change datepicker calender icon
	$('.reqInfoForm img.ui-datepicker-trigger').attr('src',base_url+'app/webroot/images/calender_icon.png');
	
	// Change Category on vendor search page
	$('ul.vendor-headings li:first, ul.headings li:first').text($('select#show_categories:first').find(":selected").text());
	 $(document).on("change","select#show_categories",function(e){
		$('ul.vendor-headings li:first, ul.headings li:first').text($(this).find(":selected").text());
	 });
	
	// start and end time for appointment
	$('#stepExample1').timepicker({ 'step': 15 });
	$('#stepExample2').timepicker({ 'step': 60 });
	
	$('input[name="upload"]').click(function(){
		$('[name=file_to_upload]').click();
	})
	
});
$(document).ready(function(){ 
       jQuery.validator.addMethod("checkchosen1", function(value, element) {
	       var chosenid = jQuery(element).attr('id')+'_chosen';
	       var attr_indd = jQuery('#'+chosenid+' .chosen-drop ul.chosen-results li.result-selected').last().attr('data-option-array-index');
	       if(attr_indd !=0 && typeof attr_indd!= 'undefined'){
	       		return true;
	       }else{
	        	return false;
	       }
      }, "Select country.");
	  
	   jQuery.validator.addMethod("checkchosen2", function(value, element) {
	       var chosenid = jQuery(element).attr('id')+'_chosen';
	       var attr_indd = jQuery('#'+chosenid+' .chosen-drop ul.chosen-results li.result-selected').last().attr('data-option-array-index');
	       if(attr_indd !=0 && typeof attr_indd!= 'undefined'){
	       		return true;
	       }else{
	        	return false;
	       }
      }, "Select question.");
      
       jQuery.validator.addMethod("alphaonly", function(value, element) {
	  return this.optional(element) || /^[a-z]+$/i.test(value);
	}, "Letters only please");

	jQuery.validator.addMethod("alphaspaceonly", function(value, element) {
	  return this.optional(element) || /^[A-Za-z ]+$/.test(value);
	}, "Please enter Letters and spaces only.");
	
	jQuery.validator.addMethod("validphn", function(value, element) {
	  return this.optional(element) || /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(value);
	}, "Please enter valid Phone No.");
	
	jQuery.validator.addMethod("withoutProtocol", function(value, element) {
	  return this.optional(element) || /(http(s)?:\\)?([\w-]+\.)+[\w-]+[.com|.in|.org]+(\[\?%&=]*)?/.test(value);
	}, "Letters only please");

	
	$("#signup").validate({
			rules:{
					sex:{
						required:true
					},
					user_fname:{
						required:true,
						alphaonly : true
					},
					user_lname:{
					  required:true,
					  alphaonly : true
					 },  
					user_email:{
						required:true,	
						email: true,
					},
					user_cemail:{
						equalTo : "#user_email"
					},
					user_dob:{
						required:true
					},
					user_zip:{
						digits: true,
						minlength: 5,
						maxlength:5,
					},
					username:{
						required:true
					},
					password:{
						required:true,
						minlength: 5,
						maxlength:20
					},
					cpassword:{
						equalTo : "#password"
					},
					terms:{
						required : true
					}, 
					user_questionid:{
						checkchosen2: true
					},
					user_anwser:{
						required : true
					}	
				},
				messages:{
					sex:{
						required:'Select gender.'
					},
					user_fname:{
						required:'Enter first name.',
						alphaonly : 'Invalid name.',
					},
					user_lname:{
						required:'Enter Last name.',
						alphaonly : 'Invalid name.',
					},
					user_email:{
						required:'Enter email.',
						email:'Enter valid email.'
					},
					user_cemail:{
						equalTo:'Does not match.'
					},
					user_dob:{
						required:'Select date of birth.'
					},
					user_zip:{
						digits: 'Enter digits only.',
						minlength: 'Enter minimum 5 digits.',
						maxlength: 'Do not enter more than 5 digits.',
					},
					username:{
						required:'Enter username.'
					},
					password:{
						required:'Enter password.',
						minlength: 'Enter minimum 5 characters.',
						maxlength: 'Do not enter more than 20 characters.',
					},
					cpassword:{
						equalTo:'Password doesnt match .',
					},
					terms:{
						required:'Please accept terms and conditions.',
					}, 
					user_questionid:{
						required:'Select Question.',
					},
					user_anwser:{
						required:'Enter your answer.',
					}
				},
				ignore:''
				
			});

	$("#vaccount").validate({
			rules:{
					user_profession:{
						required:true,
					},
					user_occupation:{
					  required:true,
					 },  
					user_address:{
						required:true
					},
					vendor_business:{
						required:true
					},
					vendor_phone:{
						required : true,
						digits: true,
						minlength: 9,
						maxlength:10
					}
				},
				messages:{
					user_profession:{
						required:'Enter profession.',
					},
					user_occupation:{
						required:'Enter occupation.',
					},
					user_address:{
						required:'Enter address.',
					},
					user_business:{
						required:'Enter business.'
					},
					user_phone:{
						required:'Enter phone.',
						digits:'Enter numbers only.',
						minlength: 'Enter minimum 9 characters.',
						maxlength: 'Do not enter more than 10 characters.',
					}
				},
				//ignore:':active'
				
	});

	$("#ccinfo").validate({
			onkeyup: function(element) { //turn off onkeyup validation for ajax checking fields
				var element_id = jQuery(element).attr('id');
				if (this.settings.rules[element_id].onkeyup !== false) {
					jQuery.validator.defaults.onkeyup.apply(this, arguments);
				}
			},
			rules:{
					cc_type:{
						required:true,
					},
					cc_name:{
					  required:true,
					 },  
					cc_number:{
						required:true,
						creditcard:true,
						onkeyup:false
					},
					cc_ccv:{
						required:true
					},
					cc_expirymonth:{
						required:true
					},
					cc_expirymonth:{
						required : true
					}
				},
				messages:{
					cc_type:{
						required:'Select your card type.',
					},
					cc_name:{
						required:'Enter your name on card.',
					},
					cc_number:{
						required:'Enter your card number.',
						creditcard:'Invalid credit card.',
					},
					cc_ccv:{
						required:'Enter your ccv number.',
					},
					cc_expirymonth:{
						required:'Select month.'
					},
					cc_expirymonth:{
						required:'Select year.'
					}
				},
				//ignore:':active'
				
	});
	

	
	/** script for 'Bactk to Top' : Starts **/
	$("#back-top").hide();
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#back-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 800);
			return false;
		});
	});
	/** script for 'Back to Top' : Ends **/
	
	/** script for 'US formatization for phone number' : Starts **/
	   $("#user_phone").mask("999-999-9999",{placeholder:" "});
	   $("#cc_number").mask("9999-9999-9999-9999",{placeholder:" "});
	   $("input[name=cell_no]").mask("999-999-9999",{placeholder:" "});
	/** script for 'US formatization for phone number' : Ends **/
	
	/** script for hiding chosen select on 'review information for vendors' : Starts **/
	   $('.editInfoDrop').next().hide();
	/** script for hiding chosen select on 'review information for vendors' : Ends **/
	
	/** 'hide/show' on review information page : Starts **/
	$('#editInfoBtn').click(function(){
		 $('.editInfoDrop').next().show();
		 $('.reviewInfo').hide();
		 $('.editInfo').show();
	});
	
	$('#reviewInfoBtn').click(function(){
		 $('.editInfoDrop').next().hide();
		 $('.editInfo').hide();
		 $('.reviewInfo').show();
	});
	/** 'hide/show' on review information page : Ends **/
	
	/* $(".showPlanDetails1").click(function(){  
	   $(this).parent().children('.showPlan1').toggle();
		  if($(this).text()=="Less Details"){
		 $(this).text('More Details');
		   }else{
		 $(this).text('Less Details');
		   }
	}); */
	
	/*$('#themerPicker').colorpicker({
		displayIndicator: false
	});
	
	 $('#themerPicker').click(function(){
		$('#noIndColor').focus();
	}); 
	$('.evo-colorind-ff').css('display','none');*/
	
   $(".showPlanDetails1").click(function(){  
		 $('.showPlan1').toggle();
		 if($(".showPlanDetails1").text()=="Less Details"){
			$(".showPlanDetails1").text('More Details');
		 }else{
			$(".showPlanDetails1").text('Less Details');
		 }
   });
   $(".showPlanDetails2").click(function(){ 
		 $('.showPlan2').toggle();
		  if($(".showPlanDetails2").text()=="Less Details"){
			$(".showPlanDetails2").text('More Details');
		 }else{
			$(".showPlanDetails2").text('Less Details');
		 }
   });
   $(".showPlanDetails3").click(function(){ 
		 $('.showPlan3').toggle();
		  if($(".showPlanDetails3").text()=="Less Details"){
			$(".showPlanDetails3").text('More Details');
		 }else{
			$(".showPlanDetails3").text('Less Details');
		 }
   }); 
  
	$('#reviewForm').submit(function(){ //alert("hjh"); return false;
			$.ajax({
				type: "POST",
				async : false,
				dataType: 'json',
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(data){ 
					if(data){
						location.reload();
					}
				}
			});
		return false;  
	});
	
	$('#user_rateplan').chosen().change( function() {
		if($(this).val()=="vendor_choice"){
			$('input[name=plan_charges]').val('$39.95');
		}else if($(this).val()=="vendor_choice_plus"){
			$('input[name=plan_charges]').val('$69.95');
		}else if($(this).val()=="vendor_premium_choice"){
			$('input[name=plan_charges]').val('$349.99');
		}
	});

	$( ".hasDatePicker" ).datepicker({
			dateFormat: 'MM dd, yy',
			maxDate: 0,
			changeMonth: true,
			changeYear: true
	});
	
	$("#appointmentDate").datepicker({
		dateFormat: 'MM dd, yy',
		minDate: 0,
		changeMonth: true,
		changeYear: true
	});
	
	
	$(window).ready(function(){
		$('#ui-datepicker-div').attr('style','display:none');
	});
	
	/** show first div of user signup question **/
	$( "div.religion-wrapper" ).first().css('display','block');

	//$('.cpDiv').colorpicker({color:'#31859b'});
	
	//$('#nooIndColor').colorpicker({
	//	displayIndicator: false
	//});

	$(document).on('click', 'ul.back-skip li a.next', function(){
		var that = $(this);
		setTimeout(function(){
			if(that.css('display') == 'block') {
				$('.color_picker_new').wheelColorPicker({ layout: 'block', sliders: "whsvrgbap" });
			}
		}, 500);
	});
	$(document).ready(function() {
		$('.color_picker_new').wheelColorPicker({ layout: 'block', sliders: "wvp" });
	});
	
 $ ('#s7').cycle({
        fx: 'fade',
        speed: 500,
		timeout: 0, 
        next: '.next',
        prev: '.prev',
        pause: 1
    }); 
	 

	$('input[name=cancelTodo]').click(function(){
		$('#add_todo').css('display','none');
		$('#black_box_pop').css('display','none');
	});
	
	$('input[name=cancelNote]').click(function(){
		$('#add_notes').css('display','none');
		$('#black_box_pop').css('display','none');
	});
});

  function onAfter() {
      // $('#s7').removeAttr("style"); 
    } 

/** show states into drop-down based upon chosen country **/
 function load_options(id,index,elem_id){ 
	//var elem_id = elem_id;;
	if(id!=""){
		$.ajax({
			url: base_url+"vendors/process_data?index="+index+"&id="+id,
			complete: function(){$("#loading").hide();},
			success: function(data) { 
				$("#"+elem_id).html(data).chosen().trigger("chosen:updated");
			}
		})
	}
} 

/** show next div for user signup questions **/
function showNextQuestion(id,userid,input,step){ 
	if(input==2){
		var answer = $('.div3_example_result_'+id).html();
	}else if(input==3){
		var answer = $('#getSelection_'+id).val();
	}else if(input==4){ 
		var answer = $( "input[name=ceremony_"+id+"]:checked" ).val();
	}else if(input==1){ 
	var answer = $("#answer_"+id).val();
	}else if(input==5){
		var answer = $('#cpDiv_'+id).colorpicker("val");
	}
	if(answer){
		$.post(base_url+"Users/signup_steps",{'question_id':id,'user_id':userid,'input':input,'answer':answer,'step':step},function(data){
			//alert(data);
			if(data=="STOP"){ 
				window.location = base_url+"Users/welcome/"+userid;
			}
		});
	}
}

 function facebookLogin(){
  window.open('https://www.facebook.com/dialog/oauth?client_id=251860604974479&display=popup&redirect_uri='+base_url+'Users/facebook&scope=email,publish_stream,offline_access','','width=600, height=400');
 }
 
 function minimize(elem,key,classs){ 
	if(key=="calender"){
		$('div.todoDiv').fadeOut(1600);
		$('.bio .two-col-block').css('width','100%');
	}else{
	$('div.'+classs).fadeOut(1600 );
	}
	$(elem).next().css('display','block');
	$(elem).css('display','none');
 }
  function maximize(elem,key,classs){ 
	if(key=="calender"){ 
		$('.bio .two-col-block').css('width','49%');
		$('div.todoDiv').fadeIn(1600 );
	}else{
		$('div.'+classs).fadeIn(1600 );
	}
	$(elem).prev().css('display','block');
	$(elem).css('display','none');
 }
 // show popup
 function showPopup(act){
	//var ids="";
	if(act=="edit"){
		if($('ul#todolistUni li.active_list').length ==1){
			var id = $('ul#todolistUni li.active_list').attr('id').replace("todo_", "");
			if(id){
			$.post(base_url+"Ajax/add_todo/"+id,{'id':id},function(data){
				var data = JSON.parse(data);
				if(data){
					$('#ToDoTitle').val(data.title);
					$('#ToDoDescription').val(data.description);
					$('#ToDoId').val(data.id);
					$('#add_todo').css('display','block');
					$('#black_box_pop').css('display','block');
				}
			});
			}
		}else{
			alert("Please select one to-do to edit.");
		}
	}else if(act=="add"){
		$('#add_todo').css('display','block');
		$('#black_box_pop').css('display','block');
	}
 }
 
 
 $(document).on("click",".todoli .fa",function(e){
	var id = $(this).parent().attr('id').replace("todoDiv_", "");
	var elem = $(this);
	if($(this).hasClass( "fa-check-square-o" )){
		if($.active>0){ }else{	
		$.post(base_url+"Ajax/changeStatus/deactive",{'id':id},function(data){
				elem.next('i').show();
				elem.hide();
				elem.closest('.todoli').removeClass("green_list");
				elem.closest('.todoli').addClass("blue_list");
				elem.closest('.todoli').removeClass("active_list");
				});
		}
	}else if($(this).hasClass( "fa-square-o" )) {
		if($.active>0){ }else{	
		$.post(base_url+"Ajax/changeStatus/active",{'id':id},function(data){
					elem.prev('i').show();
					elem.hide();
					elem.closest('.todoli').removeClass("blue_list");
					elem.closest('.todoli').addClass("green_list");
					elem.closest('.todoli').removeClass("active_list");
				});
		}
	}
 });
 
$(document).on("click",".notes_li",function(e){
	if($(this).hasClass( "active_list" )){
		$( this ).removeClass("active_list");
	}else{
		$( this ).addClass("active_list");
	}
 });
 
 

 /** close button for closing request call back form **/
  $(document).on("click",".close_btn",function(e){
		var id = $(this).attr('id').replace("close_", "");
		$('#call_'+id).toggle('clip');
 });

 /** close popup **/
 $(document).on("click","a.quote_cancel",function(e){
		$('#black_box_pop').css('display','none');
		$('#notify_to_login').css('display','none');
 });
 
 
$(document).on("click",".todoli",function(e){
	if($(this).hasClass( "active_list" )){
		$( this ).removeClass("active_list");
	}else{
		$( this ).addClass("active_list");
	}
 });
 
 
 $('#addtodo').submit(function(){ //alert("hjh"); return false;
			$.ajax({
				type: "POST",
				async : false,
				dataType: 'json',
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(data){  
					if(data.message=="success"){
						$('#add_todo').css('display','none');
						$('#black_box_pop').css('display','none');
						if(data.action=="edit"){
							$("#todolistUni li.active_list").replaceWith("<li class='blue_list todoli' id='"+data.id+"' title='"+data.description+"'><i class='fa fa-check' style='display:none'></i><i class='pending'></i>"+data.title+"<span>"+data.todo_time+"</span></li>");
						}else{
							$("#todolistUni").prepend('<li title="'+data.description+'" id="todo_'+data.id+'" class="blue_list todoli"><div id="todoDiv_'+data.id+'"><i class="fa fa-check-square-o" style="display:none"></i><i class="fa fa-square-o"></i><b class="title_of_todo">'+data.title+'</b><span><font size="0.5px;">'+data.time+'<br>'+data.date+'</font></span></div><div class="showtd" id="showtd_'+data.id+'"> <input type="text" value="'+data.title+'" id="tit_'+data.id+'" name="todo_title"><textarea id="des_'+data.id+'" name="todo_desc">'+data.description+'</textarea></div><i id="edittodo_'+data.id+'" class="fa-edit" onclick="showEditTodo(this);" title="Edit"></i><i onclick="savetodo(this);" id="savetodo_'+data.id+'" style="" title="Save" class="fa-save"></i></li>');
							
							
						}
					}
				}
			});
		return false;  
	});
	
// function to delete selected entries
function deleteActiveEntries(){
	var ids="";
		if(confirm("Are you sure to delete?")){
			$('ul#todolistUni li.active_list').each(function() { 
				var newid = $(this).attr('id').replace("todo_", "");
				ids  += newid+",";
			});
			if(ids){
			$.post(base_url+"Ajax/deleteTodo",{'ids':ids},function(data){
				if(data==1){
					$('ul#todolistUni li.active_list').remove();
				}
			});
			}else{
				confirm("Please select an entry.");
			}
		}
	
}


// function to delete selected notes
function deleteActiveNotes(){
	var ids="";
		if(confirm("Are you sure to delete?")){
			$('ul#notelist li.active_list').each(function() { 
				var newid = $(this).attr('id').replace("note_", "");
				ids  += newid+",";
			});
			if(ids){
			$.post(base_url+"Ajax/deleteNotes",{'ids':ids},function(data){
				if(data==1){
					$('ul#notelist li.active_list').remove();
				}
			});
			}else{
				confirm("Please select an entry.");
			}
		}
	
}

 // show popup
function showNotePop(act){
	//var ids="";
	if(act=="edit"){
		if($('ul#todolistUni li.active_list').length ==1){
			var id = $('ul#todolistUni li.active_list').attr('id').replace("todo_", "");
			if(id){
			$.post(base_url+"Ajax/add_todo/"+id,{'id':id},function(data){
				var data = JSON.parse(data);
				if(data){
					$('#ToDoTitle').val(data.title);
					$('#ToDoDescription').val(data.description);
					$('#ToDoId').val(data.id);
					$('#add_todo').css('display','block');
					$('#black_box_pop').css('display','block');
				}
			});
			}
		}else{
			alert("Please select one to-do to edit.");
		}
	}else if(act=="add"){
		$('#add_notes').css('display','block');
		$('#black_box_pop').css('display','block');
	}
 }
 
 
  $('#addnotes').submit(function(){ //alert("hjh"); return false;
			$.ajax({
				type: "POST",
				async : false,
				dataType: 'json',
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(data){  
					if(data.message=="success"){
						$('#add_notes').css('display','none');
						$('#black_box_pop').css('display','none');
						if(data.action=="edit"){
							$("#todolistUni li.active_list").replaceWith("<li class='blue_list todoli' id='"+data.id+"' title='"+data.description+"'><i class='fa fa-check' style='display:none'></i><i class='pending'></i>"+data.title+"<span>"+data.todo_time+"</span></li>");
						}else{
							//$("#notelist").prepend("<li id='"+data.id+"'>"+data.note+"</li>");
							//alert(data.id);
							$("#notelist").prepend("<li id='note_"+data.id+"' class='notes_li'><i onclick='jj(this);' id='edit_"+data.id+"' class='fa-edit'></i><i onclick='save(this);' id='save_"+data.id+"' style='display:none;' class='fa-save'></i><div id='div_"+data.id+"' class='noteTxtDiv'>"+data.note+"</div><textarea style='display:none' id='txt_"+data.id+"'>"+data.note+"</textarea></li>");
						}
					}
				}
			});
		return false;  
	});
	
function jj(e){
	var id = $(e).attr('id').replace("edit_", "");
	$('#div_'+id).hide();
	$('#txt_'+id).show();
	$('#edit_'+id).hide();
	$('#save_'+id).show();
}
function showEditTodo(e){
	var id = $(e).attr('id').replace("edittodo_", "");
	$('#todoDiv_'+id).hide();
	$('#showtd_'+id).show();
	$('#edittodo_'+id).hide();
	$('#deltodo_'+id).hide();
	$('#savetodo_'+id).show();
	}
function save(e){
	var id = $(e).attr('id').replace("save_", "");
	var note  = $('#txt_'+id).val(); //alert(note);
	$.post(base_url+"Ajax/add_notes/"+id,{'note':note},function(data){
				var data = JSON.parse(data);
				if(data==1){
					$('#div_'+id).text(note);
					$('#div_'+id).show();
					$('#txt_'+id).hide();
					$('#edit_'+id).show();
					$('#save_'+id).hide();
					$('#note_'+id).removeClass('active_list');
				}
	});
}

function savetodo(e){
	var id = $(e).attr('id').replace("savetodo_", "");
	var title  = $('#tit_'+id).val(); //alert(note);
	var description  = $('#des_'+id).val(); //alert(note);
	$.post(base_url+"Ajax/add_todo/"+id,{'description':description,'title':title},function(data){
				var data = JSON.parse(data);
				if(data==1){
					$('#todoDiv_'+id).find('b').text(title);
					$('#todo_'+id).attr('title',title);
					$('#todoDiv_'+id).show();
					$('#showtd_'+id).hide();
					$('#edittodo_'+id).show();
					$('#savetodo_'+id).hide();
					$('#deltodo_'+id).show();
				}
	});
}

function requestBox(elem,key){ 
	  if($('input[name=user_id]').val() == ""){ 
			$('#black_box_pop').css('display','block');
			$('#notify_to_login').css('display','block');
		}else{
			if(key=="fav"){
				var vendor_id = $(elem).attr('class').replace("callli_", "");
				window.location = base_url+"Vendors/favourites/"+vendor_id;
			}else{
				var id = $(elem).attr('class').replace("callli_", "");
				$('#call_'+id).show('slow');
                                $('ul.input-actions').css('display','block');
                                $('p.call_back_name').css('display','block');
                                $('ul.reqInfoForm li:nth-child(4)').find('textarea').attr('placeholder','Notes');
                                $('.send_btn').val("Send Request");
				if(key=="call"){
					$('div#call_'+id+' .input-actions li:first input:radio[name=call]:nth(0)').attr('checked',true);
				}else if(key=="appointment"){
					$('div#call_'+id+' .input-actions li:nth-child(2) input:radio[name=call]:nth(0)').attr('checked',true);
				}else{
                                        $('ul.input-actions').css('display','none');
                                        $('p.call_back_name').css('display','none');
                                        $('ul.reqInfoForm li:nth-child(4)').find('textarea').attr('placeholder','Add Question');
                                        $('.send_btn').val("Send");
				}
		   }
		}

}


function callPopUp(elem,popupid){
	$('#black_box_pop').css('display','block');
	$('#'+popupid).css('display','block');
}

/** function to call vendor note popup **/
function addVendorNote(elem,vendorid){
	  if($(elem).attr('title')!="Add Note"){
		 $('#NoteNote').val($(elem).attr('title')); 
		 $('#NoteId').val($(elem).closest("li").attr('id'));
	  }else{
		 $('#NoteNote').val(''); 
		 $('#NoteId').val('');
	  }
	  $('div#black_box_pop').css('display','block');
      $('div#add_notes').css('display','block');
      $('#NoteVendorId').val(vendorid);
}

/** submit function to save vendor note  **/
  $('#addVendornotes').submit(function(){ //alert("hjh"); return false;
			$.ajax({
				type: "POST",
				async : false,
				dataType: 'json',
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(data){  
					if(data.message=="success"){ 
						$('#addvendornote_'+data.vendor_id+' img').attr('src',base_url+'app/webroot/images/note_icon_green.png');
						$('#addvendornote_'+data.vendor_id).removeAttr('title');
						$('#addvendornote_'+data.vendor_id).attr('title',data.note);
						//$('#addvendornote_'+data.vendor_id).parent( "li" ).addClass('todoli');
						//$(".todoli").tipTip();
						$('#add_notes').css('display','none');
						$('#black_box_pop').css('display','none');
					}
				}
			});
		return false;  
	});





// makeEditable( ele=elements that contains all elements , button=the button that was clicked and has to replaced by save now button, editsClass=class for elemnts that are to replaced by editable elements. )

function makeEditable( ele , editsClass ){
	if($(ele+" ."+editsClass).length > 0){
		$(ele+" ."+editsClass).each(function( k, v ) {
			var attribs = {};
			attribs = $(this).getAttributes();
			var allAtts = getStringFromAtts( attribs );
			$(this).replaceWith('<input type="text" '+allAtts+' />');
		});
	}
}

function makeUnEditable( ele , editsClass ){
	if($(ele+" ."+editsClass).length > 0){
		$(ele+" ."+editsClass).each(function( k, v ) {
			var attribs = {};
			attribs = $(this).getAttributes();
			var allAtts = getStringFromAtts( attribs );
			$(this).replaceWith('<span '+allAtts+' >'+($(this).val())+'</span>');
		});
	}
}

function getStringFromAtts( atts ){
	var str = '';
	$.each(atts, function( key, val ) {
	  str += " "+key+"=\""+val+"\" ";
	});
	return str;
}

(function($) {
    $.fn.getAttributes = function () {
        var elem = this, 
            attr = {};

        if(elem && elem.length) $.each(elem.get(0).attributes, function(v,n) { 
            n = n.nodeName||n.name;
            v = elem.attr(n); // relay on $.fn.attr, it makes some filtering and checks
            if(v != undefined && v !== false) attr[n] = v
        })

        return attr;
    }
})(jQuery);

function edit_account_tracks( ){
   		makeEditable( '#account_info', 'amEditable' );
 		$("#edit_prof_btn").hide();
		$("#save_prof_btn").show();
                $("#phone").mask("999-999-9999",{placeholder:" "});
                $("#altphone").mask("999-999-9999",{placeholder:" "});
                $("#fax_no").mask("999-999-9999",{placeholder:" "});
                $('#account_info').validate({
			rules:{
					company_name:{
						alphaspaceonly : true,
					},
					address1:{
					  alphaspacenumonly:true,
					 },
					 address2:{
					  alphaspacenumonly:true,
					 },
					phone:{
						validphn: true
					 },
					 altphone:{
						validphn: true
					 },
					fax_no:{
						validphn: true
					 },
					email:{
					  email:true,
					 },
					contact_name:{
					  alphaspaceonly:true,
					 },
					user_fname:{
					  alphaspaceonly:true,
					 },
					user_lname:{
					  alphaspaceonly:true,
					 }
				},
			messages:{
					company_name:{
						alphaspaceonly : 'Please Fill alphabats Only'
					},
					address1:{
					  alphaspacenumonly:'Please Fill alphabats Only'
					 },
					 address2:{
					  alphaspacenumonly:'Please Fill alphabats Only'
					 },
					phone:{
						validphn: 'Please enter Valid Phone No.',
						minlength: 'Enter phone no atleast 9 digits!',
						maxlength: 'Enter phone no atleast 10 digits!'
					 },
					altphone:{
						validphn: 'Please enter Valid Phone No.',
					 },
					fax_no:{
						validphn: 'Please enter Valid Fax No.',
					 },
					email:{
					  email: 'Please enter a valid Email'
					 },
					contact_name:{
					  alphaspaceonly:'Please Fill alphabats Only',
					 },
					 user_fname:{
					  alphaspaceonly:'Please Fill alphabats Only',
					 },
					 user_lname:{
					  alphaspaceonly:'Please Fill alphabats Only',
					 }
				}
			});
}

function save_account_tracks(){
 
	if($('#account_info input[type="text"].amEditable').length > 0){
	  if($('#account_info').valid()){
		
		$.ajax({
			url: base_url+"vendors/save_profile_vendor",
			type: "POST",
			data: {  
					 user_tbl_index : $("#user_tbl_index").val() ,
					 vendor_tbl_index : $("#vendor_tbl_index").val() ,
					 comp_name : $("#company_name").val() ,
					 contact_name : $("#contact_name").val() ,
					 user_fname : $("#user_fname").val() ,
					 user_lname : $("#user_lname").val() ,
					 phone : $("#phone").val(), 
					 fax : $("#fax_no").val(), 
					 email : $("#email").val(), 
					 user_phone : $("#altphone").val(), 
					 address1 : $("#address1").val(), 
					 address2 : $("#address2").val() 
					},
			dataType: "html",
			beforeSend: function(){
				showSpinner();
			},
			success: function(){
			  makeUnEditable( '#account_info', 'amEditable' );
			},
			complete: function( result ){
				// hideSpinner(); || 
                               // alert(result.toSource());
                                 if( result.responseText =='success' ){
                                    fadeoutSpinner();
                                 }else{
                                     showErrorHide();
                                    setTimeout(function(){  location.reload(); }, 5000);
                                    
                                }
			},
			error: function(){
				showErrorHide();
			}
		});


		$("#edit_prof_btn").show();
		$("#save_prof_btn").hide();
	  }else{
		
	  }
	  
	}else{
		
	}
}

//Favourite vendor page checkboxes events

$(document).on("click","ul.saved_vendor_list",function(e){
	if($(this).find('li:nth-child(5) input[type="checkbox"]').is(":checked")){
		$(this).find('li:nth-child(6) input[type="checkbox"]').prop('checked', true);
	}
});

//Favourite vendor page : remove vendor from user's fav list

function removeVendorFromFav(id){
	if(confirm("Are you sure to remove vendor from favourite list?")){
		$.post(base_url+'ajax/removeVendorFromUserFav/'+id,{},function(data){
				if(data==1){
					$('.vendorInfoDiv_'+id).remove();
					alert("Vendor has been removed from favourites successfully.");
				}
		})
	}
}

// function to close popup
function closepopup(popupid){
	$('#black_box_pop').css('display','none');
	$('#'+popupid).css('display','none');
}


$(document).ready(function(){
	attachFileupload( "upload", base_url+"app/webroot/files/vendor_company_logos/temp" , base_url+'vendors/CompanylogoUpload' , 'logo' );
	attachFileupload('image1', base_url+'app/webroot/files/vendors_home_images/' , base_url+'vendors/homeImgUpload' ,'banner');
	attachFileupload('image2', base_url+'app/webroot/files/vendors_home_images/' , base_url+'vendors/homeImgUpload' ,'banner');
	attachFileupload('image3', base_url+'app/webroot/files/vendors_home_images/' , base_url+'vendors/homeImgUpload' ,'banner');
	attachFileupload('image4', base_url+'app/webroot/files/vendors_home_images/' , base_url+'vendors/homeImgUpload' ,'banner');
	attachFileupload('image5', base_url+'app/webroot/files/vendors_home_images/' , base_url+'vendors/homeImgUpload' ,'banner');
	attachFileupload('image6', base_url+'app/webroot/files/vendors_home_images/' , base_url+'vendors/homeImgUpload' ,'banner');
	attachVideoupload('video1', base_url+'app/webroot/files/vendor_videos/' , base_url+'vendors/homeVideoUpload' ,'homevideo');

});

/** function to save Company Name Information  **/
 $('#CompanyNameInfo').submit(function(){ 
			$.ajax({
				type: "POST",
				async : false,
				dataType: 'json',
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(data){  //alert(data.comp_logo); return false;
					if(data.id){
						if(data.Act=="Logo"){
							$('span#logoName').html('<img class="logo" src="'+base_url+'app/webroot/files/vendor_company_logos/'+data.comp_logo+'"/>');
							$('.logo').attr('src',base_url+"app/webroot/files/vendor_company_logos/"+data.comp_logo);		
						}else if(data.Act=="Title"){
							$('span#logoName').html(data.comp_name);
						}
						$('#black_box_pop').css('display','none');
						$('#uploadLogo').css('display','none');
					}
				}
			});
		return false;  
});


function openPopUp(popupid){ 
	$("<div />").css({
		position: "absolute",
		left: 0,
		top: 0,
        width: $(document).width(), // width same as that of document
        height: $(document).height(), // height same as that of document
		zIndex: 1000000,  // to be on the safe side
		"background-color": 'rgba(0,0,0,0.8)'
	}).attr('id','custom-Overlay-pop').appendTo($("body"));
	$('#'+popupid).css({ display:'block',zIndex: 1000001 }).center();
	$('#'+popupid).prepend( '<a class="quote_cancel" onclick="javascript:closePopUp(\''+popupid+'\')" href="javascript:void(0)"></a>' );
}
function closePopUp( popupid ){ 
   $('#'+popupid).css({ display:'none',zIndex: 0});
   if($('#'+popupid).children('iframe').length >0 )
   {
	$('#'+popupid).children('iframe').remove();
   }
   $('#custom-Overlay-pop').remove();
}
/** function to save Company Name Information  **/


 function saveSecondaryCard(){ 
      var validator = $("#ccinfo1").validate({
			rules:{
					cc_type:{
						required:true,
					},
					cc_name:{
					  required:true,
                                           alphaspaceonly:true
					 },  
					cc_number:{
						required:true,
						creditcard:true,
						onkeyup:false
					},
					cc_ccv:{
						required:true,
						digits:true
					},
					cc_expirymonth:{
						checkchosen2:true
					},
					cc_expirymonth:{
						required : true
					}
				},
				messages:{
					cc_type:{
						required:'Select your card type.',
					},
					cc_name:{
						required:'Enter your name on card.',
                                                alphaspaceonly:'Enter characters and spaces only'
					},
					cc_number:{
						required:'Enter your card number.',
						creditcard:'Invalid credit card.',
					},
					cc_ccv:{
						required:'Enter your ccv number.', 
                                                digits:'Enter numbers only'
					},
					cc_expirymonth:{
						checkchosen2:'Select month.'
					},
					cc_expirymonth:{
						required:'Select year.'
					}
				},
				//ignore:':active'
				
	});
                
          if( validator.form() ){
                          $.ajax({
				type: "POST",
				async : false,
				dataType: 'json',
				url: $('.secondary_cc_info').attr('action'),
				data: $('.secondary_cc_info').serialize(),
				success: function(data){ 
                                         window.location = base_url+"Vendors/billing";
                                        return false;
				},
                               error:function(){
                                   
                              }
			});
		return false;  
          }else{
             return false;  
         }
}
/** open Add images popup for vendor homepage **/
function callAddImagePopUp(elem,popupid){
	$('#black_box_pop').css('display','block');
	$('#addBannerImages').css('display','block');
}

/** open Add images popup for vendor homepage **/
function callAddVideoPopUp(elem,popupid){
	$('#black_box_pop').css('display','block');
	$('#addVideos').css('display','block');
}
function goCCPrimary( cardID  ){
      
		$.ajax({
				type: "POST",
				async : false,
				dataType: 'json',
				url: base_url+'vendors/goCCPrimary',
				data: { 'id':cardID  },
				beforeSend:function(){
					showSpinner();
				},
				complete:function( result ){
						if( result.responseText =='success' ){
                                                        fadeoutSpinner();
						       window.location = base_url+"Vendors/billing";
							return true;
						 }else{
							showErrorHide( 'custom-Overlay', 'PleaseWait', ' Sorry, But not able to make it primary, please try again later !!' );
							return false;
						}
				},
				error:function( error ){
					// showErrorHide( 'custom-Overlay', 'PleaseWait', ' But not able to make it primary, please try again later !!' );
					return false;
				}
			}); 
}

/** Show All images for a Album **/
jQuery(document).ready(function(){
	checkFiles( 'album_imgs1' );
	checkFiles( 'album_imgs2' );
	checkFiles( 'album_imgs3' );
	checkVideoUrl();
	if( jQuery('.show_meAlbum').length > 0 ){
		jQuery('.show_meAlbum').click(function(e){
			e.preventDefault();
			if(!temp_session.hasOwnProperty('temp_vendor_id')){
				getFullAlbum( jQuery(this).attr('data-link-id') , jQuery(this).attr('data-link-title') );
			}else{
				getFullAlbumTemp( jQuery(this).attr('data-link-id') , jQuery(this).attr('data-link-title') );
			}
		});
	}
	if( jQuery('.show_meAlbum_img').length > 0 ){
		jQuery('.show_meAlbum_img').dblclick(function(e) {
			e.preventDefault();
			if(!temp_session.hasOwnProperty('temp_vendor_id')){
				getFullAlbum( jQuery(this).attr('data-link-id') , jQuery(this).attr('data-link-title') );
			}else{
				getFullAlbumTemp( jQuery(this).attr('data-link-id') , jQuery(this).attr('data-link-title') );
			}
		});
	}
});

function getFullAlbum( AlbumId, AlbumTitle ){
	
	$.ajax({
			type: "POST",
			async : false,
			dataType: 'json',
			url: base_url+'VendorPortfolios/AlbumImages',
			data: { 'AlbumId':AlbumId  },
			beforeSend:function(){
				initSpinnerFunction( );
				showSpinner();
			},
			complete:function( result ){
				var ResponseAlbum = JSON.parse(result.responseText );
				if( ResponseAlbum.status =='success' ){
					   fadeoutSpinner();
					   jQuery('#tabs1 .album_det').fadeOut("slow",function() {
							jQuery('#AlbumContainer').html( ResponseAlbum.HTML );
							bindFancyGallery();
							jQuery('#AlbumContainer').fadeIn("slow");
							jQuery('.album_content').removeClass("album_content");
							jQuery('.album_head_img').html(AlbumTitle).fadeIn("slow");
							// dragDropAlbumImgs();
						});
					return true;
				 }
				 else if( ResponseAlbum.status =='failure' ){
					showErrorHide( 'custom-Overlay', 'PleaseWait', ResponseAlbum.error );
					if(ResponseAlbum.error=="Sorry, No images in this Album"){
						add_to_album( AlbumId );
					}else{
						setTimout(function(){
							window.location=base_url+'VendorPortfolios/index';
						},2000);
					}
				 }else{
					showErrorHide( 'custom-Overlay', 'PleaseWait', ' Sorry, No Images avaialble right now, please try again later !!' );
					return false;
				}
			},
			error:function( error ){
				// showErrorHide( 'custom-Overlay', 'PleaseWait', ' Sorry, No Images avaialble right now, please try again later !!' );
				return false;
			}
	});
}

function getFullAlbumTemp( AlbumId, AlbumTitle ){
	
	$.extend( temp_session , { 'AlbumId':AlbumId  });
	$.ajax({
			type: "POST",
			async : false,
			dataType: 'json',
			url: base_url+'VendorPortfolios/AlbumImagesTemp',
			data: temp_session,
			beforeSend:function(){
				initSpinnerFunction( );
				showSpinner();
			},
			complete:function( result ){
				var ResponseAlbum = JSON.parse(result.responseText );
				if( ResponseAlbum.status =='success' ){
					   fadeoutSpinner();
					   jQuery('#tabs1 .album_det').fadeOut("slow",function() {
							jQuery('#tabs1 ul.isotope').hide();
							jQuery('#AlbumContainer').html( ResponseAlbum.HTML );
							
							bindFancyGallery( '.Albumimagedetails', 'media-gallery-album' );
							
							jQuery('#AlbumContainer').fadeIn("slow");
							jQuery('.album_content').removeClass("album_content");
							jQuery('.album_head_img').html(AlbumTitle).fadeIn("slow");
							// dragDropAlbumImgs();
							bindLikeToBtn();
						});
					return true;
				 }
				 else if( ResponseAlbum.status =='failure' ){
					showErrorHide( 'custom-Overlay', 'PleaseWait', ResponseAlbum.error );
					if(ResponseAlbum.error=="Sorry, No images in this Album"){
					
					}else{
						setTimout(function(){
							window.location=base_url+'VendorPortfolios/vendor/'+temp_session.temp_vendor_id;
						},2000);
					}
				 }else{
					showErrorHide( 'custom-Overlay', 'PleaseWait', ' Sorry, No Images avaialble right now, please try again later !!' );
					return false;
				}
			},
			error:function( error ){
				// showErrorHide( 'custom-Overlay', 'PleaseWait', ' Sorry, No Images avaialble right now, please try again later !!' );
				return false;
			}
	});
}

// Check if filetype has valid no of files 
// @Arguments: "fileName" - Name of input type file, "NoOfFiles" - No fo filesallowed
// @function checkFiles: 
function checkFiles( fileName , NoOfFiles ){
	jQuery(document).ready(function(){
		var FileId = '#'+fileName;
		if(jQuery(FileId).length > 0){
		
			var NoFiles = NoOfFiles || 5;
			jQuery( FileId ).change(function() {
				var FileType = this;
				var FileTypejQuery = jQuery( this );
				if( this.files.length > NoFiles ){
				
					if(jQuery('#inputerror-'+fileName).length > 0){
						jQuery('#inputerror-'+fileName).fadeIn("slow");
					}else{
						FileTypejQuery.after('<label class="error" id="inputerror-'+fileName+'" >Maximum '+NoFiles+' Files allowed at a time!!</label>');
					}
					
					jQuery( FileId ).closest('form').submit(function( event ) {
						if ( FileType.files.length > NoFiles  ) {
							if(jQuery('#inputerror-'+fileName).length > 0){
								jQuery('#inputerror-'+fileName).fadeIn("slow");
							}else{
								FileTypejQuery.after('<label class="error" id="inputerror-'+fileName+'" >Maximum '+NoFiles+' Files allowed at a time!!</label>');
							}
							event.preventDefault();
						}
						return;
					});
				}else{
					jQuery( '#inputerror-'+fileName ).fadeOut("slow");
				}
			});

		}
	});
}

function checkVideoUrl() {
	jQuery(document).ready(function(){
		jQuery("#video_url").on("change",function(){
			var url = jQuery( this ).val();
			var reg_you = new RegExp(/^.*(youtu.be\/|v\/|e\/|u\/\w+\/|embed\/|v=)([^#\&\?]*).*/);
			var reg_vim = new RegExp(/http:\/\/(www\.)?vimeo.com\/(\d+)($|\/)/);
			
			var matches_you = url.match(reg_you);
			var matches_vim= url.match(reg_vim);
			// alert(matches_you[2]);
			// alert(matches_vim[2]);
			if( jQuery.type( matches_you ) === "null" && jQuery.type( matches_vim ) === "null" ){
				bindVideoError();
			}else{
				if(jQuery('#inputerror-video_url').length > 0){
					jQuery('#inputerror-video_url').fadeOut("slow");
				}
			}
		});
	});
}

function bindVideoError(){
	if(jQuery('#inputerror-video_url').length > 0){
		jQuery('#inputerror-video_url').fadeIn("slow");
	}else{
		jQuery( "#video_url" ).after('<label class="error" id="inputerror-video_url" >Not a valid Youtube or Vimeo URL!!</label>');
	}
				
	jQuery( "#video-form" ).submit(function( event ) {
		var url = jQuery("#video_url").val();
		
		var reg_you = new RegExp(/^.*(youtu.be\/|v\/|e\/|u\/\w+\/|embed\/|v=)([^#\&\?]*).*/);
		var reg_vim = new RegExp(/http:\/\/(www\.)?vimeo.com\/(\d+)($|\/)/);
		
		var matches_you = url.match(reg_you);
		var matches_vim= url.match(reg_vim);
		
		if ( jQuery.type( matches_you ) === "null" && jQuery.type( matches_vim ) === "null" ){
			bindSmallErrorVideo();
			event.preventDefault();
		}
		return;
	});
}

function bindSmallErrorVideo(){
	if(jQuery('#inputerror-video_url').length > 0){
		jQuery('#inputerror-video_url').fadeIn("slow");
	}else{
		jQuery( "#video_url"  ).after('<label class="error" id="inputerror-video_url" >Not a valid Youtube or Vimeo URL!!</label>');
	}
}

function vimeoLoadingThumb(id){  
		// alert(id);
		var url = "http://vimeo.com/api/v2/video/" + id + ".json?callback=showThumb";

		var id_img = "#vimeo-" + id;

		var script = document.createElement( 'script' );
		script.type = 'text/javascript';
		script.src = url;

		$(id_img).before(script);
}


function showThumb(data){
    var id_img = "#vimeo-" + data[0].id;
    $(id_img).attr('src',data[0].thumbnail_large);
}


function appendVideoIframe( Div , IframeWidth, IframeHeight ){
	jQuery(document).ready(function(){
		if( jQuery('.videoIframePop').length > 0 ){
			jQuery('.videoIframePop').click(function(e){
				e.preventDefault();
				var href= jQuery( this ).attr('href');
				var Iwidth =  IframeWidth || 640;
				var Iheight =  IframeHeight || 480;
				ifrm = document.createElement("IFRAME");
				ifrm.setAttribute("src", href );
				ifrm.style.width = Iwidth+"px";
				ifrm.style.height = Iheight+"px";
				ifrm.style.padding="12px";
				ifrm.style.backgroundColor="#fff";
				jQuery('#'+Div).html('').append( ifrm );
				openPopUp( Div );
			});
		}
	});
}
appendVideoIframe( 'TheVideoPopUp' );


function portfolioValidations(){
	portfolioAlbumValidate('new_album');
	portfolioAlbumValidate('new_album1');
	jQuery('#video-form').validate({
		rules:{
				video_url:{
					required:true
				},
				video_title:{
					required:true,
					alphaspacenumonly : true
				}	
			},
			messages:{
				video_url:{
					required:'Enter Video URL.'
				},
				video_title:{
					required:'Enter video title.',
					alphaspacenumonly : 'Enter alphabats, No only.'
				}
			}
	});
}

function portfolioAlbumValidate( ID ){
	jQuery('#'+ID).validate({
		rules:{
			album_title:{
				required:true,
				alphaspacenumonly : true
			}	
		},
		messages:{
			album_title:{
				required:'Enter Album title.',
				alphaspacenumonly :'Enter alphabats, No only.'
			}
		}
	});
}

jQuery(document).ready(function(){
	portfolioValidations();
	setTimeout(function(){
		getTheActiveTab();
	},200);
	
});

function getTheActiveTab(){
	var hash = document.URL.substr(document.URL.indexOf('#')+1) ;
	if(hash != "" && document.URL.indexOf('#')!=-1){
	   var tabId = hash;
	   jQuery('.tab-content .fade.in.active').removeClass('fade in active');
	   var index = $('.tab-content #'+tabId).addClass('fade in active');
	   jQuery('ul.portfolio_navi.nav.nav-tabs li').removeClass('active');
	   jQuery('ul.portfolio_navi.nav.nav-tabs li').each(function(){
			if(jQuery( this ).children('a').attr('href')=='#'+tabId){
				jQuery( this ).addClass('active');
			}
	   });
	}
}
if(jQuery("ul.tab_labels li label").length > 0 && jQuery('.img_cont_main .existing').length > 0 &&  jQuery('.img_cont_main .new_albm').length > 0 ){
	jQuery("ul.tab_labels li label").click(function(){
		jQuery('.img_cont_main .existing').removeAttr('style');
		jQuery('.img_cont_main .new_albm').removeAttr('style');
	});
}

if( jQuery("ul.portfolio_navi.nav.nav-tabs li a#albums_link").length > 0 ){
	jQuery("ul.portfolio_navi.nav.nav-tabs li a#albums_link").click(function(){
		jQuery('#tabs1 .album_head_img').html('Album Name').hide();
		jQuery('#tabs1 #AlbumContainer').hide();
		jQuery('#tabs1 .album_det').show();
		jQuery('#tabs1 ul.isotope').show();
		jQuery('#tabs1').children("ul").addClass('album_content');
		dragDropAlbum();
	});
}


function add_to_album( albumId ){
	jQuery("#album_id").val( albumId );
	   jQuery('.tab-content .active').removeClass('fade in active');
	   var index = $('.tab-content #tabs2').addClass('fade in active');
	   jQuery('ul.portfolio_navi.nav.nav-tabs li').removeClass('active');
	   jQuery('ul.portfolio_navi.nav.nav-tabs li').each(function(){
			if( jQuery( this ).children('a').attr('href')=='#tabs2' ){
				jQuery( this ).addClass('active');
			}
	   });
	   jQuery('#tabs2 .img_cont_main .existing').show();
	   jQuery('#tabs2 .img_cont_main .new_albm').hide();
}


function processImgOp( imgID, fxnType, AlbumId, Albumtitle ){
	$.ajax({
			type: "POST",
			async : false,
			dataType: 'json',
			url: base_url+'VendorPortfolios/'+fxnType+'AlbumImg/'+imgID,
			data: { 'AlbumId':AlbumId  },
			beforeSend:function(){
				initSpinnerFunction( );
				showSpinner();
			},
			complete:function( result ){
				if( result.responseText =='success' ){
					   fadeoutSpinner();
					   getFullAlbum( AlbumId, Albumtitle );
				 }
				 else if( result.responseText =='failure' ){
					showErrorHide( 'custom-Overlay', 'PleaseWait', ' Sorry, Not able to '+fxnType+' the image right now, please try again later !!'  );
				 }else{
					showErrorHide( 'custom-Overlay', 'PleaseWait', ' Sorry, Not able to '+fxnType+' the image right now, please try again later !!' );
					return false;
				}
			},
			error:function( error ){
				// showErrorHide( 'custom-Overlay', 'PleaseWait', ' Sorry, Not able to '+fxnType+' the image right now, please try again later !!' );
				return false;
			}
	});
}

function dragDropAlbum(){
	jQuery( ".album_content" ).sortable({ 
		containment: "parent",
		stop: function( event, ui ) {
				$.ajax({
					type: "POST",
					async : false,
					url: base_url+'VendorPortfolios/orderAlbums',
					data: jQuery( ".album_content" ).sortable( "serialize" ),
					beforeSend:function(){
						initSpinnerFunction( );
						showSpinner();
					},
					success: function(data){ 
						
					},
					complete: function(data){
						if( data.responseText =='success' ){
							   fadeoutSpinner();
						 }
						 else if( data.responseText =='failure' ){
							showErrorHide( 'custom-Overlay', 'PleaseWait', ' Sorry, Not able to reorder the Album right now, please try again later !!'  );
						 }else{
							showErrorHide( 'custom-Overlay', 'PleaseWait', ' Sorry, Not able to reorder the Album right now, please try again later !!' );
							return false;
						}
					},
					error:function( error ){
						// showErrorHide( 'custom-Overlay', 'PleaseWait', ' Sorry, Not able to '+fxnType+' the image right now, please try again later !!' );
						return false;
					}
				});
			}
		});
	/* $( ".album_det" ).droppable({
		drop: function( event, ui ) {
			alert("hi");
		}
	}); */
}



/** Additional Jquery Fancybox plugin resize effect **/
(function ($, F) {
    F.transitions.resizeIn = function() {
        var previous = F.previous,
            current  = F.current,
            startPos = previous.wrap.stop(true).position(),
            endPos   = $.extend({opacity : 1}, current.pos);

        startPos.width  = previous.wrap.width();
        startPos.height = previous.wrap.height();

        previous.wrap.stop(true).trigger('onReset').remove();

        delete endPos.position;

        current.inner.hide();

        current.wrap.css(startPos).animate(endPos, {
            duration : current.nextSpeed,
            easing   : current.nextEasing,
            step     : F.transitions.step,
            complete : function() {
                F._afterZoomIn();

                current.inner.fadeIn("fast");
            }
        });
    };

}(jQuery, jQuery.fancybox));

/** Additional Jquery Fancybox plugin resize effect **/




function dragDropAlbumImgs(){
	jQuery( ".album_img_iso" ).sortable({ 
		containment: "parent",
		stop: function( event, ui ) {
				$.ajax({
					type: "POST",
					async : false,
					url: base_url+'VendorPortfolios/orderAlbumImgs',
					data: jQuery( ".album_img_iso" ).sortable( "serialize" ),
					beforeSend:function(){
						initSpinnerFunction( );
						showSpinner();
					},
					success: function(data){ 
						
					},
					complete: function(data){
						if( data.responseText =='success' ){
							   fadeoutSpinner();
						 }
						 else if( data.responseText =='failure' ){
							showErrorHide( 'custom-Overlay', 'PleaseWait', ' Sorry, Not able to reorder the Images right now, please try again later !!'  );
						 }else{
							showErrorHide( 'custom-Overlay', 'PleaseWait', ' Sorry, Not able to reorder the Images right now, please try again later !!' );
							return false;
						}
					},
					error:function( error ){
						// showErrorHide( 'custom-Overlay', 'PleaseWait', ' Sorry, Not able to '+fxnType+' the image right now, please try again later !!' );
						return false;
					}
				});
			}
		});
}

function bindFancyGallery( ImgSelector, galRelation){
	var imgSel = ImgSelector || '.imagedetails';
	var galRel = galRelation|| 'media-gallery';
	jQuery( imgSel ).attr('rel', galRel ).fancybox({
		openEffect : 'elastic',
		openSpeed  : 150,

		closeEffect : 'elastic',
		closeSpeed  : 150,
		
		nextMethod : 'resizeIn',
		nextSpeed  : 250,
		
		prevMethod : false,
		type: 'iframe',
		autoScale:false,
		autoDimensions:false,
		afterShow: function() {
		}
	});
	/* jQuery( imgSel ).each(function(){
		var hrefOfTheImage = jQuery( this ).attr('href');
		jQuery( this ).attr('rel', 'prettyPhoto['+galRel+']' ).attr('href', hrefOfTheImage+'?iframe=true&height=100%&width=100%' );
	});
	jQuery( "a[rel^='prettyPhoto["+galRel+"]']" ).prettyPhoto({ allow_resize: true,social_tools:false });  */
}
bindFancyGallery();

function resizeTheFancyBoxImage(){
	console.log('dfgh');
	jQuery( ".shrinkToFit" ).addClass('reSizedImg').removeClass('shrinkToFit');
	window.parent.resizeFancyboxInner();
}
function resizeFancyboxInner(){
	jQuery( ".fancybox-inner" ).addClass('reSizedImg');
	// jQuery( ".fancybox-inner" ).css({'width':'auto !important','height':'auto !important'});
	// jQuery( ".fancybox-type-iframe" ).css({'width':'auto !important', 'height':'auto !important'});
	$('#fancybox-content').removeAttr('style');
    $('#fancybox-wrap').removeAttr('style');
	$('.fancybox-iframe').contents().find('body').attr('id','reSizedImgBody');
	$.fancybox.update();
	$.fancybox.reposition();
}

function shareFB( URL , Title ){
	window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(URL)+'&t='+encodeURIComponent(Title),'sharer','toolbar=0,status=0,width=626,height=436');
}
function shareTW( URL , Title ){
	window.open("http://twitter.com/share?url="+encodeURIComponent(URL)+"&text="+encodeURIComponent(Title),'tweet','toolbar=0,status=0,width=626,height=436');
}

function isNumber (o) {
  return ! isNaN (o-0) && o !== null && o.replace(/^\s\s*/, '') !== "" && o !== false;
}

var likeBtnClicked;
function bindLikeToBtn(){
	
	jQuery('.like_btn_topright').unbind("click");
	
	jQuery('.like_btn_topright').click(function(){
		
		var $this = jQuery( this );
		likeBtnClicked= jQuery( this );
		
		var imageId = $this.attr('image-data');
			
		if(typeof imageId === "undefined")
		{
			return false;
		}

		var postData = { "action": "like" };
		if( jQuery( this ).hasClass("liked") ){
			postData = { "action": "unlike" };
		}
		
		$.extend( postData , { 'item_id':imageId,'type':'image'  } );
		
		jQuery.ajax({
			type: "POST",
			async : false,
			dataType: 'json',
			url: base_url+'VendorPortfolios/punchLike/',
			data: postData,
			beforeSend:function(){
				initSpinnerFunction( );
				showSpinner();
			},
			complete:function( result ){
				if( result.responseText =='needToLogin' ){
					fadeoutSpinner();
					letsLogin();
				 }else if( result.responseText =='failure' ){
					showErrorHide( 'custom-Overlay', 'PleaseWait', ' Sorry, Not able to like the image right now, please try again later !!'  );
				 }else if( isNumber(result.responseText) ){
					   fadeoutSpinner();
					   if( postData.action=="like" ){
							jQuery("span[image-data='"+imageId+"']").addClass('liked');
						}else{
							jQuery("span[image-data='"+imageId+"']").removeClass('liked');
						}
						
						if( likeBtnClicked.find('.like-count').length > 0 ){
							if( result.responseText > 0 )
								likeBtnClicked.find('.like-count').html( result.responseText );
							else if( result.responseText == 0 )
								likeBtnClicked.find('.like-count').html('');
						}else{
							if( result.responseText > 0 )
								likeBtnClicked.append('<i class="like-count">'+result.responseText+'</i>');
							else if( result.responseText == 0 )
								likeBtnClicked.append('<i class="like-count"></i>');
							
						}
						
						
						if(window.parent){
							if( postData.action=="like" ){
								window.parent.jQuery("span[image-data='"+imageId+"']").addClass('liked');
							}else{
									window.parent.jQuery("span[image-data='"+imageId+"']").removeClass('liked');
							}
							
							var sameBtnAsClicked = window.parent.jQuery("span[image-data='"+imageId+"']");
							
							if( sameBtnAsClicked.find('.like-count').length > 0 ){
								if( result.responseText > 0 )
									sameBtnAsClicked.find('.like-count').html( result.responseText );
								else if( result.responseText == 0 )
									sameBtnAsClicked.find('.like-count').html('');
							}else{
								if( result.responseText > 0 )
									sameBtnAsClicked.append('<i class="like-count">'+result.responseText+'</i>');
								else if( result.responseText == 0 )
									sameBtnAsClicked.append('<i class="like-count"></i>');
								
							}
							
						}
						
				 }else{
					showErrorHide( 'custom-Overlay', 'PleaseWait', ' Sorry, Not able to like the image right now, please try again later !!' );
					return false;
				}
			},
			error:function( error ){
				// showErrorHide( 'custom-Overlay', 'PleaseWait', ' Sorry, Not able to '+fxnType+' the image right now, please try again later !!' );
				return false;
			}
		});
	});
}

function openIPopUp( URL ){ 
	$("<div />").css({
		position: "absolute",
		left: 0,
		top: 0,
        width: $(document).width(), // width same as that of document
        height: $(document).height(), // height same as that of document
		zIndex: 1000000,  // to be on the safe side
		"background-color": 'rgba(0,0,0,0.8)'
	}).attr('id','custom-Overlay-pop').appendTo($("body"));
	var TheId =jQuery.now();
	jQuery('<div />', {
		id: TheId,
		height:420,
		width:620
	}).appendTo('#custom-Overlay-pop').addClass("outer-Iframe").css({  padding: '20px' ,'background-color' : '#fff',display:'block',zIndex: 1000001 });
	
	if( $(document).width() < 580){
		var divWidt = $(document).width()-10;
		var divHigt = divWidt * 0.75;
	}else{
		var divWidt = 580;
		var divHigt = 400;
	}
		
	$('<iframe />', {
		id: TheId+'-iframe',
		src: URL,
		height:divHigt,
		width:divWidt
	}).appendTo('#'+TheId).css({ 'background-color' : '#fff' })
	jQuery('#'+TheId).center();
	jQuery('#'+TheId).prepend( '<a class="quote_cancel" onclick="javascript:closePopUp(\''+TheId+'\')" href="javascript:void(0)"></a>' );
}

function letsLogin(){
	openIPopUp( base_url+'Login/' );
}
function closeTheLoginBox(){
	jQuery("#custom-Overlay-pop").remove();
	likeBtnClicked.trigger( "click" );
}

/*

jQuery(".fbs_share").click(function() {
	TheImg = jQuery( this ).closest(".hover_placeho.hover_placeho2").siblings("img");
	u=TheImg.attr('src');
	// t=document.title;
	t=TheImg.attr('alt');
	window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t),'sharer','toolbar=0,status=0,width=626,height=436');
	return false;
});
jQuery(".fbs_tweet").click(function() {
	TheImg = jQuery( this ).closest(".hover_placeho.hover_placeho2").siblings("img");
	u=TheImg.attr('src');
	// t=document.title;
	t=TheImg.attr('alt');

	window.open("http://twitter.com/share?url="+encodeURIComponent(u)+"&text="+encodeURIComponent(t), 'tweet', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=500,height=500,left = 710,top = 290');
	return false;
});

*/

jQuery(window).resize(function(){
	jQuery('#custom-Overlay-pop').css({
		position: "absolute",
		left: 0,
		top: 0,
		padding: '2em',
        width: $(document).width(), // width same as that of document
        height: $(document).height(), // height same as that of document
		zIndex: 1000000,  // to be on the safe side
		"background-color": 'rgba(0,0,0,0.8)'
	});
	if( $(document).width() < 580)
		var divWidt = $(document).width()-10;
	else
		var divWidt = 580;

	var divHigt = divWidt * 0.75;
	jQuery(".outer-Iframe").css({  width: divWidt, height: divHigt }).center();
});




var followBtnClicked;
function bindFollowToBtn(){
	
	jQuery('.follow_btn').unbind("click");
	
	jQuery('.follow_btn').click(function(){
		
		var $this = jQuery( this );
		followBtnClicked= jQuery( this );
		
		var imageId = $this.attr('image-data');
			
		if(typeof imageId === "undefined")
		{
			return false;
		}

		var postData = { "action": "follow" };
		if( jQuery( this ).hasClass("followed") ){
			postData = { "action": "unfollow" };
		}
		
		$.extend( postData , { 'item_id':imageId,'type':'image'  } );
		
		jQuery.ajax({
			type: "POST",
			async : false,
			dataType: 'json',
			url: base_url+'VendorPortfolios/makeMeFollow/',
			data: postData,
			beforeSend:function(){
				initSpinnerFunction( );
				showSpinner();
			},
			complete:function( result ){
				if( result.responseText =='needToLogin' ){
					fadeoutSpinner();
					letsLogin();
				 }else if( result.responseText =='failure' ){
					showErrorHide( 'custom-Overlay', 'PleaseWait', ' Sorry, Not able to Follow the image right now, please try again later !!'  );
				 }else if( isNumber(result.responseText) ){
					   fadeoutSpinner();
					   if( postData.action=="follow" ){
							jQuery("span[image-data='"+imageId+"'].follow_btn").addClass('followed');
						}else{
							jQuery("span[image-data='"+imageId+"'].follow_btn").removeClass('followed');
						}
						
						if( followBtnClicked.find('.follow-count').length > 0 ){
							if( result.responseText > 0 )
								followBtnClicked.find('.follow-count').html( result.responseText );
							else if( result.responseText == 0 )
								followBtnClicked.find('.follow-count').html('');
						}else{
							if( result.responseText > 0 )
								followBtnClicked.append('<i class="follow-count">'+result.responseText+'</i>');
							else if( result.responseText == 0 )
								followBtnClicked.append('<i class="follow-count"></i>');
							
						}
						
						
						if(window.parent){
							if( postData.action=="follow" ){
								window.parent.jQuery("span[image-data='"+imageId+"'].follow_btn").addClass('followed');
							}else{
									window.parent.jQuery("span[image-data='"+imageId+"'].follow_btn").removeClass('followed');
							}
							
							var sameBtnAsClicked = window.parent.jQuery("span[image-data='"+imageId+"'].follow_btn");
							
							if( sameBtnAsClicked.find('.follow-count').length > 0 ){
								if( result.responseText > 0 )
									sameBtnAsClicked.find('.follow-count').html( result.responseText );
								else if( result.responseText == 0 )
									sameBtnAsClicked.find('.follow-count').html('');
							}else{
								if( result.responseText > 0 )
									sameBtnAsClicked.append('<i class="follow-count">'+result.responseText+'</i>');
								else if( result.responseText == 0 )
									sameBtnAsClicked.append('<i class="follow-count"></i>');
								
							}
							
						}
						
				 }else{
					showErrorHide( 'custom-Overlay', 'PleaseWait', ' Sorry, Not able to follow the image right now, please try again later !!' );
					return false;
				}
			},
			error:function( error ){
				// showErrorHide( 'custom-Overlay', 'PleaseWait', ' Sorry, Not able to '+fxnType+' the image right now, please try again later !!' );
				return false;
			}
		});
	});
}

			var SentMail;
jQuery(document).ready(function(){
	bindLikeToBtn();
	bindFollowToBtn();
	
	jQuery('#showMailFields').click(function(e){
		e.preventDefault();
		var cssTop = parseInt(jQuery( this ).offset().top) + 65;
		var cssleft = jQuery( this ).offset().left;
		jQuery('#attachment_mail').css({ 'top' : cssTop,'left' : cssleft, 'z-index' : '999999999' }).slideDown();
		jQuery('#sendImageMailForm').show();
		jQuery('#attachment_mail').children('[type=email]').focus();
	});
	jQuery('#sendImageMailForm').submit(function(){ 
			var AttachmentDiv =jQuery('#attachment_mail')
			jQuery('#sendImageMailForm').hide();
			var AttachmentDivMsg = jQuery('#MailStatusMsg');
			if( AttachmentDivMsg.length == 0 )
				AttachmentDiv.append('<div id="MailStatusMsg" ></div>');
				
			var AttachmentDivMsg = jQuery('#MailStatusMsg');
			SentMail = jQuery.ajax({
				type: "POST",
				async : false,
				url: $(this).attr('action'),
				dataType: "html",
				data: $(this).serialize(),
				beforeSend:function(){
					AttachmentDivMsg.html('<p>Sending Mail <img src="'+base_url+'/app/webroot/images/loadings.gif" />..<a onclick="javascript:cancelSentMail();" style="float:right" href="javascript:void(0);" >X</a></p>');
				},
				complete: function(resulData){
					if(resulData.responseText=='success'){
						AttachmentDivMsg.html("<div class='success' >Your Image is sent successfully! </div>");
					        setTimeout( function(){ AttachmentDivMsg.hide() } ,3000);
jQuery('#mailReciever').val('');
					}else{
						AttachmentDivMsg.html("<div class='error' >Sorry not able to mail it right now,Please come again later! </div>");
						setTimeout( function(){ AttachmentDivMsg.hide() } ,3000);
					}
				}
			});
		return false;  
	});
});	
function closeFbox(){
	jQuery.fancybox.close();
}
function cs( thing ){
	console.log( thing );
}
function cancelSentMail(){
 if(SentMail){
 SentMail.abort();var AttachmentDivMsg = jQuery('#MailStatusMsg');
setTimeout( function(){ AttachmentDivMsg.hide() } ,3000);
}
}
 function hidemailDiv(){cs('asd');
			$('#attachment_mail').fadeOut('fast');
		}
function shareImgOnFb( $imgURL ){
	jQuery.ajax({
		type: "POST",
		async : false,
		url: base_url+'VendorPortfolios/facebook_shareImg',
		dataType: "html",
		data: { 'imgURL' : $imgURL },
		beforeSend:function(){
			initSpinnerFunction( );
			showSpinner();
		},
		complete: function(resulData){
			
			var $respShare = jQuery.parseJSON(resulData.responseText);
			cs( $respShare );
			if( $respShare.success=="imgSharedOnFb"){
				showErrorHide( 'custom-Overlay', 'PleaseWait', $respShare.message );
				return false;
			}else if( $respShare.error=='noImage'){
				showErrorHide( 'custom-Overlay', 'PleaseWait', ' Sorry, But no image avialable right now, please try again later !!' );
				return false;
			}else if( $respShare.error=='needFbLogin' ){
				fadeoutSpinner();
				window.open( $respShare.loginUrl,'fbImageShareScreen','width=600, height=400');
			}else if( $respShare.error=='imgShareError' ){
				showErrorHide( 'custom-Overlay', 'PleaseWait', "Error Type: "+$respShare.errorType+"<br/> Error Message: "+$respShare.message );
				return false;
			}else{
				showErrorHide( 'custom-Overlay', 'PleaseWait', ' Sorry, But not able to share image right now, please try again later !!' );
				return false;
			}
		}
	});
}


function shareImgOnTw( $imgURL ){
	jQuery.ajax({
		type: "POST",
		async : false,
		url: base_url+'VendorPortfolios/twitter_shareImg',
		dataType: "html",
		data: { 'imgURL' : $imgURL },
		beforeSend:function(){
			initSpinnerFunction( );
			showSpinner();
		},
		complete: function(resulData){
			
			var $respShare = jQuery.parseJSON(resulData.responseText);
			cs( $respShare );
			if( $respShare.success=="imgSharedOnTw"){
				showErrorHide( 'custom-Overlay', 'PleaseWait', $respShare.message );
				return false;
			}else if( $respShare.error=='noImage'){
				showErrorHide( 'custom-Overlay', 'PleaseWait', ' Sorry, But no image avialable right now, please try again later !!' );
				return false;
			}else if( $respShare.error=='needTwLogin' ){
				fadeoutSpinner();
				window.open( $respShare.loginUrl ,'twImageShareScreen','width=600, height=400');
			}else if( $respShare.error=='imgShareError' ){
				showErrorHide( 'custom-Overlay', 'PleaseWait', "Error Type: "+$respShare.errorType+"<br/> Error Message: "+$respShare.message );
				return false;
			}else{
				showErrorHide( 'custom-Overlay', 'PleaseWait', ' Sorry, But not able to share image right now, please try again later !!' );
				return false;
			}
		}
	});
}

$(function() {
$('#searchfilters').submit(function(){ //alert("hjh"); return false;
			$.ajax({
				type: "POST",
				async : false,
				dataType: 'HTML',
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(data){
					alert(data);
					$('#searchRes').html(data);
				}
			});
		return false;  
	});
});