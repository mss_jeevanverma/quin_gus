$(document).ready(function()
		{
			var box = null;
			
			$("a.userchat").click(function(event, ui)
			{	
				if(box)
				{
					box.chatbox("option", "boxManager").toggleBox();
				}
				else
				{
					box = $("#chat_div").chatbox(
					{
						id: "Me",
                        user:
						{
							key : "value"
						},
						title : "User",
						messageSent : function(id, user, msg)
						{
							//$("#log").append(id + " said: " + msg + "<br/>");
                          				  $("#chat_div").chatbox("option", "boxManager").addMsg();
                        }
					});
				}
			});
		});
