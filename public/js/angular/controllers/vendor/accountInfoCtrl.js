VendorApp.controller("accountInfo", function($scope,$http) {
   $scope.user = {address:'new addres'};
   $scope.user.lang = [{"name":"English","n":"English"},{"name":"Spanish","n":"español, castellano"},{"name":"Tagalog","n":"Wikang Tagalog, ᜏᜒᜃᜅ᜔ ᜆᜄᜎᜓᜄ᜔"},{"name":"Abkhaz","n":"аҧсуа"},{"name":"Afar","n":"Afaraf"},{"name":"Afrikaans","n":"Afrikaans"},{"name":"Akan","n":"Akan"},{"name":"Albanian","n":"Shqip"},{"name":"Amharic","n":"አማርኛ"},{"name":"Arabic","n":"العربية"},{"name":"Aragonese","n":"Aragonés"},{"name":"Armenian","n":"Հայերեն"},{"name":"Assamese","n":"অসমীয়া"},{"name":"Avaric","n":"авар мацӀ, магӀарул мацӀ"},{"name":"Avestan","n":"avesta"},{"name":"Aymara","n":"aymar aru"},{"name":"Azerbaijani","n":"azərbaycan dili"},{"name":"Bambara","n":"bamanankan"},{"name":"Bashkir","n":"башҡорт теле"},{"name":"Basque","n":"euskara, euskera"},{"name":"Belarusian","n":"Беларуская"},{"name":"Bengali","n":"বাংলা"},{"name":"Bihari","n":"भोजपुरी"},{"name":"Bislama","n":"Bislama"},{"name":"Bosnian","n":"bosanski jezik"},{"name":"Breton","n":"brezhoneg"},{"name":"Bulgarian","n":"български език"},{"name":"Burmese","n":"ဗမာစာ"},{"name":"Catalan; Valencian","n":"Català"},{"name":"Chamorro","n":"Chamoru"},{"name":"Chechen","n":"нохчийн мотт"},{"name":"Chichewa; Chewa; Nyanja","n":"chiCheŵa, chinyanja"},{"name":"Chinese","n":"中文 (Zhōngwén), 汉语, 漢語"},{"name":"Chuvash","n":"чӑваш чӗлхи"},{"name":"Cornish","n":"Kernewek"},{"name":"Corsican","n":"corsu, lingua corsa"},{"name":"Cree","n":"ᓀᐦᐃᔭᐍᐏᐣ"},{"name":"Croatian","n":"hrvatski"},{"name":"Czech","n":"česky, čeština"},{"name":"Danish","n":"dansk"},{"name":"Divehi; Dhivehi; Maldivian;","n":"ދިވެހި"},{"name":"Dutch","n":"Nederlands, Vlaams"},{"name":"Esperanto","n":"Esperanto"},{"name":"Estonian","n":"eesti, eesti keel"},{"name":"Ewe","n":"Eʋegbe"},{"name":"Faroese","n":"føroyskt"},{"name":"Fijian","n":"vosa Vakaviti"},{"name":"Finnish","n":"suomi, suomen kieli"},{"name":"French","n":"français, langue française"},{"name":"Fula; Fulah; Pulaar; Pular","n":"Fulfulde, Pulaar, Pular"},{"name":"Galician","n":"Galego"},{"name":"Georgian","n":"ქართული"},{"name":"German","n":"Deutsch"},{"name":"Greek, Modern","n":"Ελληνικά"},{"name":"Guaraní","n":"Avañeẽ"},{"name":"Gujarati","n":"ગુજરાતી"},{"name":"Haitian; Haitian Creole","n":"Kreyòl ayisyen"},{"name":"Hausa","n":"Hausa, هَوُسَ"},{"name":"Hebrew (modern)","n":"עברית"},{"name":"Herero","n":"Otjiherero"},{"name":"Hindi","n":"हिन्दी, हिंदी"},{"name":"Hiri Motu","n":"Hiri Motu"},{"name":"Hungarian","n":"Magyar"},{"name":"Interlingua","n":"Interlingua"},{"name":"Indonesian","n":"Bahasa Indonesia"},{"name":"Interlingue","n":"Originally called Occidental; then Interlingue after WWII"},{"name":"Irish","n":"Gaeilge"},{"name":"Igbo","n":"Asụsụ Igbo"},{"name":"Inupiaq","n":"Iñupiaq, Iñupiatun"},{"name":"Ido","n":"Ido"},{"name":"Icelandic","n":"Íslenska"},{"name":"Italian","n":"Italiano"},{"name":"Inuktitut","n":"ᐃᓄᒃᑎᑐᑦ"},{"name":"Japanese","n":"日本語 (にほんご／にっぽんご)"},{"name":"Javanese","n":"basa Jawa"},{"name":"Kalaallisut, Greenlandic","n":"kalaallisut, kalaallit oqaasii"},{"name":"Kannada","n":"ಕನ್ನಡ"},{"name":"Kanuri","n":"Kanuri"},{"name":"Kashmiri","n":"कश्मीरी, كشميري‎"},{"name":"Kazakh","n":"Қазақ тілі"},{"name":"Khmer","n":"ភាសាខ្មែរ"},{"name":"Kikuyu, Gikuyu","n":"Gĩkũyũ"},{"name":"Kinyarwanda","n":"Ikinyarwanda"},{"name":"Kirghiz, Kyrgyz","n":"кыргыз тили"},{"name":"Komi","n":"коми кыв"},{"name":"Kongo","n":"KiKongo"},{"name":"Korean","n":"한국어 (韓國語), 조선말 (朝鮮語)"},{"name":"Kurdish","n":"Kurdî, كوردی‎"},{"name":"Kwanyama, Kuanyama","n":"Kuanyama"},{"name":"Latin","n":"latine, lingua latina"},{"name":"Luxembourgish, Letzeburgesch","n":"Lëtzebuergesch"},{"name":"Luganda","n":"Luganda"},{"name":"Limburgish, Limburgan, Limburger","n":"Limburgs"},{"name":"Lingala","n":"Lingála"},{"name":"Lao","n":"ພາສາລາວ"},{"name":"Lithuanian","n":"lietuvių kalba"},{"name":"Luba-Katanga","n":""},{"name":"Latvian","n":"latviešu valoda"},{"name":"Manx","n":"Gaelg, Gailck"},{"name":"Macedonian","n":"македонски јазик"},{"name":"Malagasy","n":"Malagasy fiteny"},{"name":"Malay","n":"bahasa Melayu, بهاس ملايو‎"},{"name":"Malayalam","n":"മലയാളം"},{"name":"Maltese","n":"Malti"},{"name":"Māori","n":"te reo Māori"},{"name":"Marathi (Marāṭhī)","n":"मराठी"},{"name":"Marshallese","n":"Kajin M̧ajeļ"},{"name":"Mongolian","n":"монгол"},{"name":"Nauru","n":"Ekakairũ Naoero"},{"name":"Navajo, Navaho","n":"Diné bizaad, Dinékʼehǰí"},{"name":"Norwegian Bokmål","n":"Norsk bokmål"},{"name":"North Ndebele","n":"isiNdebele"},{"name":"Nepali","n":"नेपाली"},{"name":"Ndonga","n":"Owambo"},{"name":"Norwegian Nynorsk","n":"Norsk nynorsk"},{"name":"Norwegian","n":"Norsk"},{"name":"Nuosu","n":"ꆈꌠ꒿ Nuosuhxop"},{"name":"South Ndebele","n":"isiNdebele"},{"name":"Occitan","n":"Occitan"},{"name":"Ojibwe, Ojibwa","n":"ᐊᓂᔑᓈᐯᒧᐎᓐ"},{"name":"Old Church Slavonic, Church Slavic, Church Slavonic, Old Bulgarian, Old Slavonic","n":"ѩзыкъ словѣньскъ"},{"name":"Oromo","n":"Afaan Oromoo"},{"name":"Oriya","n":"ଓଡ଼ିଆ"},{"name":"Ossetian, Ossetic","n":"ирон æвзаг"},{"name":"Panjabi, Punjabi","n":"ਪ"},{"name":"Pāli","n":"पाऴि"},{"name":"Persian","n":"فارسی"},{"name":"Polish","n":"polski"},{"name":"Pashto, Pushto","n":"پښتو"},{"name":"Portuguese","n":"Português"},{"name":"Quechua","n":"Runa Simi, Kichwa"},{"name":"Romansh","n":"rumantsch grischun"},{"name":"Kirundi","n":"kiRundi"},{"name":"Romanian, Moldavian, Moldovan","n":"română"},{"name":"Russian","n":"русский язык"},{"name":"Sanskrit (Saṁskṛta)","n":"संस्कृतम्"},{"name":"Sardinian","n":"sardu"},{"name":"Sindhi","n":"सिन्धी, سنڌي، سندھی‎"},{"name":"Northern Sami","n":"Davvisámegiella"},{"name":"Samoan","n":"gagana faa Samoa"},{"name":"Sango","n":"yângâ tî sängö"},{"name":"Serbian","n":"српски језик"},{"name":"Scottish Gaelic; Gaelic","n":"Gàidhlig"},{"name":"Shona","n":"chiShona"},{"name":"Sinhala, Sinhalese","n":"සිංහල"},{"name":"Slovak","n":"slovenčina"},{"name":"Slovene","n":"slovenščina"},{"name":"Somali","n":"Soomaaliga, af Soomaali"},{"name":"Southern Sotho","n":"Sesotho"},{"name":"Sundanese","n":"Basa Sunda"},{"name":"Swahili","n":"Kiswahili"},{"name":"Swati","n":"SiSwati"},{"name":"Swedish","n":"svenska"},{"name":"Tamil","n":"தமிழ்"},{"name":"Telugu","n":"తెలుగు"},{"name":"Tajik","n":"тоҷикӣ, toğikī, تاجیکی‎"},{"name":"Thai","n":"ไทย"},{"name":"Tigrinya","n":"ትግርኛ"},{"name":"Tibetan Standard, Tibetan, Central","n":"བོད་ཡིག"},{"name":"Turkmen","n":"Türkmen, Түркмен"},{"name":"Tswana","n":"Setswana"},{"name":"Tonga (Tonga Islands)","n":"faka Tonga"},{"name":"Turkish","n":"Türkçe"},{"name":"Tsonga","n":"Xitsonga"},{"name":"Tatar","n":"татарча, tatarça, تاتارچا‎"},{"name":"Twi","n":"Twi"},{"name":"Tahitian","n":"Reo Tahiti"},{"name":"Uighur, Uyghur","n":"Uyƣurqə, ئۇيغۇرچە‎"},{"name":"Ukrainian","n":"українська"},{"name":"Urdu","n":"اردو"},{"name":"Uzbek","n":"zbek, Ўзбек, أۇزبېك‎"},{"name":"Venda","n":"Tshivenḓa"},{"name":"Vietnamese","n":"Tiếng Việt"},{"name":"Volapük","n":"Volapük"},{"name":"Walloon","n":"Walon"},{"name":"Welsh","n":"Cymraeg"},{"name":"Wolof","n":"Wollof"},{"name":"Western Frisian","n":"Frysk"},{"name":"Xhosa","n":"isiXhosa"},{"name":"Yiddish","n":"ייִדיש"},{"name":"Yoruba","n":"Yorùbá"},{"name":"Zhuang, Chuang","n":"Saɯ cueŋƅ, Saw cuengh"}];
   //$scope.user.state = [{"name":"ALABAMA","abbreviation":"AL"},{"name":"ALASKA","abbreviation":"AK"},{"name":"AMERICAN SAMOA","abbreviation":"AS"},{"name":"ARIZONA","abbreviation":"AZ"},{"name":"ARKANSAS","abbreviation":"AR"},{"name":"CALIFORNIA","abbreviation":"CA"},{"name":"COLORADO","abbreviation":"CO"},{"name":"CONNECTICUT","abbreviation":"CT"},{"name":"DELAWARE","abbreviation":"DE"},{"name":"DISTRICT OF COLUMBIA","abbreviation":"DC"},{"name":"FEDERATED STATES OF MICRONESIA","abbreviation":"FM"},{"name":"FLORIDA","abbreviation":"FL"},{"name":"GEORGIA","abbreviation":"GA"},{"name":"GUAM","abbreviation":"GU"},{"name":"HAWAII","abbreviation":"HI"},{"name":"IDAHO","abbreviation":"ID"},{"name":"ILLINOIS","abbreviation":"IL"},{"name":"INDIANA","abbreviation":"IN"},{"name":"IOWA","abbreviation":"IA"},{"name":"KANSAS","abbreviation":"KS"},{"name":"KENTUCKY","abbreviation":"KY"},{"name":"LOUISIANA","abbreviation":"LA"},{"name":"MAINE","abbreviation":"ME"},{"name":"MARSHALL ISLANDS","abbreviation":"MH"},{"name":"MARYLAND","abbreviation":"MD"},{"name":"MASSACHUSETTS","abbreviation":"MA"},{"name":"MICHIGAN","abbreviation":"MI"},{"name":"MINNESOTA","abbreviation":"MN"},{"name":"MISSISSIPPI","abbreviation":"MS"},{"name":"MISSOURI","abbreviation":"MO"},{"name":"MONTANA","abbreviation":"MT"},{"name":"NEBRASKA","abbreviation":"NE"},{"name":"NEVADA","abbreviation":"NV"},{"name":"NEW HAMPSHIRE","abbreviation":"NH"},{"name":"NEW JERSEY","abbreviation":"NJ"},{"name":"NEW MEXICO","abbreviation":"NM"},{"name":"NEW YORK","abbreviation":"NY"},{"name":"NORTH CAROLINA","abbreviation":"NC"},{"name":"NORTH DAKOTA","abbreviation":"ND"},{"name":"NORTHERN MARIANA ISLANDS","abbreviation":"MP"},{"name":"OHIO","abbreviation":"OH"},{"name":"OKLAHOMA","abbreviation":"OK"},{"name":"OREGON","abbreviation":"OR"},{"name":"PALAU","abbreviation":"PW"},{"name":"PENNSYLVANIA","abbreviation":"PA"},{"name":"PUERTO RICO","abbreviation":"PR"},{"name":"RHODE ISLAND","abbreviation":"RI"},{"name":"SOUTH CAROLINA","abbreviation":"SC"},{"name":"SOUTH DAKOTA","abbreviation":"SD"},{"name":"TENNESSEE","abbreviation":"TN"},{"name":"TEXAS","abbreviation":"TX"},{"name":"UTAH","abbreviation":"UT"},{"name":"VERMONT","abbreviation":"VT"},{"name":"VIRGIN ISLANDS","abbreviation":"VI"},{"name":"VIRGINIA","abbreviation":"VA"},{"name":"WASHINGTON","abbreviation":"WA"},{"name":"WEST VIRGINIA","abbreviation":"WV"},{"name":"WISCONSIN","abbreviation":"WI"},{"name":"WYOMING","abbreviation":"WY"}];
   
   
   
   
   
   
   $scope.frm = {msg:''};
    $http.get('/vendor/dashboard/accountinfo/').
            success(function(response) {
               //console.log(response);
                  $scope.val = response; 
                  $scope.user.address = $scope.val.vendor[0].address;
                  $scope.user.about = $scope.val.vendor[0].about;
                  $scope.user.title = $scope.val.vendor[0].title;
                  //$scope.user.tagline = $scope.val.vendor[0].tagline;
                  //$scope.user.selectedlang = $scope.val.vendor[0].languages;
                console.log($scope.user);
            }).error(function (response){
               console.log(response);
            });

      $scope.submitform = function (user,email) {
         $(".alert-success").fadeIn(); // show alert msg if hidden
         user.email = email; 
         $http.post('/vendor/dashboard/accountinfosave/', 
                    user
                    ).success(function(data) {
                        if (data==1){ $scope.frm.msg = " Your Info has been Updated.";}
                        else if (data==0){$scope.frm.msg = " You Have not made any changes to Update."; }
                        else{$scope.frm.msg = " Something Went Wrong . Please Update later.";}
                        window.setTimeout(function() {$(".alert-success").fadeOut(); }, 5000); // hide after 4 secs 
                         
                    }).error(function(data) { 
                        $scope.frm.msg = " Something Went Wrong . Please Update later.";
                    });


      };
      
      $scope.stateName = '';

   $scope.change = function(state) {
        $scope.stateName = $("#t_state option:selected").html();
        //console.log( $scope.stateName);
      //console.log(state);
         $http({
         url : '/vendor/index/statecity',
         method: "POST",
         data : {'state':state,'statename':$scope.stateName},
         headers: {"content-type":"application/json" }
               }).success(function(result) {
                      $('#mySelect')
                            .find('option')
                            .remove()
                            .end();
               $.each(result, function(index, element) {
                  //console.log(element.city);
                          
			    $('#mySelect')
			    .append($("<option></option>")
			    .attr("value",element.city)
			    .text(element.city));
                            });
                       
                            
                    }).error(function(data) { 
                                 alert("error");
                    });


      };
         $scope.change_country = function(country) {
      
      //console.log(country);
         $http({
         url : '/vendor/index/countrystate',
         method: "POST",
         data : {'country':country},
         headers: {"content-type":"application/json" }
               }).success(function(result) {
                      $('#t_state')
                            .find('option')
                            .remove()
                            .end();
               $.each(result, function(index, element) {
                  //console.log(element.state);
                          
			    $('#t_state')
			    .append($("<option></option>")
			    .attr("value",element.state_code)
			    .text(element.state));
                            });
                          if (country == 'USA') {
			      var state = 'Ak';
                              var statename = 'Alaska';
			    }else{
				var state = 'AG';
                                var statename = 'Aguascalientes';
			    }
                            
                               $http({
                                 url : '/vendor/index/statecity',
                                 method: "POST",
                                 data : {'state':state,'statename':statename},
                                 headers: {"content-type":"application/json" }
                                       }).success(function(result) {
                                              $('#mySelect')
                                                    .find('option')
                                                    .remove()
                                                    .end();
                                       $.each(result, function(index, element) {
                                          //console.log(element.city);
                                                  
                                                    $('#mySelect')
                                                    .append($("<option></option>")
                                                    .attr("value",element.city)
                                                    .text(element.city));
                                                    });
                                               
                                                    
                                            }).error(function(data) { 
                                                         alert("error");
                                            });
                         
                    }).error(function(data) { 
                                 alert("error");
                    });


      };

});
