<?php

   class Application_Model_UserMapper
    {
        protected $_dbTable;

        public function setDbTable($dbTable)
        {
            if (is_string($dbTable)) {
                $dbTable = new $dbTable();
            }
            if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                throw new Exception('Invalid table data gateway provided');
            }
            $this->_dbTable = $dbTable;
            return $this;
        }

        public function getDbTable()
        {
            if (null === $this->_dbTable) {
                $this->setDbTable('Application_Model_DbTable_User');
            }
            return $this->_dbTable;
        }

        public function save(Application_Model_User $user)
        {
            $data = array(
            	'username'         => $user->getUsername(),
            	'email'            => $user->getEmail(),
            	'password'         => $user->getPassword(),
            	'activation_key'   => $user->getActivationKey(),
            	'status'           => $user->getStatus(),

                //'email'   => $user->getEmail(),
                //'comment' => $user->getComment(),
                //'created' => date('Y-m-d H:i:s'),
            );

            if (null === ($id = $user->getId())) {
                unset($data['id']);
                $this->getDbTable()->insert($data);
            } else {
                $this->getDbTable()->update($data, array('id = ?' => $id));
            }
        }

        public function find($id, Application_Model_User $user)
        {
            $result = $this->getDbTable()->find($id);
            if (0 == count($result)) {
                return;
            }
            $row = $result->current();
            $user->setId($row->id)
                      ->setUsername($row->username)
                      ->setEmail($row->email)
                      ->setPassword($row->password)
                      ->setActivationKey($row->activation_key)
                      ->setStatus($row->status);

                      //->setComment($row->comment)
                      //->setCreated($row->created);
        }

        public function fetchAll()
        {
            $resultSet = $this->getDbTable()->fetchAll();
            $entries   = array();
            foreach ($resultSet as $row) {
                $entry = new Application_Model_User();
                $entry->setId($row->id)
                      ->setUsername($row->username)
                      ->setEmail($row->email)
                      ->setPassword($row->password)
                      ->setActivationKey($row->activation_key)
                      ->setStatus($row->status);
                $entries[] = $entry;
            }
            return $entries;
        }
    }