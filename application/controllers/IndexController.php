<?php

class IndexController extends Zend_Controller_Action
{
    protected $_redirector = null;

    public function init() {
        $this->_redirector = $this->_helper->getHelper('Redirector');
    }

    public function indexAction()
    {
	$db=Zend_Registry::get("db");    	
     	if($this->getRequest()->isGET()) {
		$UserSession = new Zend_Session_Namespace('UserSession');
		
		$slider 	= $db->fetchAll("select * from media where section=? order by id DESC limit 10 ", array('slider'), 2);
		$sitelogo 	= $db->fetchAll("select * from media where section=?", array('site-logo'), 2);
		$pages 		= $db->fetchAll("select * from pages where post_type=?", array('page'), 2);
		$vendor 	= $db->fetchAll("select * from vendor where public=?", array('1'), 2);
		$media 		= $db->fetchAll("select * from media", array(), 2);
		$venueIdea 	= $db->fetchAll("select * from media where section = 'venueidea'", array(), 2);
		$country_city = $db->fetchAll("select * from  locations ",array(),2);
		if(isset($UserSession->userId)){		
  			$sql = 'SELECT * FROM user WHERE id = ?';
                	$result = $db->fetchAll($sql, $UserSession->userId);
                        //$media = $db->fetchAll('SELECT * FROM media WHERE owner = ?', array($UserSession->userId), 2);
			$this->view->data = array('result' => $result, 'slider' => $slider, 'sitelogo' =>$sitelogo, 'pages' => $pages, 'media'=>$media, 'vendor'=>$vendor, 'venueidea'=>$venueIdea);
		}
		$this->view->data = array('slider' => $slider, 'sitelogo' =>$sitelogo, 'pages' => $pages, 'vendor'=>$vendor, 'media'=>$media, 'venueidea'=>$venueIdea,'country_city'=>$country_city);
	}
    }
    
  
}
