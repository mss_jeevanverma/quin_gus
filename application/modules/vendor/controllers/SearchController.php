<?php

class Vendor_SearchController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
	$ajaxContext->addActionContext('vsearch', 'html')
	            ->addActionContext('aep', 'html')
		    ->addActionContext('as', 'html')
	            ->initContext();
    }

    public function indexAction()
    {
        // action body
    }
    
    public function getcategoryAction()
    {
        $favrt = array();
        $db=Zend_Registry::get("db");
        $UserSession = new Zend_Session_Namespace('UserSession');
        if($this->getRequest()->isGET()){
            $category = $db->fetchAll('select * from vendor_category order by id DESC', array('user-avatar'), 2);
            $location = $db->fetchAll("select * from  locations ",array(),2);
            $vendor = $db->fetchAll("select * from vendor where public=?", array('1'), 2);
            $media = $db->fetchAll("select * from media", array(), 2);
            $graphic = $db->fetchAll('select * from media where section=? order by id DESC', array('user-avatar'), 2);  
            $user_session = ($UserSession->userId?'yes':'no');
            if($UserSession->userId){
                $favorite = $db->fetchAll('select favorite_id from favorites where user_id=?', array($UserSession->userId));
                if($favorite){
                    foreach($favorite as $key => $value){
                        array_push($favrt,$value->favorite_id);
                    }
                }          
            }

            
            $value = array("category"=>$category,"location"=>$location,"vendor"=>$vendor,"media"=>$media,"graphic"=>$graphic,'user_sess'=>$user_session,'favorite'=>$favrt);
            print_r(json_encode($value)); die();
        }
    }

    public function vsearchAction() {
        
        $request = new Zend_Controller_Request_Http;
        if ( $request->isPost() ) {
            try {
                $country = $this->getRequest()->getPost('country', null);
		        $city = $this->getRequest()->getPost('city', null);
                $by_location = $this->getRequest()->getPOST('by_location',null);            
                $cat 	= $this->getRequest()->getPost('category', null);
                $keyword = $this->getRequest()->getPost('search_key', null);
                $rating = $this->getRequest()->getPost('rating', null);
		        $state = $this->getRequest()->getPost('state', null);
                $db=Zend_Registry::get("db");
                //foreach($cat as $category) {
                //    
                //    $query .=" AND category = $category"; 
                //    
                //}
		if($cat){
		  $q=implode("','",$cat);
		}else{
		    $q='';
		}
		if($q){
		  $query="AND category In ('".$q."')";
		}else{
		  $query ='';    
		}
                if( $keyword ) {
                    $key = " AND first_name LIKE '%$keyword%' OR last_name LIKE '%$keyword%' OR address LIKE '%$keyword%' OR about LIKE '%$keyword%' OR zipcode LIKE '%$keyword%'";
                } else {
                    $key ="";
                }                
                if( $country ) {
                    $country = " AND country ='$country'";
                } else {
                    $country = "";
                }
                if( $state ) {
                    $state = " AND state ='$state'";
                }
		else {
                    $state = "";
                }
		 if( $city ) {
                    $city = " AND city ='$city'";
                }
		else {
                    $city = "";
                }
		        if( $rating ) {
                    $rate = " AND rating ='$rating'";
                } else {
                    $rate = "";
                }
                if($by_location) {
                    // list($city_v,$sate_v,$country_v) = explode('-',$by_location);
                    // $location_val = " AND city ='$city_v' AND state ='$sate_v' AND country ='$country_v'";
                    $location_val = " And location='$by_location'";
                }else{ 
                    $location_val = ""; 
                }
                $sql = $country.$city.$rate.$query.$key.$state.$location_val; 
                //echo "<pre>"; print_r($sql) ; die;
                //$result = $db->fetchAll("SELECT vendor.id,vendor.user_id,vendor.rating,vendor.address,vendor.city,vendor.state,vendor.country,vendor.phone,vendor.zipcode,vendor.fax,vendor.about,vendor.like  FROM vendor where public=? $sql", array('1'), 2);
                $result = $db->fetchAll("SELECT vendor.id,vendor.user_id,vendor.rating,vendor.company_name,vendor.address,vendor.city,vendor.state,vendor.country,vendor.phone,vendor.zipcode,vendor.fax,vendor.about,vendor.like  FROM vendor where public=? $sql", array('1'), 2);
                //echo "<pre>"; print_r($result); die;
                if ( !$result ) {
                    //print("User with this email and password does not exist !");
                } else {
                    $this->view->data = array('vendor'=>$result);
                }
            } catch (Exception $e) {
                // handle exceptions yourself
                echo $e;
            }
        }
        
    }


}

