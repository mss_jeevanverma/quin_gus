<?php

class Vendor_DashboardController extends Zend_Controller_Action
{
    protected $_redirector = null;

    public function init()
    {
        $this->_helper->layout->setLayout('v');
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('avp', 'html')
                    ->addActionContext('aep', 'html')
                    ->addActionContext('as', 'html')
                    ->addActionContext('jobapplication', 'html')
                    ->addActionContext('operationjob','html')
                    ->initContext();
    }
    
    public function indexAction() {

        $request = new Zend_Controller_Request_Http;
        //get default session namespace
        $UserSession = new Zend_Session_Namespace('VendorSession');
        if( !isset($UserSession->userId ) ){
            $urlOptions = array('module'=>'users', 'controller'=>'index', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }

        $db=Zend_Registry::get("db"); 
        $email = $UserSession->userEmail;
        
        $sql2 = 'SELECT * FROM user WHERE email = ?';
        $user = $db->fetchAll($sql2, $email);

        $status = $user[0]->status;
        $user_id = $user[0]->id;
        
        $sql = 'SELECT * FROM vendor_category';                  //fetch all categories
        $category = $db->fetchAll($sql);
     
        $sql1 = 'SELECT * FROM countries';                        //fetch all countries
        $countries = $db->fetchAll($sql1);

        $sql3 = 'SELECT * FROM vendor WHERE email = ?';
        $vendor = $db->fetchAll($sql3, $email);

        $country = $vendor[0]->country;
      
        $sql3 = 'SELECT * FROM countries WHERE short_name = ?';   //fetch vendor's country_id
        $vendor1 = $db->fetchAll($sql3, $country);
        $country_id = $vendor1[0]->country_id;
         
        $videos = $db->fetchAll('select * from media where owner=? and section=?', array($user_id, 'vendorvideo'), 2);
        $gallery = $db->fetchAll('select * from media where owner=? and section=?', array($user_id, 'vendorgallery'), 2);

        $data = array('data'=> $category,'country'=>$countries,'status'=>$status,'user'=>$user,'vendor'=>$vendor,'country_id'=>$country_id, 'videos'=>$videos, 'gallery'=>$gallery);
          
        $this->view->data = $data;
        if($request->isPost()) {

            try{  
               
                $user_data = array('first_name'   => $_POST['fname'],            //insertion in user table
                             'last_name'    => $_POST['lname'],
                             'address'      => $_POST['address'],
                             'pincode'      => $_POST['zipcode'],
                             'phone'        => $_POST['phone'],
                             'website'      => $_POST['website'],
                         
                            );

                //echo "user data<pre>"; print_r($user_data); die;

                //$db->insert('user', $user_data);
                $updateUser = $db->update('user', $user_data, 'id ='.$user_id);
            
                $vendor_data = array( 'category'      => $_POST['category'],         //insertion in vendor table
                               'first_name'    => $_POST['fname'],  
                               'last_name'     => $_POST['lname'],
                               'address'       => $_POST['address'],
                               'city'          => $_POST['city'],   
                               'state'         => $_POST['state'],
                               'country'       => $_POST['country'],
                               'phone'         => $_POST['phone'],
                               'zipcode'       => $_POST['zipcode'],
                               'website'       => $_POST['website'],
                               'fax'           => $_POST['fax'],   
                               'occupation'    => $_POST['occupation'],
                               'company_logo'  => $_POST['company_logo'],
                               'company_name'  => $_POST['company_name'],
                               //'tagline'       => $_POST['tagline'],
                               //'title'         => $_POST['title'],
                               'slider_images' => $_POST['slider_images'],
                               'about'         => $_POST['about'],
                         
                         
                            );

                //echo "user data<pre>"; print_r($user_data); die;

                 //$db->insert('vendor', $vendor_data);
                 $updateUser = $db->update('vendor', $vendor_data, 'user_id ='.$user_id);
             
                 $sql2 = 'SELECT * FROM user WHERE email = ?';
                 $user = $db->fetchAll($sql2, $email);
                 //echo "<pre>"; print_r($user);die;
                 $status = $user[0]->status;
                 $user_id = $user[0]->id;
                
                 $sql = 'SELECT * FROM vendor_category';                  //fetch all categories
                 $category = $db->fetchAll($sql);
                 //echo "<pre>"; print_r($category);die;
             
                 $sql1 = 'SELECT * FROM countries';                        //fetch all countries
                 $countries = $db->fetchAll($sql1);
                 //echo "<pre>"; print_r($countries);die;

                 $sql3 = 'SELECT * FROM vendor WHERE email = ?';
                 $vendor = $db->fetchAll($sql3, $email);
                // echo "<pre>"; print_r($vendor);die;
                 $country = $vendor[0]->country;
                 //echo $country;die;
              
                 $sql3 = 'SELECT * FROM countries WHERE short_name = ?';   //fetch vendor's country_id
                 $vendor1 = $db->fetchAll($sql3, $country);
                 //echo "<pre>"; print_r($vendor1); 
                 $country_id = $vendor1[0]->country_id;
                //echo $country_id;die;
                 $usersection = 'user-avatar';
		 $resultMedia = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $UserSession->userId , $usersection ), 2);
                
                 print_r($resultMedia);
		    if($resultMedia){
			    $media = $resultMedia ;
			    }else {
				$media = '';
			    }
                $data = array('data'=> $category,'country'=>$countries,'status'=>$status,'user'=>$user,'vendor'=>$vendor,'country_id'=>$country_id,'media' => $media);

                //echo "<pre>";print_r($data);die;
          
                $this->view->data = $data;

            } catch (Exception $e) {
 
               echo $e;
            }
        }
    }
   // ******************** contract controller when user Hires a vendor Start.
   public function contractsAction(){
        $db=Zend_Registry::get("db"); 
        $UserSession = new Zend_Session_Namespace('VendorSession');
        if( !isset($UserSession->userId ) ){
            $urlOptions = array('module'=>'users', 'controller'=>'index', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }
        
        try {
                $request = new Zend_Controller_Request_Http;
                if( $request->isGet() ) {
                    $contracts=$db->fetchAll("SELECT * FROM job WHERE hired_id=? ", array("$UserSession->userId"), 2);
                    $plan=$db->fetchAll("SELECT * FROM vendor as v Join plans as p On v.plan_id = p.id where v.user_id=? ", array($UserSession->userId), 2);
                }
                $this->view->data = array('contracts' => $contracts, 'plan'=> $plan);
        
            } catch( Exception $e ) {
                    print ($e); exit;
            }

   }
   public function viewContractsAction(){
        $request = new Zend_Controller_Request_Http;
        $db=Zend_Registry::get("db");
        $sess =  new Zend_Session_Namespace('VendorSession');
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $params = $request->getParams();
       
        if( !isset( $sess->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , 'default', array() );
        }

        if($request->isGet()) {                
            try{ 
                if( $params['id'] ) {      
                    $result = $db->fetchAll('SELECT j.id , j.title,j.ended_on, j.description, j.category,j.user_id, j.status, j.budget,j.start_on,j.end_on,j.added_on,j.updated_on, j.locations, j.address, j.starttime,j.endtime,j.termAndConditions,j.accept,j.decline,j.is_ended,j.hired_id,j.accepted_on,j.ended_on,u.id as user_id , u.username as user_username , u.email as user_email , u.name as user_name , u.first_name as user_first_name , u.last_name as user_last_name, u.address as user_address, u.pincode as user_pincode, u.phone as user_phone   FROM job as j INNER JOIN  user as u ON u.id=j.user_id WHERE j.id=? AND j.hired_id=?',array($params['id'],$sess->userId),2);         
                    //$result = $db->fetchAll('SELECT * FROM job WHERE id=? AND hired_id = ?', array($params['id'],$sess->userId) , 2);
                    if( $result ) {
                            $this->view->data = array('contract'=> $result);                    
                    }    
                }           
            } catch (Zend_Exception $e) {

            }   
        }

        if($request->isPost()) {
            try{
                $contract_status = $_POST['contract_status'];
                $contract_id = $_POST['contract_id'];
                $user_id = $_POST['user_id'];
                $vendor_id = $sess->userId;
                $job_title = $_POST['job_title'];

                //$get_convo = $db->fetchAll('SELECT * FROM conversation WHERE with_who = ? AND by_who = ?', array($vendor_id,$user_id) , 2);
                $get_convo = $db->fetchAll('select id from conversation where  (job_thread=? and with_who=? and by_who=?) or (job_thread=? and with_who=? and by_who=?) ',array("job_".$contract_id,$vendor_id,$user_id,"job_".$contract_id,$user_id,$vendor_id),2);
                //print_r($get_convo); exit;
                if($contract_status=="accepted"){ // *************** if the contract is Accepted
                $data = array("accept" => "1","accepted_on" => date("Y-m-d H:i:s"));
                $where = array("id = ?" => "$contract_id");
                $query =  $db->update('job', $data, $where); // job accepted by the Vendor
                    if(count($get_convo)) //*********** if conversation exists
                    {
                        $data = array(
                          'conv_id'=>$get_convo[0]["id"],
                          'sent_to'=>$user_id,
                          'by_from'=>$vendor_id,
                          'message'=>"Thank You For Your Offer. Your Job Contract Has Been Accepted",
                          'date' => date("Y-m-d H:i:s")
                          );
                        if ( $db->insert("messages", $data) ) {
                           // print ("Success, your message has been sent"); //exit; 
                        } else {
                            print ("Error, Try again"); //exit;
                        }

                    }else{ // *************** if conversation not present 
                        $con_data = array(
                            'name'=>$job_title,
                            'with_who'=>$vendor_id,
                            'by_who'=>$user_id,
                            'job_thread'=> "job_".$contract_id,
                            'date'=>date("Y-m-d H:i:s"),
                        );
                        if( $db->insert("conversation", $con_data) ) {
                            $lastInsertedId = $db->lastInsertId();
                            $data = array(
                                'conv_id'=>$lastInsertedId,
                                'sent_to'=>$user_id,
                                'by_from'=>$vendor_id,
                                'message'=>"Thank You For Your Offer. Your Job Contract Has Been Accepted",
                                'date' => date("Y-m-d H:i:s")
                            );
                            if ( $db->insert("messages", $data) ) {
                               // print ("Success, your message has been sent"); //exit; 
                            } else {
                                print ("Error, Try again"); //exit;
                            }
                        }
                    }
                    // applier end job message 
                    $get_apllier = $db->fetchAll('Select applier_id from appliers where job_id=?',array($contract_id),2);

                    if($get_apllier){
                        foreach ($get_apllier as $applierId) {
                                if($applierId['applier_id']!=$vendor_id){
                                     $get_convo_applier = $db->fetchAll('select id from conversation where  (job_thread=? and with_who=? and by_who=?) or (job_thread=? and with_who=? and by_who=?) ',array("job_".$contract_id,$applierId['applier_id'],$user_id,"job_".$contract_id,$user_id,$applierId['applier_id']),2);
                                        if(count($get_convo_applier)) //*********** if conversation exists
                                        {
                                            $data = array(
                                              'conv_id'=>$get_convo_applier[0]["id"],
                                              'sent_to'=>$applierId['applier_id'],
                                              'by_from'=>$user_id,
                                              'message'=>"Job Id #".$contract_id." has been closed.",
                                              'date' => date("Y-m-d H:i:s")
                                              );
                                            if ( $db->insert("messages", $data) ) {
                                                print ("Success, your message has been sent"); //exit; 
                                            } else {
                                                print ("Error, Try again"); //exit;
                                            }

                                        }else{ // *************** if conversation not present 
                                            $con_data = array(
                                                'name'=>$job_title,
                                                'with_who'=>$applierId['applier_id'],
                                                'by_who'=>$user_id,
                                                'job_thread'=> "job_".$contract_id,
                                                'date'=>date("Y-m-d H:i:s"),
                                            );
                                            if( $db->insert("conversation", $con_data) ) {
                                                $lastInsertedId = $db->lastInsertId();
                                                $data = array(
                                                    'conv_id'=>$lastInsertedId,
                                                    'sent_to'=>$applierId['applier_id'],
                                                    'by_from'=>$user_id,
                                                    'message'=>"Job Id #".$contract_id." has been closed.",
                                                    'date' => date("Y-m-d H:i:s")
                                                );
                                                if ( $db->insert("messages", $data) ) {
                                                    print ("Success, your message has been sent"); //exit; 
                                                } else {
                                                    print ("Error, Try again"); //exit;
                                                }
                                            }
                                        }
                                }
                        } 
                    }
                } 
                else{// *************** if the contract is Declined

                    // code for adding decline ides of vendors for fucture prospect Start
                    $result = $db->fetchAll('SELECT * FROM job WHERE id=? ', $contract_id , 2);
                    $decline_vendors = explode(',',$result[0]["decline"]);
                    if(!in_array($vendor_id, $decline_vendors, true)){
                            array_push($decline_vendors, $vendor_id);
                        }
                    $decline_vendors = implode(',',$decline_vendors);   
                    // code for adding decline ides of vendors for fucture prospect End
                    $data = array("hired_id" => "0","decline"=>$decline_vendors);
                    $where = array("id = ?" => "$contract_id");
                    $query =  $db->update('job', $data, $where);
                         if(count($get_convo)) // *********** if conversation exists
                        {
                            $data = array(
                              'conv_id'=>$get_convo[0]["id"],
                              'sent_to'=>$user_id,
                              'by_from'=>$vendor_id,
                              'message'=>"Your Contract Offer Has Been Declined.",
                              'date' => date("Y-m-d H:i:s")
                            );
                            if ( $db->insert("messages", $data) ) {
                                print ("/vendor/dashboard/contracts/"); //exit;
                            } else {
                                print ("Error, Try again"); //exit;
                            }

                        }else{ // *************** if conversation not present 
                            $con_data = array(
                                'name'=>$job_title,
                                'with_who'=>$vendor_id,
                                'by_who'=>$user_id,
                                'job_thread'=> "job_".$contract_id,
                                'date'=>date("Y-m-d H:i:s"),
                            );
                            if( $db->insert("conversation", $con_data) ) {
                                $lastInsertedId = $db->lastInsertId();
                                $data = array(
                                    'conv_id'=>$lastInsertedId,
                                    'sent_to'=>$user_id,
                                    'by_from'=>$vendor_id,
                                    'message'=>"Your Contract Offer Has Been Declined.",
                                    'date' => date("Y-m-d H:i:s")
                                );
                                if ( $db->insert("messages", $data) ) {
                                    print ("/vendor/dashboard/contracts/"); // exit;
                                } else {
                                    print ("Error, Try again"); //exit;
                                }
                            }
                        }
                   
                }
                 $this->view->success = "success";

            } catch (Zend_Exception $e) {
                print_r($e); die;
            }
        }
   }
   // ******************** contract controller when user Hires a vendor End
   public function gallAction() {
    
     $request = new Zend_Controller_Request_Http;
        //get default session namespace
        Zend_Session::rememberMe(604800); // Week
        $db=Zend_Registry::get("db"); 
        $UserSession = new Zend_Session_Namespace('VendorSession');
        if( !isset($UserSession->userId ) ){
            $urlOptions = array('module'=>'users', 'controller'=>'index', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }
        $user_id = $UserSession->userId;
         if( $request->isGet() ) {
           // $plan=$db->fetchAll("SELECT * FROM vendor as v Join plans as p On v.plan_id = p.id where v.user_id=? ", array($user_id), 2);
             $plan=$db->fetchAll("SELECT plan_data FROM vendor WHERE user_id=? ", array($user_id), 2);
             $plan[0] = json_decode($plan[0]["plan_data"],1);
            //echo "<pre>" ; print_r($plan); die;
            $videos = $db->fetchAll('select * from media where owner=? and section=? and status=?', array($user_id, 'vendorvideo','1'), 2);
            //$gallery = $db->fetchAll('select * from media where owner=? and section=?', array($user_id, 'vendorgallery'), 2);
            $gallery2 = $db->fetchAll('select * from media where owner=? and (section=? or section=?) and status = 1 order by id DESC limit 2', array($user_id , 'vendorgallery','vendor_image'), 2);
            $gallery = $db->fetchAll('select * from media where owner=? and (section=? or section=?) order by id DESC ', array($user_id , 'vendorgallery','vendor_image'), 2);
            $albums = $db->fetchAll('select * from albums where user_id=? order by id desc ', array($user_id),2); 
          
           // echo "<pre>";
            //print_r($albums);die;
            // $ids = [];
            // if(isset($albums)){
            //     foreach ($albums as $key => $value) {
            //         $ids[] = $value->id;
            //     }
            // }
            
            // $id = implode(',', $ids);

            //$images = $db->fetchAll("SELECT  a.* FROM media a INNER JOIN ( SELECT  album_id, MAX(ID) max_ID FROM media where album_id in ($id) GROUP BY album_id order by id desc) b ON  a.album_id = b.album_id AND a.ID = b.max_ID");
           $images = $db->fetchAll("SELECT  * FROM media where section=? and owner=?",array('vendorgallery',$user_id),2);
           // print_r($images);die;

            $this->view->data = array('videos'=>$videos,'gallery'=>$gallery,'gallery2'=>$gallery2,'albums'=>$albums,'images'=>$images,'plan'=>$plan);
        }
        
    }

public function videosAction() {
    
        $request = new Zend_Controller_Request_Http;
        //get default session namespace
        Zend_Session::rememberMe(604800); // Week
        $db=Zend_Registry::get("db"); 
        $UserSession = new Zend_Session_Namespace('VendorSession');
        if( !isset($UserSession->userId ) ){
            $urlOptions = array('module'=>'users', 'controller'=>'index', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }
        $user_id = $UserSession->userId;
         if( $request->isGet() ) {
            $plan=$db->fetchAll("SELECT * FROM vendor as v Join plans as p On v.plan_id = p.id where v.user_id=? ", array($user_id), 2);
            $videos = $db->fetchAll("select * from media where owner=? and section in ('vendorvideo', 'vendorvideoimage') order by id desc ", array($user_id), 2);
            $gallery = $db->fetchAll('select * from media where owner=? and section=?', array($user_id, 'vendorgallery'), 2);   
            $albums = $db->fetchAll('select * from albums where user_id=? order by id desc ', array($user_id),2); 
           $images = $db->fetchAll("SELECT  * FROM media where section=? and owner=?",array('vendorgallery',$user_id),2);
           // print_r($images);die;
            $this->view->data = array('videos'=>$videos,'gallery'=>$gallery,'albums'=>$albums,'images'=>$images,'plan'=>$plan);
        }
        
    }

  public function albumImagesAction() {
    
     $request = new Zend_Controller_Request_Http;
        //get default session namespace
        Zend_Session::rememberMe(604800); // Week
        $db=Zend_Registry::get("db"); 
        $UserSession = new Zend_Session_Namespace('VendorSession');
        if( !isset($UserSession->userId ) ){
            $urlOptions = array('module'=>'users', 'controller'=>'index', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }
        
        $user_id = $UserSession->userId; 
         if( $request->isGet() ) {  
            
            $album_id = $_GET['id'];
                $plan=$db->fetchAll("SELECT * FROM vendor as v Join plans as p On v.plan_id = p.id where v.user_id=? ", array($user_id), 2);
            //$images = $db->fetchAll('select * from media where ', array($album_id)); 
           // $images = $db->fetchAll('select * from media where owner=?  and (section=? or section=?) and status = 1 order by id DESC limit '.$plan[0]['portfolio_pictures'], array($user_id , 'vendorgallery','vendor_image'), 2);
           $images = $db->fetchAll('select * from media where owner=? and (section=? or section=?) order by id DESC ', array($user_id , 'vendorgallery','vendor_image'), 2);
           // echo "<pre>"; print_r($images);die;

            $this->view->data = array('user_id'=>$user_id,'images'=>$images,'albumid'=>$album_id);
        }
        

    }


public function mypicAction()
    {
        $db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('VendorSession');
	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
	
     	if($this->getRequest()->isGET()) {
		    if(isset($UserSession->userId)){		
			    $sql = 'SELECT * FROM user WHERE id = ?';
			    $result = $db->fetchAll($sql, $UserSession->userId);
			    $usersection = 'user-avatar';
			    $resultMedia = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $UserSession->userId, $usersection ), 2);
			    print_r($resultMedia);
                            if($resultMedia){
				$media = $resultMedia ;
			    }else {
				$media = '';
			    }
			    $this->view->data = array('status' => $result[0]->status, 'data' => $result, 'media' => $media);
		    }
                    echo "session expire";
	}
        
    }
	public function contactsAction()
	{	
		$db=Zend_Registry::get("db");
        $UserSession = new Zend_Session_Namespace('VendorSession');
        if( !isset($UserSession->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , null );
        }
        if($this->getRequest()->isGET()) {
            $sql = 'SELECT * from user left join favorites on favorites.user_id=user.id  where favorite_id=?';
            $result = $db->fetchAll($sql, $UserSession->userId, 2); 
            $userIds = [];
        
            $main = [];
            foreach ($result as $key => $value) {
              //array_push($main['users'],$value);
              $main['users'][$key] = $value;    
              $images = $db->fetchAll("SELECT * FROM media where owner = ?", array($value['user_id']), 2);
              $main['media'][$key] = @$images[0];
              //array_push($userIds,$value['user_id']);
               // array_push($userIds,$value->user_id);
            }

            $ids = implode(',',$userIds);
//            echo "<pre>"; print_r($main); die; 
            if ($ids) {
                echo "Inside query";
                $images = $db->fetchAll("SELECT * FROM media where owner in ($ids)", array(), 2);

            }
       
            if($result){
                $users = $result ;
            }else {
                $users = NULL;
            }
            //$this->view->data = array('users' => $users , 'media' => $images );
            $this->view->data = $main;

      } 
    }

    public function userinitAction()
    {
        //print_r($_SERVER['SERVER_NAME']); 
        // $UserSession = new Zend_Session_Namespace('UserSession');
        // $chatWith=146;
        // echo getcwd();
        // fopen("./chat/room_".$UserSession->userId."_".$chatWith.".txt", "w");
        // //$onlineusers_file=file("http://localhost/quinceanera/library/onlineusers.txt",FILE_IGNORE_NEW_LINES);
        // //$olu=join("<br>",$onlineusers_file);
        // //echo $olu;   die();
        // $this->view->data = "";
    }

    public function receiveAction()
    {
        $UserSession = new Zend_Session_Namespace('VendorSession');
        $chatWith=$_POST['receiver'];
        $lastreceived=$_POST['lastreceived'];
        $filename="./chat/room_".$UserSession->userId."_".$chatWith.".txt";
       
        if (file_exists($filename)) {
            $room_file=file($filename ,FILE_IGNORE_NEW_LINES);
            
            if( $room_file ) { 
                for($line=0;$line<count($room_file);$line++){
                    $messageArr=split("<!@!>",$room_file[$line]);
                    if( trim($messageArr[4]) == trim($UserSession->userId) ){
                        $user = 'Me';
                    }else{
                        $user = $messageArr[3];
                    }
                    if($messageArr[0]>$lastreceived) echo "<b>".$user."</b>: ". $messageArr[5]."<br>";
                }
                echo "<SRVTM>".$messageArr[0]; die();
            }
        }else{
            exit();
        }
        $this->view->data = "";        
    }

    public function sendAction()
    {
        $db=Zend_Registry::get("db");
        $UserSession = new Zend_Session_Namespace('VendorSession');
        $chatWith = $_POST['receiver'];
        $sql = 'SELECT email, first_name, last_name FROM vendor WHERE user_id=?';
        //$sql = 'SELECT email, first_name, last_name FROM user WHERE id=?';
        $result = $db->fetchAll($sql, $UserSession->userId, 2);
	
        $message=strip_tags($_POST['message']);
        $message=stripslashes($message);
        $filename = "./chat/room_".$chatWith."_".$UserSession->userId.".txt";
        if (file_exists($filename)) {
            $existsfile = "./chat/room_".$chatWith."_".$UserSession->userId.".txt";   
        }else {
            $existsfile = "./chat/room_".$UserSession->userId."_".$chatWith.".txt";

        } 
            $room_file=file($existsfile ,FILE_IGNORE_NEW_LINES);
            $room_file[]=time()."<!@!>".$chatWith."<!@!>".date("Y-m-d H:i:s"). "<!@!>".$result[0]['first_name']."<!@!>". $UserSession->userId . "<!@!>".$message;
		
            if (count($room_file)>20)$room_file=array_slice($room_file,1);
            $file_save=fopen($existsfile ,"w+");
            flock($file_save,LOCK_EX);
            for($line=0;$line<count($room_file);$line++){
                fputs($file_save,$room_file[$line]."\n");
            };
            flock($file_save,LOCK_UN);
            fclose($file_save);
            echo "sentok";
            exit();
    }
    // @vikrant Show Vendor Reviews
    public function reviewsAction(){
        $db=Zend_Registry::get("db");
        $UserSession = new Zend_Session_Namespace('VendorSession');

        if( !isset($UserSession->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , null );
        }

        try{
            $result = $db->fetchAll("SELECT * FROM feedback_reviews  WHERE about_who = ? ORDER BY id DESC", array($UserSession->userId), 2);                

            if(count($result) > 0){  
                $reviews =  $result;
            }else {
                $reviews =  NULL;
            }

            $review_main = array();
            $review_new = array();
            foreach($reviews as $review){

                $result = $db->fetchAll("SELECT * FROM user WHERE id = ?", array($review['user_id']), 2);                
                $review_new['by_email']=$result[0]['email'];
                $review_new['by_name']=$result[0]['first_name']." ".$result[0]['last_name'];
                $review_new['review']=$review['feedback_review'];
                $review_new['added_on']=$review['added_on'];
                $review_new['updated_on']=$review['updated_on'];
                $review_new['id']=$review['id'];
                array_push($review_main,$review_new);
            }

            $this->view->data = array('reviews'=> $review_main);
        }
        catch(Exception $e){

        }
    }

    // @ jeevan favourite actions
    public function bidsAction() {
    	$db=Zend_Registry::get("db");
        $UserSession = new Zend_Session_Namespace('VendorSession');

        if( !isset($UserSession->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , null );
        }
	
	$request = new Zend_Controller_Request_Http; // initilize the http request object
	
	try {
	    // @ In get request method
	    if( $request->isGet() ) {
        $plan=$db->fetchAll("SELECT * FROM vendor as v Join plans as p On v.plan_id = p.id where v.user_id=? ", array($UserSession->userId), 2);
		$result = $db->fetchAll("SELECT job.user_id,job.budget,job.hired_id,job.is_ended,job.accept, appliers.id ,appliers.applier_id, appliers.job_id, appliers.added_on, appliers.updated_on, job.title FROM appliers left join job on appliers.job_id = job.id where applier_id = ?", array($UserSession->userId), 2);				
	    	if(count($result) > 0){  
			$bids =  $result;
		}else {
			$bids =  NULL;
		}

		$this->view->data = array('bids' => $bids,'plan' => $plan);
	    }
	    
	    // @ In post request method
	    if( $request->isPost() ) {
		
	    }
	    
	} catch( Exception $e ) {
	    print ($e); exit;
	}
	
    }

	//@ Deepak Apointments Action
	public function apointmentAction(){
		
	$request = new Zend_Controller_Request_Http;
        $db=Zend_Registry::get("db");
        
        // get default session namespace
	Zend_Session::rememberMe(604800);
        $sess =  new Zend_Session_Namespace('VendorSession');
 	 
        if( !isset( $sess->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , 'default', array() );
        }
        
        if($request->isGet()) {   
                         
            try{   
                $result = $db->fetchAll("SELECT * from quotes where vendor_id = ? and apoint_date IS NOT NULL", array($sess->userId), 2);
		if($result){
			$quotes = $result;
			$users = $db->fetchAll('SELECT * from user_operations where operation_on = ? and terms = ?', array($sess->userId, 'apoint'), 2);
			$arr = array();
			foreach($users as $key => $value){
				$arr[] =  $value['operation_by'];
			}	
			$user_id = implode(',', $arr);
			$media = $db->fetchAll("SELECT * FROM media where section='user-avatar' and owner in ($user_id)", array(), 2);
			if(!empty($media)) $me = $media; 
			else $me = NULL;
		}else{
			$quotes = NULL;
			$me = NULL;
		}                  
                $this->view->data = array('quotes'=>$quotes, 'media'=>$me);     

            } catch (Zend_Db_Adapter_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;        
            } catch (Zend_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
            } 
        }
        
    }
	 // @Dinesh Quotes for vendor from users.
    public function quoteAction() {
        // action body
        $request = new Zend_Controller_Request_Http;
        $db=Zend_Registry::get("db");
        
        // get default session namespace
	Zend_Session::rememberMe(604800);
        $sess =  new Zend_Session_Namespace('VendorSession');
 	 
        if( !isset( $sess->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , 'default', array() );
        }
        
        if($request->isGet()) {   
                         
            try{   
                $result = $db->fetchAll("SELECT * from quotes where vendor_id = ? and apoint_date IS NULL and job_id IS NULL", array($sess->userId), 2);
		if($result){
			$quotes = $result;
			$users = $db->fetchAll('SELECT * from user_operations where operation_on = ? and terms = ?', array($sess->userId, 'quote'), 2);
			$arr = array();
			foreach($users as $key => $value){
				$arr[] =  $value['operation_by'];
			}	
			$user_id = implode(',', $arr);
              
			$media = $db->fetchAll("SELECT * FROM media where section='user-avatar' and owner in ($user_id)", array(), 2);
			if(!empty($media)) $me = $media; 
			else $me = NULL;
		}else{
			$quotes = NULL;
			$me = NULL;
		}
               
                $this->view->data = array('quotes'=>$quotes, 'media'=>$me);     

            } catch (Zend_Db_Adapter_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;        
            } catch (Zend_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
            } 
        }
        
    }
    // @vikrant Delete vendor Reviews
    public function deleteReviewAction(){
        $request = new Zend_Controller_Request_Http;
        $db=Zend_Registry::get("db");
        $sess =  new Zend_Session_Namespace('VendorSession');
     
        if( !isset( $sess->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , 'default', array() );
        }
        if($request->isGet()) {   
                         
            try{  
                 if( $id = $request->get('id') ) {
                    $delete = $db->delete( 'feedback_reviews', 'id = '.$id );
                    if($delete) {
                        $urlOptions = array('module'=>'vendor', 'controller'=>'dashboard', 'action'=>'reviews');
                            $this->_helper->redirector->gotoRoute($urlOptions);
                    }else{
                        $this->view->data = array('error'=>'Unable to delete Review, kindly retry !');     
                    }
                }
            } catch (Zend_Db_Adapter_Exception $e) {
    
            } 
        }

    }
    // @Dinesh delete vendor applied bids.
    public function deleteBidAction() {

	$request = new Zend_Controller_Request_Http;
        $db=Zend_Registry::get("db");
        $sess =  new Zend_Session_Namespace('VendorSession');
 	 
        if( !isset( $sess->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , 'default', array() );
        }
        
        if($request->isGet()) {   
                         
            try{  
		 if( $id = $request->get('id') ) {
			$delete = $db->delete( 'appliers', 'id = '.$id );
			if($delete) {
				$urlOptions = array('module'=>'vendor', 'controller'=>'dashboard', 'action'=>'bids');
		        	$this->_helper->redirector->gotoRoute($urlOptions);
			}else{
				$this->view->data = array('error'=>'Unable to delete bid, kindly retry !');		
			}
		}
 	    } catch (Zend_Db_Adapter_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;        
            } catch (Zend_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
            } 
        }

   } 
   //@vikrant to View Review data
   public function viewReviewAction(){
        $request = new Zend_Controller_Request_Http;
        $db=Zend_Registry::get("db");
        $sess =  new Zend_Session_Namespace('VendorSession');
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $params = $request->getParams();

        if( !isset( $sess->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , 'default', array() );
        }
        if($request->isGet())
        {
            try{
            $sql = 'SELECT * from feedback_reviews where id=?';
            $result = $db->fetchAll($sql, $params['id'] , 2);
            $result = $result[0];
            $sql = 'SELECT * from user where id=?';
            $result_user = $db->fetchAll($sql, $result['user_id'] , 2);
            $result['by_email']=$result_user[0]['email'];
            $result['by_name'] = $result_user[0]['first_name']." ".$result_user[0]['last_name'];
            $this->view->data = array('review'=>$result);
            //echo "<pre>"; print_r($result); die;
            }
            catch(Exception $e){

            }

        }

   }
   public function viewQuotesAction(){
    
    $request = new Zend_Controller_Request_Http;
        $db=Zend_Registry::get("db");
        $sess =  new Zend_Session_Namespace('VendorSession');
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $params = $request->getParams();
        if( !isset( $sess->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , 'default', array() );
        }
         if($request->isGet())
        {
             try{
            $result = $db->fetchAll("SELECT * from quotes where id = ? and apoint_date IS NULL and job_id IS NULL", array($params['id']), 2);
            $result = $result[0];
   
            $this->view->data = array('quotes'=>$result);
            //echo "<pre>"; print_r($result); die;
            }
            catch(Exception $e){

            }

            
        }
    
    
    
   }
   public function viewBidAction(){
	$request = new Zend_Controller_Request_Http;
        $db=Zend_Registry::get("db");
        $sess =  new Zend_Session_Namespace('VendorSession');
 	$request = Zend_Controller_Front::getInstance()->getRequest();
        $params = $request->getParams();
        
        if( !isset( $sess->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , 'default', array() );
        }

         if($request->isGet()) {                
            try{ 
		if( $params['id'] ) {               
		        $sql = 'SELECT * from job where id=?';
			$result = $db->fetchAll($sql, $params['id'] , 2);
                        
              		//echo "<pre>"; print_r($result);die;
		      if( $result ) {
			    $messages = $db->fetchAll("select m.id,m.conv_id, m.sent_to,m.by_from,m.message,m.date,u.first_name, u.last_name,u.email from messages as m left join conversation as c on m.conv_id = c.id left join user as u on m.by_from = u.id  where c.job_thread = ? and c.by_who = ?  and m.status in (0,1)", array('job_'.$params['id'],$sess->userId ) , 2);   
                           
                            if(count($messages) > 0 ){
				$jobMess = $messages;
			    }else{
				$jobMess = NULL;
			    }                  
		            $this->view->data = array('method' => $this->getRequest()->getMethod(), 'job'=> $result, 'messages' => $jobMess);                    
		        } else {                    
		            $this->view->data = NULL;                    
		        }    
		}           
            } catch (Zend_Db_Adapter_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;        
            } catch (Zend_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
            }   // echo "<pre>";   print_r($result);die;  
        }
    
	if($request->isPost()) {
            try{
                if(!empty($_POST['conv_id'])) {
			$conv_id = $_POST['conv_id'];
            $data = array(
                'conv_id' => $conv_id,
                'sent_to' => $_POST['with_who'],
                'by_from' => $sess->userId,
                'message' => $_POST['message'],
                'date'    => date('Y-m-d H:i:s'),
                'status'  =>  0
                );
            
              if( $n ) {
                    $this->_redirector->gotoSimple('view-bid', 'dashboard' , null , array('id' => $_POST['job_id']) );    
                } else {
                    $data = array('method' => $this->getRequest()->getMethod(), 'data' =>false, 'resp' => 'error');
                    $this->view->data  = $data;
                }
		}else{
			  $conversation = array(
		            'name'      =>  $_POST['job_thread'],
		            'with_who'  =>  $_POST['with_who'],
		            'by_who'    =>  $sess->userId,
			        'job_thread'=>  $_POST['job_thread'],
		            'date'      =>  date('Y-m-d H:i:s')
		        );
		        $convers = $db->insert('conversation', $conversation);
			if( $convers ){
                    		$conv_id = $db->lastInsertId('conversation');
			}		
		}
                if( $conv_id ){
                   	
                    $data = array(
                        'conv_id' => $conv_id,
                        'sent_to' => $_POST['with_who'],
                        'by_from' => $sess->userId,
                        'message' => $_POST['message'],
                        'date'    => date('Y-m-d H:i:s'),
                        'status'  =>  0
                        );
                    $n = $db->insert( 'messages', $data );
                    if( $n ) {
			$this->_redirector->gotoSimple('view-bid', 'dashboard' , null , array('id' => $_POST['job_id']) );
                        /*$data = array('method' => $this->getRequest()->getMethod(), 'data' =>false, 'resp'=> 'success');
                        $this->view->data  = $data;*/
                    } else {
                        $data = array('method' => $this->getRequest()->getMethod(), 'data' =>false, 'resp' => 'error');
                        $this->view->data  = $data;
                    }
                }else {
                    $data = array('method' => $this->getRequest()->getMethod(), 'data' =>false, 'resp' => 'error');
                    $this->view->data  = $data;
                }
            } catch (Zend_Db_Adapter_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
        
            } catch (Zend_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
            }
        }
   }
//********************************************************* Angular functions  Start
   public function accountinfoAction (){
        $UserSession = new Zend_Session_Namespace('VendorSession');
        $db=Zend_Registry::get("db"); 

        $email = $UserSession->userEmail;
        
        $sql2 = 'SELECT * FROM user WHERE email = ?';
        $user = $db->fetchAll($sql2, $email);

        $status = $user[0]->status;
        $user_id = $user[0]->id;
        
        $sql3 = 'SELECT * FROM vendor WHERE email = ?';
        $vendor = $db->fetchAll($sql3, $email);
        $count=$vendor[0]->country;
        $sql4 = "SELECT * FROM states_main where country_code = '".$count."'";                        //fetch all states
        $states = $db->fetchAll($sql4);
        $stat=$vendor[0]->state;
        $sql = 'SELECT * FROM vendor_category';                  //fetch all categories
        $category = $db->fetchAll($sql);
    
        $sql = "SELECT distinct(city) FROM city_main where state_code = '".$stat."'";                  //fetch all city
        $cities = $db->fetchAll($sql);
       
        //$sql4 = 'SELECT * FROM countries';                        //fetch all countries
        //$countries = $db->fetchAll($sql4);


        //$country = $vendor[0]->country;
        //if($country){
        //    $sql3 = 'SELECT * FROM countries WHERE short_name = ?';   //fetch vendor's country_id
        //    $vendor1 = $db->fetchAll($sql3, $country);
        //    $country_id = $vendor1[0]->country_id;
        // }
        // else{
        //    $country_id = 0;
        // }
        $media1 = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array($user_id,'user-avatar'), 2);
        $videos = $db->fetchAll('select * from media where owner=? and section=?', array($user_id, 'vendorvideo'), 2);
        $gallery = $db->fetchAll('select * from media where owner=? and section=?', array($user_id, 'vendorgallery'), 2);

        
        $data = array('data'=> $category,'states'=>$states,'status'=>$status,'user'=>$user,'vendor'=>$vendor,'media1'=>$media1, 'videos'=>$videos, 'gallery'=>$gallery,'cities'=>$cities);
          
        print_r(json_encode($data)); die;
        if( !isset($UserSession->userId ) ){
            $urlOptions = array('module'=>'users', 'controller'=>'index', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }
       
        

   }

   public function accountinfosaveAction(){
    $postdata = file_get_contents("php://input");

    $db=Zend_Registry::get("db"); 
    $data = (array)json_decode($postdata);
    
    if(isset($data['languages']))
        {
            $languages = implode(', ', $data['languages']);
            $data['languages'] = $languages;
        }
      
    $email = $data['email'];
    if(isset($data['fname'])) { $data['first_name'] = $data['fname']; unset($data['fname']);}
    if(isset($data['lname'])) { $data['last_name'] = $data['lname']; unset($data['lname']);}

    unset($data['email']);
    unset($data['lang']);    
    unset($data['selectedlang']);    
    //print_r($data); die;   
    $where = array("email = ?" => "$email");

    $query =  $db->update('vendor', $data, $where);
    
    print_r($query);
    die;
   }

   // vendor job handler 
   public function jobAction(){
    
     $db=Zend_Registry::get("db");
    $UserSession = new Zend_Session_Namespace('VendorSession');
            
    if( !isset($UserSession->userId ) ){
        $this->_redirector->gotoSimple('index', 'index' , null );
    }
        
        // create zend object to handle the main http request
        $request = new Zend_Controller_Request_Http;
        try {
         
            //@ handle the main get list all jobs created by me, sorted by recent on top
            if( $request->isGET() ) {
            $already= [];
            $plan=$db->fetchAll("SELECT * FROM vendor as v Join plans as p On v.plan_id = p.id where v.user_id=? ", array($UserSession->userId), 2);
    
            $data_vendor = $db->fetchAll("select * from vendor where user_id = ?", array($UserSession->userId), 2);

            $data = $db->fetchAll("SELECT * FROM job WHERE category = ? AND accept = ? ORDER BY id DESC", array($data_vendor[0]['category'],0), 2);
            
                foreach ( $data as $value ) {
                    // Already Applied job
                    $alreadyApplied = $db->fetchAll("select * from appliers where job_id=? and applier_id=? ", array($value['id'], $UserSession->userId), 2);
        
                    //if($alreadyApplied){
                    array_push($already,$alreadyApplied);
                    //}
                }
       
          
                if ( $data ) {
                    // forward data to the view
                    $this->view->data = array( 'data'=>$data,'plan'=>$plan,'applied'=>$already );
            
                }else{
                 $this->view->data = array('plan'=>$plan );
                }
            }
            
            // @ handle the main post request of the controller method
            if( $request->isPOST() ) {
                
                echo "In post request"; die;
                
            }
            
            
        } catch ( Exception $e ) {
            
        }

   }



    //@ centerlised controller to edit existing jobs 
    public function jobapplicationAction() {
        $db=Zend_Registry::get("db");
    $UserSession = new Zend_Session_Namespace('VendorSession');
    if( !isset($UserSession->userId ) ){
        $this->_redirector->gotoSimple('index', 'index' , null );
    }
    
    // create zend object to handle the main http request
        $request = new Zend_Controller_Request_Http;
    
    // get the list of all parameters in query string
    $params = Zend_Controller_Front::getInstance()->getRequest()->getParams();
    
    // try catch begins
    try {
        // to display send job application form in sidebar
        if( $request->isGET() ) {
        
        $jobId = $params['data']; 
        
        
        // check if this user has already applied for the job
        // $alreadyApplied = $db->fetchAll("select * from appliers where job_id=? and applier_id=?", array($jobId, $UserSession->userId), 2);
        // if ( $alreadyApplied ) {
            
        //     print("You have already applied for this JOB, you can edit or reply from your dashboard if you want to !!"); exit;
        // }
        
        // Apply for this job as below
        $job_applier = $db->fetchAll("select v.user_id,jo.user_id as job_user_id, jo.id, jo.title ,jo.start_on , jo.end_on , jo.budget , jo.locations , jo.address , jo.starttime ,jo.endtime from vendor as v, job as jo where v.user_id=? and jo.id=?", array($UserSession->userId, $jobId), 2);
        $appliers_qotes = $db->fetchAll("select a.*, q.id as quote_id , q.quote_content , q.quote_budget from appliers as a , quotes as q where a.job_id=? and a.applier_id=? and q.job_id=? and q.vendor_id=?", array($jobId, $UserSession->userId,$jobId, $UserSession->userId), 2);
        if( $job_applier ) {
            $this->view->data = array('data'=>$job_applier,'quotes'=>$appliers_qotes);
        } else {
            print ("Error, can apply for this job. You can notify admin about this  generating support ticket !!, or try again"); 
        }
        }
        
        // to display send job application form in sidebar
        if( $request->isPOST() ) {
        
        // @ get the all post variables
        $quoteContent = $this->getRequest()->getPost('quoteContent');
        $jobId = $this->getRequest()->getPost('jobId');
        $applierId = $this->getRequest()->getPost('applierId');
        $quote_budget = $this->getRequest()->getPost('budget'); 
        $job_title = $this->getRequest()->getPost('job_title');
        $user_job_id = $this->getRequest()->getPost('user_job_id');
        $alreadyApplied = $db->fetchAll("select * from quotes where job_id=? and vendor_id=?", array($jobId, $UserSession->userId), 2);
        if ( $alreadyApplied ) {
            
            $data = array(
                'updated_on'=>date("Y-m-d H:i:s"),
                'quote_content'=>$quoteContent,
                'quote_budget'=>$quote_budget
                ); 
           if($quoteContent=='' && $quote_budget==''){
            print("kindly fill the required fields"); exit;
           }
            $n = $db->update('quotes', $data, 'id ='.$alreadyApplied[0]['id']);
            if($n){
                print("Application has been updated, successfully"); exit;
            }
            else{
                 print ("Error, can edit for this job. You can notify admin about this  generating support ticket !!, or try again"); exit;
            }
            //print("You have already applied for this JOB, you can edit or reply from your dashboard if you want to !!"); exit;
        }
    
        // @ check if all parameters are valid
        if( $quoteContent && $jobId && $applierId  && $quote_budget ) {
            
            $data = array(
                  'vendor_id'=>$applierId,
                  'job_id'=>$jobId,
                  'quote_content'=>$quoteContent,
                  'added_on'=>date("Y-m-d H:i:s"),
                  'updated_on'=>date("Y-m-d H:i:s"),
                  'quote_budget'=>$quote_budget
                  );
            $n = $db->insert("quotes", $data);
            if( $n ) {
            $data = array(
                      'applier_id'=>$applierId,
                      'job_id'=>$jobId,
                      'added_on'=>date("Y-m-d H:i:s"),
                      'updated_on'=>date("Y-m-d H:i:s")                   
                      );
            $application = $db->insert("appliers", $data);
            if($application) {
                  $convo_id = $db->fetchAll('select id from conversation where (job_thread=? and  with_who=? and by_who=?) or (job_thread=? and with_who=? and by_who=?) ',array("job_".$jobId,$user_job_id,$applierId,"job_".$jobId,$applierId,$user_job_id),2);
                    if($convo_id){
                            $data = array(
                                'conv_id'=>$convo_id[0]['id'],
                                'sent_to'=>$user_job_id,
                                'by_from'=>$applierId,
                                'message'=>"Your Job #".$jobId." has been Applied",
                                'date' => date("Y-m-d H:i:s")
                            );
                            if ( $db->insert("messages", $data) ) {
                                 print("Application has been sent, successfully"); exit;
                            } else {
                                print ("Error, Try again"); exit;
                            }
                    }else{
                        $con_data = array(
                            'name'=>$job_title,
                            'with_who'=>$user_job_id,
                            'by_who'=>$applierId,
                            'job_thread'=> "job_".$jobId,
                            'date'=>date("Y-m-d H:i:s"),
                        );
                        if( $db->insert("conversation", $con_data) ) {
                            $lastInsertedId = $db->lastInsertId();
                            $data = array(
                                'conv_id'=>$lastInsertedId,
                                'sent_to'=>$user_job_id,
                                'by_from'=>$applierId,
                                'message'=>"Your Job #".$jobId." has been Applied",
                                'date' => date("Y-m-d H:i:s")
                            );
                            if ( $db->insert("messages", $data) ) {
                                 print("Application has been sent, successfully"); exit;
                            } else {
                                print ("Error, Try again"); exit;
                            }
                        } 
                    } 
              }
            }
            
        } else {
            print("kindly fill the required fields"); exit;
        }
        
        }
        
    } catch ( Exception $e ) {
        print $e;
    }
    
    }


    function operationjobAction(){
        $db=Zend_Registry::get("db");
        $UserSession = new Zend_Session_Namespace('VendorSession');
        if( !isset($UserSession->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , null );
        }
            
            // create zend object to handle the main http request
            $request = new Zend_Controller_Request_Http;
        try {
            
            if( $request->isGET() ) {
           $applier  = $UserSession->userId;
            //$operation = $request->getPost('data', null ); sendmessage_9_1
            list($type, $job,$user_id) = explode("_", $request->get('data', null ));
            $job_thread ='job_'.$job;
            $data = $db->fetchAll("select * from vendor as v, user as u where v.user_id=? and v.user_id=u.id", array($applier), 2);
            $messages = $db->fetchAll("select m.id,m.conv_id,c.job_thread, m.sent_to,m.by_from,m.message,m.date,u.first_name, u.last_name,u.email from messages as m left join conversation as c on m.conv_id = c.id left join user as u on m.by_from = u.id  where  (c.job_thread = ? and m.sent_to = ? and m.by_from = ?) or (c.job_thread = ? and m.sent_to = ? and m.by_from = ?) and m.status in (0,1)", array($job_thread,$applier,$user_id,$job_thread,$user_id,$applier) , 2);
            $user_details = $db->fetchAll("Select * from user where id=?",array($user_id),2);
             if(count($messages) > 0 ){
                $jobMess = $messages;
             }else{
                $jobMess = NULL;
             } 
            $job_id_title = $db->fetchAll("select title from job where id=?",array($job),2);
             if($job_id_title){
                $job_title = $job_id_title;
             }else{
                $job_title = NULL;
             }
             $job_applier = $db->fetchAll("select v.user_id,jo.user_id as job_user_id, jo.id, jo.title ,jo.start_on , jo.end_on , jo.budget , jo.locations , jo.address , jo.starttime ,jo.endtime from vendor as v, job as jo where v.user_id=? and jo.id=?", array($UserSession->userId, $job), 2);
             $appliers_qotes = $db->fetchAll("select a.*, q.id as quote_id , q.quote_content , q.quote_budget from appliers as a , quotes as q where a.job_id=? and a.applier_id=? and q.job_id=? and q.vendor_id=?", array($job, $UserSession->userId,$job, $UserSession->userId), 2);
            if( $data ) {
                $this->view->data = array('data'=>$data, 'type'=>$type, 'job'=> $job,'job_title'=>$job_title, 'messages' => $jobMess,'quotes'=>$appliers_qotes,'data_job'=>$job_applier,'user_details'=>$user_details); // @ working here to do some basic operations on applier.
            } else {
                print("Vendor not found !!"); exit;   
            }       
            }
            
            if( $request->isPost() ) {
             
            //$conversation = $this->getRequest()->getPost('conversationOn');
            $applierMessage = $this->getRequest()->getPost('applierMessage');
            $withWho = $this->getRequest()->getPost('withWho');
            $job_thread = $this->getRequest()->getPost('job_thread');
            $byWho = $UserSession->userId;
            
            if($applierMessage && $withWho && $byWho ) { // check if all fields are set
                $convo =  $this->getRequest()->getPost('conv_id');  //$db->fetchAll("select * from conversation where name=?", array($conversation), 2);
                if( $convo ) { // check if conversation already exist
                $data = array(
                      'conv_id'=>$convo,
                      'sent_to'=>$withWho,
                      'by_from'=>$byWho,
                      'message'=>$applierMessage,
                      'date' => date("Y-m-d H:i:s") 
                      );
                if ( $db->insert("messages", $data) ) {
                    print ("Success, your message has been sent"); exit; 
                } else {
                    print ("Error, Try again"); exit;
                };
                } else { // If it is new thread insert new conversation and fire message to user
                $data = array(
                      'name'=>$conversation,
                      'with_who'=>$withWho,
                      'by_who'=>$byWho,
                      'job_thread'=> $job_thread,
                      'date'=>date("Y-m-d H:i:s"),
                      );
                if( $db->insert("conversation", $data) ) {
                    
                    $lastInsertedId = $db->lastInsertId();
                    $data = array(
                      'conv_id'=>$lastInsertedId,
                      'sent_to'=>$withWho,
                      'by_from'=>$byWho,
                      'message'=>$applierMessage,
                      'date' => date("Y-m-d H:i:s")
                      );
                    if ( $db->insert("messages", $data) ) {
                    print ("Success, your message has been sent"); exit; 
                    } else {
                    print ("Error, Try again"); exit;
                    };
                }
                }
            } else {
                print("All fields are required !!"); exit; 
            }
            }
            
        } catch ( Exception $e ) {
            print $e;
        }
    }
//*********************************************************** Angular functions  end


}
