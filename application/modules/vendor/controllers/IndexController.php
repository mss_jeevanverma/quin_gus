<?php

class Vendor_IndexController extends Zend_Controller_Action
{

    protected $_redirector = null;

    public function init() {
       
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('vl', 'html')
          	    ->addActionContext('freeplan', 'html')
                    ->initContext();
    }
public function ipnsimulatorAction(){
	// this function and its view is used to test ipn at local host .
	// Fill in the fields at local host for sending a paypal like request 
	// Link is at :- http://localhost:90/vendor/index/ipnsimulator
}
public function paypalthankuAction(){
      $db=Zend_Registry::get("db"); 
	    $UserSession = Zend_Registry::get('VendorSession');
	    $user_id = $UserSession->userId;
  if(!$user_id){
    $this->view->msg = "ThankYou for Successfully Subscribing <strong> <a id='login' href='javascript:void(0);'>Click Here</a></strong> to get into your account.";
  }else{
     $this->view->msg = "ThankYou for Successfully Subscribing.";
  }
	// this function and its view is used to show thamku page after payment sucess
	// Fill in the fields at local host for sending a paypal like request 
	// Link is at :- http://localhost:90/vendor/index/ipnsimulator
}
public function testipnAction(){


		// CONFIG: Enable debug mode. This means we'll log requests into 'ipn.log' in the same directory.
		// Especially useful if you encounter network errors or other intermittent problems with IPN (validation).
		// Set this to 0 once you go live or don't require logging.
		define("DEBUG", 1);

		// Set to 0 once you're ready to go live
		define("USE_SANDBOX", 1);


		define("LOG_FILE", "ipn.log");


		// Read POST data
		// reading posted data directly from $_POST causes serialization
		// issues with array data in POST. Reading raw POST data from input stream instead.
		$raw_post_data = file_get_contents('php://input');
		$raw_post_array = explode('&', $raw_post_data);
		$myPost = array();
		foreach ($raw_post_array as $keyval) {
			$keyval = explode ('=', $keyval);
			if (count($keyval) == 2)
				$myPost[$keyval[0]] = urldecode($keyval[1]);
		}
		// read the post from PayPal system and add 'cmd'
		$req = 'cmd=_notify-validate';
		if(function_exists('get_magic_quotes_gpc')) {
			$get_magic_quotes_exists = true;
		}
		foreach ($myPost as $key => $value) {
			if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
				$value = urlencode(stripslashes($value));
			} else {
				$value = urlencode($value);
			}
			$req .= "&$key=$value";
		}

		// Post IPN data back to PayPal to validate the IPN data is genuine
		// Without this step anyone can fake IPN data

		if(USE_SANDBOX == true) {
			$paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
		} else {
			$paypal_url = "https://www.paypal.com/cgi-bin/webscr";
		}

		$ch = curl_init($paypal_url);
		if ($ch == FALSE) {
			return FALSE;
		}

		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);

		if(DEBUG == true) {
			curl_setopt($ch, CURLOPT_HEADER, 1);
			curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
		}

		// CONFIG: Optional proxy configuration
		//curl_setopt($ch, CURLOPT_PROXY, $proxy);
		//curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);

		// Set TCP timeout to 30 seconds
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

		// CONFIG: Please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path
		// of the certificate as shown below. Ensure the file is readable by the webserver.
		// This is mandatory for some environments.

		//$cert = __DIR__ . "./cacert.pem";
		//curl_setopt($ch, CURLOPT_CAINFO, $cert);

		$res = curl_exec($ch);
		if (curl_errno($ch) != 0) // cURL error
			{
			if(DEBUG == true) {	
				error_log(date('[Y-m-d H:i e] '). "Can't connect to PayPal to validate IPN message: " . curl_error($ch) . PHP_EOL, 3, LOG_FILE);
			}
			curl_close($ch);
			exit;

		} else {
				// Log the entire HTTP response if debug is switched on.
				if(DEBUG == true) {
					error_log(date('[Y-m-d H:i e] '). "HTTP request of validation request:". curl_getinfo($ch, CURLINFO_HEADER_OUT) ." for IPN payload: $req" . PHP_EOL, 3, LOG_FILE);
					error_log(date('[Y-m-d H:i e] '). "HTTP response of validation request: $res" . PHP_EOL, 3, LOG_FILE);
				}
				curl_close($ch);
		}

		// Inspect IPN validation result and act accordingly

		// Split response headers and payload, a better way for strcmp
		$tokens = explode("\r\n\r\n", trim($res));
		$res = trim(end($tokens));

		if (strcmp ($res, "VERIFIED") != 0) {
			// *************** Updating vendor data according to plan Start
			$db=Zend_Registry::get("db");  
			$email = $_POST['custom']; 
			$plan_id = $_POST['item_number'];
			$sql = 'SELECT * FROM plans WHERE id = ?';      //Get all the data from Plan 
			$plan_data = $db->fetchAll($sql, $plan_id,2);
			$plan_data = json_encode($plan_data[0]);
			$plan_start_date = date('Y-m-d h:m:s', time()); 
			$newdate = strtotime ( '+ 1 month' , strtotime ( $plan_start_date ) ) ;
			$plan_end_date = date ( 'Y-m-d h:m:s' , $newdate );
			$data = array(
					'plan_id' => $plan_id,
					'plan_end_date'   => $plan_end_date,
					'plan_data' => $plan_data
				    );
			
			$updateVendor = $db->update('vendor', $data, 'email ='."'$email'"); 

			// *************** Updating vendor data according to plan End

			// *************** Updating Transaction data according to plan Start
			$transaction_data = array(
				    		'transaction_id' => $_POST['txn_id'],
				    		'status' => $_POST['payment_status'],
				    		'amount' => $_POST['mc_gross'],
				    		'payment_date' => $plan_start_date,
				    		'email_id' => $email,
				    		'full_paypal_msg' => json_encode($_POST)
			    		);
			$insertPayment = $db->insert('pp_transactions', $transaction_data); 
			// *************** Updating Transaction data according to plan End

			//mail("mss.vikrantbhalla@gmail.com","test",$post_json."");
			// check whether the payment_status is Completed
			// check that txn_id has not been previously processed
			// check that receiver_email is your PayPal email
			// check that payment_amount/payment_currency are correct
			// process payment and mark item as paid.

			// assign posted variables to local variables
			//$item_name = $_POST['item_name'];
			//$item_number = $_POST['item_number'];
			//$payment_status = $_POST['payment_status'];
			//$payment_amount = $_POST['mc_gross'];
			//$payment_currency = $_POST['mc_currency'];
			//$txn_id = $_POST['txn_id'];
			//$receiver_email = $_POST['receiver_email'];
			//$payer_email = $_POST['payer_email'];
			
			if(DEBUG == true) {
				error_log(date('[Y-m-d H:i e] '). "Verified IPN: $req ". PHP_EOL, 3, LOG_FILE);
			}
		} else if (strcmp ($res, "INVALID") == 0) {
			// log for manual investigation
			// Add business logic here which deals with invalid IPN messages
			if(DEBUG == true) {
				error_log(date('[Y-m-d H:i e] '). "Invalid IPN: $req" . PHP_EOL, 3, LOG_FILE);
			}
		}
	die("here");	

    }

    public function indexAction() {
 

	$db=Zend_Registry::get("db");            
        if($this->getRequest()->isPost()) {

            
		    $UserSession = Zend_Registry::get('VendorSession');
                    
                    $email = $_POST['email']; 

                    $UserSession->userEmail = $email;                // write email in session variable

                    $password =$_POST['password'];                   
    
                    $sql = 'SELECT * FROM user WHERE email = ?';
     
                    $result = $db->fetchAll($sql, $email);
                    //echo "<pre>"; print_r($result);die;
                    $role = $result[0]->role;

                    if(count($result) > 0 && md5($password) == $result[0]->password) {
                        if($result[0]->status == '1' && $result[0]->role == '2' ){                      
                        		$UserSession->userId = $result[0]->id;
				        $UserSession->userStatus = $result[0]->status;
				        $UserSession->userRole = $result[0]->role;
                        	// echo $UserSession->userId;die;
 				//$this->view->message = "You have login successfully.";
 				$urlOptions = array('module'=>'vendor', 'controller'=>'dashboard', 'action'=>'index');
          			$this->_helper->redirector->gotoRoute($urlOptions);                        	
                                            
                        } else {

                           	$this->view->message = "You have not activated your account as a vendor yet.";
				            
                        }

                    } else { 

                        $this->view->message = "Please check your username and password.";
                       
                    }
        }

    }


   public function countrystateAction() {


	try{
	    $db=Zend_Registry::get("db"); 
	    $UserSession = Zend_Registry::get('VendorSession');
	    $user_id = $UserSession->userId;
	
	  if(isset($_POST['country'])) {
	    //echo $_POST['state'];
	    $country=$_POST['country'];
	  }
	  else{
	    $data=json_decode(file_get_contents("php://input"));
	    $country=$data->country;
	    
	 }
	    $state=$db->fetchAll("select state,state_code from states_main where country_code = ?",array($country),2);
	    //print_r($city);
	    //$option='';
	    
	    print_r(json_encode($state));die;
	   // 
	   //foreach($city as $citi){
	   //
	   // $option.="<option value='".$citi['city']."'>".$citi['city']."</option>";
	   // echo $option;
	   // }
	
	    
	  
	}catch(Exception $e){
	    print_r($e); die;    
	}
    }
    public function statecityAction() {


	try{
	    $db=Zend_Registry::get("db"); 
	    $UserSession = Zend_Registry::get('VendorSession');
	    $user_id = $UserSession->userId;
	
	  if(isset($_POST['state'])) {
	    //echo $_POST['state'];
	    $state=$_POST['state'];
	    $statename=$_POST['statename'];
	  }
	  else{
	    $data=json_decode(file_get_contents("php://input"));
	    $state=$data->state;
	    $statename=$data->statename;
	    
	 }
	 
	 $statename1="'%".trim($statename)."%'";
	 
	 $country=$db->fetchAll("select country_code from states_main where state_code = ? and state Like ".$statename1,array($state),2);
	 //print_r($country);

	    $city=$db->fetchAll("select city from city_main where state_code = ? and country_code = ?",array($state,$country[0]['country_code']),2);

	    //$option='';
	    
	    print_r(json_encode($city));die;
	   // 
	   //foreach($city as $citi){
	   //
	   // $option.="<option value='".$citi['city']."'>".$citi['city']."</option>";
	   // echo $option;
	   // }
	
	    
	  
	}catch(Exception $e){
	    print_r($e); die;    
	}
    }
    
    public function paymentAction() {
	
	$db=Zend_Registry::get("db"); 
	$UserSession = Zend_Registry::get('VendorSession');
	$user_id = $UserSession->userId;
	
      
	$sql = 'SELECT * FROM vendor_category';   //sending all categories to view
	$result = $db->fetchAll($sql);
	$state=$db->fetchAll('select * from states');
	$freeplan =$db->fetchAll('select * from plans where id=1');
	$VendorsChoice =$db->fetchAll('select * from plans where id=2');
	$VendorsPremiumChoice = $db->fetchAll('select * from plans where id=3');
	$VendorsChoicePlus = $db->fetchAll('select * from plans where id=4');
	$locations = $db->fetchAll("select * from  locations ",array(),2);
	$data = array('data'=> $result,'freeplan' => $freeplan,'vendorchoice' =>$VendorsChoice,'state'=>$state,'premiumchoice'=>$VendorsPremiumChoice,'vendorchoiceplus' => $VendorsChoicePlus,'locations'=>$locations);
	
	
	$this->view->data = $data;
    }
       
    public function upgradeAction() {
	$db=Zend_Registry::get("db"); 
	$UserSession = Zend_Registry::get('VendorSession');
	$user_id = $UserSession->userId;
	if( !isset($UserSession->userId) ){
            $urlOptions = array('module'=>'vendor', 'controller'=>'index', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }
	$sql = 'SELECT * FROM vendor_category';   //sending all categories to view
	$result = $db->fetchAll($sql);
	$freeplan =$db->fetchAll('select * from plans where id=1');
	$VendorsChoice =$db->fetchAll('select * from plans where id=2');
	$VendorsPremiumChoice = $db->fetchAll('select * from plans where id=3');
	$VendorsChoicePlus = $db->fetchAll('select * from plans where id=4');
	$data = array('data'=> $result,'freeplan' => $freeplan,'vendorchoice' =>$VendorsChoice,'premiumchoice'=>$VendorsPremiumChoice,'vendorchoiceplus' => $VendorsChoicePlus);
	$this->view->data = $data;
    }
    
    public function freeplanAction(){
    	$request = new Zend_Controller_Request_Http;
		$db = Zend_Registry::get("db");
     
    $category = $_COOKIE['category'];
	$country = ""; //$_COOKIE['country'];
    $city = ""; //$_COOKIE['city'];
	$state = ""; //$_COOKIE['state'];
	$email = $_COOKIE['email'];  
	$confirmemail = $_COOKIE['confirmemail']; 
    $location = $_COOKIE['location'];
        if(isset($_POST['duplicate'])) {
		    	if($email != $confirmemail)
			{
	    
				    $this->_helper->json(
				    array('message' => 'email doesnot match!' ,
					'resp' => 'success'
				    )
				);
				return;
		    }
	    
		    $sql3 = 'SELECT * FROM user WHERE email=?';    //find for  email id in user table.
		    $user3 = $db->fetchAll($sql3, $email, 2);
		    
		    $sql4 = 'SELECT * FROM vendor WHERE email=?';    //find for  email id in vendor table.
		    $user4 = $db->fetchAll($sql3, $email, 2);
		    
		    if( $user3 || $user4 ){
				$this->_helper->json(
				    array('message' => 'Either email is already registered or not valid, kindly check and try again !' ,
					'resp' => 'success'
				    )
				);
				return;
		    } else {
			       $this->_helper->json(
				    array('message' => 'error.' ,
					'resp' => 'error'
				    )
				);
				return;
		    }
        } 
    
        $plan_start_date = date('Y-m-d h:m:s', time()); 
        $plan_end_date = date('Y-m-d h:m:s', strtotime($plan_start_date. ' + 30 days'));
	   
		if(isset($_POST)){

	            $sql = 'SELECT * FROM user WHERE email = ?';    //find for  emailid in user table.
	            $user = $db->fetchAll($sql, $email);


	            $sql1 = 'SELECT * FROM vendor WHERE email = ?';    //find for vendor emailid in vendor table.
	            $vendor = $db->fetchAll($sql1, $email);
	            
	            if( $user && $vendor ) {

	            	echo "if";die;
	            }else {
	            	//echo "else";die;
		        $data = array(
				'category'        => $category,
				'plan_id'         => '1',
				'plan_start_date' => $plan_start_date,
				'plan_end_date'   => $plan_end_date,
				'country'         => '',
				'state'           => '',
				'city'            => '',
				'email'           => $email,
			);    
		        //inserted record in vendor table.
		//	$insert =$db->insert('vendor', $data);
			$urlOptions = array('module'=>'vendor', 'controller'=>'index', 'action'=>'vendor-account');
			$this->_helper->redirector->gotoRoute($urlOptions);
		    }
		}
    }

    public function sendMailtoVendor($email) {

	 // create view object
      	 Zend_Loader::loadClass('Zend_View');
      	 $html = new Zend_View();
      	 $html->setScriptPath(APPLICATION_PATH . '/modules/vendor/views/scripts/email/');
         
	 $mail = new Zend_Mail('utf-8');
 	 //render view 
 	 $data = array('vendor_email' => $email );
         //print_r($data); die;
 	 $html->pamentinfo = $data;
	 $bodyText = $html->render('sendMailtoVendor.phtml');
	 $mail->addTo($email, '');
	 $mail->setSubject('Quinceanera Payment Email');	      
     	 $mail->setFrom('admin@gmail.com', 'Email from quinceanera.');
     	 $mail->setBodyHtml($bodyText); 
    	 $mail->send();
       
    }

    public function sendMailToAdmin($email) {

	 // create view object
      	 Zend_Loader::loadClass('Zend_View');
      	 $html = new Zend_View();
      	 $html->setScriptPath(APPLICATION_PATH . '/modules/vendor/views/scripts/email/');
         Zend_Loader::loadClass('Zend_Mail');
	 $mail = new Zend_Mail();
 	 //render view 
 	 $data = array('vendor_email' => $email );
 	 $html->pamentinfo = $data;
	 $bodyText = $html->render('sendMailtoAdmin.phtml');
	 $mail->addTo('mss.harjeetkaur@gmail.com', '');
	 $mail->setSubject('Quinceanera Payment Email');	      
         $mail->setFrom('admin@gmail.com', 'Email from quinceanera.');
         $mail->setBodyHtml($bodyText); 
         $mail->send();
       
    }
// ******************************** FUNCTION FOR VENDOR PAYMENT SUCCESS START
    public function vendorpaymentsuccessAction(){

    	//ToDo: 
    	// 1.) Enter paypal data in the db for user present and user not present .
    	// 2.) Create cookie for Account Duration to work with vendor sign up account . 

    	$db=Zend_Registry::get("db");

    	$pp_transaction_id = $_GET['tx'];
    	$pp_amt = $_GET['amt'];
    	$pp_status = $_GET['st'];
    	$date = new DateTime();
		$payment_date =$date->format('Y-m-d h:m:s');

    	$transaction_data = array(
    		'transaction_id' => $pp_transaction_id,
    		'status' => $pp_status,
    		'amount' => $pp_amt,
    		'payment_date' => $payment_date
    		);

		
    	if($pp_amt=='39.95'){
    		$account_duration = 6;
    	}elseif($pp_amt=='69.95'){
			$account_duration = 9;    		
    	}elseif($pp_amt=='349.99'){
    		$account_duration = 12;
    	}else{
    		$account_duration = 0;
    	}

		$email = $_COOKIE['email'];
		$category = $_COOKIE['category'];
		$city_state_country = $_COOKIE['country'];

    	try{

	    	$sql = 'SELECT * FROM vendor WHERE email = ?';		   
	    	$result = $db->fetchAll($sql, $email);

		    if(count($result)){ // if the account by this email is present.

		    	// ************************** SAVING PP RETURN DATA **********
		    	$transaction_data['user_id'] = $result[0]->user_id;
		    	$insertVendor = $db->insert('pp_transactions', $transaction_data); 

				$newdate = strtotime ( '+'.$account_duration.' month' , strtotime ( $payment_date ) ) ;
				$newdate = date ( 'Y-m-d h:m:s' , $newdate );

		    	// ************************** Updating The Plan date **********
		    	$data1 = array(
					'plan_start_date' => $payment_date,
					'plan_end_date'   => $newdate
				    );
			
			    $updateVendor = $db->update('vendor', $data1, 'email ='."'$email'"); 
			
		    }else{// If no email -> redirect to venodor Signup page 

		    	$_SESSION["pp_transaction_id"] = $pp_transaction_id; 
		    	$_SESSION["pp_amt"] = $pp_amt; 
		    	$_SESSION["pp_status"] = $pp_status; 
		    	$_SESSION["account_duration"] = $account_duration; 

    			$urlOptions = array('module'=>'vendor', 'controller'=>'index', 'action'=>'vendor-account');
				$this->_helper->redirector->gotoRoute($urlOptions);
		    }

    	}catch(Exception $e){
    		

    	}
    }
// ******************************** FUNCTION FOR VENDOR PAYMENT SUCCESS END
     public function paymentSuccessAction() {
	$request = new Zend_Controller_Request_Http;
	$db=Zend_Registry::get("db");

	if(isset($_POST) && $_POST !='') {
	    try {
		    if( !$_POST ) {
			// echo "post";die;
			$urlOptions = array('module'=>'vendor', 'controller'=>'index', 'action'=>'vendor-account');
			$this->_helper->redirector->gotoRoute($urlOptions);
		    }
			
		    //echo "harry";die;
		    $category = $_COOKIE['category'];
		    $email = $_COOKIE['email'];   
		    unset($_COOKIE['category']);
		    unset($_COOKIE['email']);
		    unset($_COOKIE['country']);

		    $zip_code = $_POST['address_zip'];
		    $country = $_POST['address_country'];
		    $state= $_POST['address_state'];
		    $city = $_POST['address_city'];
		    $address = $_POST['address_street'];
		    $plan_amount = $_POST['mc_gross'];
		    $payer_name = $_POST['address_name'];
		    $payer_email = $_POST['payer_email'];
		    $payment_status = $_POST['payment_status'];
		    $first_name = $_POST['first_name'];
		    $last_name = $_POST['last_name'];

		      
		    //convert date to sql format
		    $payment_date1 = $_POST['payment_date']; 
		 
		    $date = new DateTime($payment_date1);
		    $payment_date =$date->format('Y-m-d h:m:s');
			 
		    //get the plan id from plan amount
		    $plan_amount = $_POST['mc_gross'];
		 
		    setcookie("PaypalEmail",$payer_email);
		 
		    $sql = 'SELECT * FROM plans WHERE price = ?';		   
		    $result = $db->fetchAll($sql, $plan_amount);
			
		    $plan_id = $result[0]->id; 
		    $plan_name = $result[0]->name;    

		    $plan_end = date('Y-m-d h:m:s', strtotime($payment_date. ' + 30 days'));
		    // echo $plan_end; die;  
			 
		    $payer_email = $_POST['payer_email'];
           
		    $sql2 = 'SELECT * FROM vendor WHERE email = ?';		   
		    $result2 = $db->fetchAll($sql2, $payer_email);
		    //echo "<pre>"; print_r($result2);die;
			 
		    if( !$result2 ){
			$data = array(
					'category'        => $category,
					//'name'            => $payer_name,
					'plan_id'         => $plan_id,
					'plan_start_date' => $payment_date,
					'plan_end_date'   => $plan_end,
					'country'         => $country,
					'state'           => $state,
					'city'            => $city,
					'email'           => $payer_email,
					'address'         => $address,
					'phone'           => '',
					'zipcode'         => $zip_code,
					'first_name'      => $first_name,
					'last_name'       => $last_name
				    );

			//echo "vendor <pre>"; print_r($data); echo count($data);die;
			if(isset($data) && count($data) != 0) {     
			    //inserted record in vendor table.
			    $insertVendor = $db->insert('vendor', $data);
			    //echo "data inserted vendor"; die;
			    $vendor_id = $db->lastInsertId('vendor');
			}

		     } else {   
		     	// echo "hat=rry";die;
		     	$vendor_id   = $result2[0]->id; 
		     	$data1 = array(
					'plan_id' => $plan_id,
					'plan_start_date' => $payment_date,
					'plan_end_date'   => $plan_end,
				    );
			if($data1) {
			    $updateVendor = $db->update('vendor', $data1, 'id ='.$vendor_id);
			}
		     }  

		
		    $txn_mc_gross               = $_POST['mc_gross'];
		    $txn_protection_eligibility = $_POST['protection_eligibility'];
		    $txn_address_status         = $_POST['address_status'];
		    $txn_payer_id               = $_POST['payer_id'];
		    $txn_tax                    = $_POST['tax'];
		    $txn_address_street         = $_POST['address_street'];
		 
		    $txn_payment_date1          = $_POST['payment_date'];
		    $date = new DateTime($txn_payment_date1);
		    $txn_payment_date =$date->format('Y-m-d h:m:s');
		 
		    $txn_payment_status         = $_POST['payment_status']; 
		    $txn_charset                = $_POST['charset'];
		    $txn_address_zip            = $_POST['address_zip'];
		    $txn_first_name             = $_POST['first_name'];
		    $txn_mc_fee                 = $_POST['mc_fee'];
		    $txn_address_country_code   = $_POST['address_country_code'];
		    $txn_address_name           = $_POST['address_name'];
			
		    $txn_notify_version         = $_POST['notify_version'];
		    $txn_custom                 = $_POST['custom'];
		    $txn_payer_status           = $_POST['payer_status'];
		    $txn_business               = $_POST['business'];
		    $txn_address_country        = $_POST['address_country']; 
		    $txn_address_city           = $_POST['address_city'];
		    $txn_quantity               = $_POST['quantity'];  
		    $txn_payer_email            = $_POST['payer_email'];
		    $txn_verify_sign            = $_POST['verify_sign'];
		    $txn_txn_id                 = $_POST['txn_id'];
		    $txn_payment_type           = $_POST['payment_type'];
		    $txn_last_name              = $_POST['last_name'];
		    $txn_address_state          = $_POST['address_state'];
		    $txn_receiver_email         = $_POST['receiver_email'];
		 
		    $txn_payment_fee            = $_POST['payment_fee']; 
		    $txn_receiver_id            = $_POST['receiver_id'];
		    $txn_pending_reason         = $_POST['pending_reason']; 
		    $txn_txn_type               = $_POST['txn_type'];  
		    $txn_item_name              = $_POST['item_name'];
		    $txn_mc_currency            = $_POST['mc_currency'];
		    $txn_item_number            = $_POST['item_number'];
		    $txn_residence_country      = $_POST['residence_country'];
		    $txn_test_ipn               = $_POST['test_ipn'];
		    //$txn_receipt_id             = $_POST['receipt_id'];
		    $txn_handling_amount        = $_POST['handling_amount'];
		    $txn_transaction_subject    = $_POST['transaction_subject']; 
		    $txn_payment_gross          = $_POST['payment_gross'];
		    $txn_shipping               = $_POST['shipping']; 
		    $txn_merchant_return_link   = $_POST['merchant_return_link'];
		    $txn_auth                   = $_POST['auth']; 
		 
		    $trans_data = array(
			'vendor_id'       => $vendor_id,
			'mc_gross'                 => $txn_mc_gross,
			'protection_eligibility'   => $txn_protection_eligibility,
			'address_status'           => $txn_address_status,
			'payer_id'                 => $txn_payer_id,
			'tax'                      => $txn_tax,
			'address_street'           => $txn_address_street,
			'payment_date'             => $txn_payment_date,
			'payment_status'           => $txn_payment_status,
			'charset'                  => $txn_charset,
			'address_zip'              => $txn_address_zip,
			'first_name'               => $txn_first_name,
			'mc_fee'                   => $txn_mc_fee,
			'address_country_code'     => $txn_address_country_code,
			'address_name'             => $txn_address_name,
			'notify_version'           => $txn_notify_version,
			'custom'                   => $txn_custom,
			'payer_status'             => $txn_payer_status,
			'business'                 => $txn_business,
			'address_country'          => $txn_address_country,
			'address_city'             => $txn_address_city,
			'quantity'                 => $txn_quantity,
			'payer_email'              => $txn_payer_email,
			'verify_sign'              => $txn_verify_sign,
			'txn_id'                   => $txn_txn_id,
			'payment_type'             => $txn_payment_type,
			'last_name'                => $txn_last_name,
			'address_state'            => $txn_address_state,
			'receiver_email'           => $txn_receiver_email,
			'payment_fee'              => $txn_payment_fee,
			'receiver_id'              => $txn_receiver_id,
			'pending_reason'           => $txn_pending_reason,
			'txn_type'                 => $txn_txn_type,
			'item_name'                => $txn_item_name,
			'mc_currency'              => $txn_mc_currency,
			'item_number'              => $txn_item_number,
			'residence_country'        => $txn_residence_country,
			'test_ipn'                 => $txn_test_ipn,
			//'receipt_id'               => $txn_receipt_id,
			'handling_amount'          => $txn_handling_amount,
			'transaction_subject'      => $txn_transaction_subject,
			'payment_gross'            => $txn_payment_gross,
			'shipping'                 => $txn_shipping,
			'merchant_return_link'     => $txn_merchant_return_link,
			'auth'                     => $txn_auth  
		    );

		    if(isset($trans_data) && count($trans_data) != 0) {  
			    $n = $db->insert('transaction', $trans_data);
		    }
		    if($n && $insertVendor){
			$urlOptions = array('module'=>'vendor', 'controller'=>'index', 'action'=>'vendor-account');
			$this->_helper->redirector->gotoRoute($urlOptions);
		    }else{
			$urlOptions = array('module'=>'vendor', 'controller'=>'index', 'action'=>'index');
			$this->_helper->redirector->gotoRoute($urlOptions);
		    }

		} catch (Exception $e) {
		    // handle exceptions yourself
		    echo $e;
		}
	    } else {
		$urlOptions = array('module'=>'vendor', 'controller'=>'index', 'action'=>'vendor-account');
		$this->_helper->redirector->gotoRoute($urlOptions);
	    }
    }
    public function sendMailAfterPayment( $txn_address_name, $plan_name,$plan_amount,$txn_txn_id,$txn_payer_id,$txn_payment_date, $txn_payer_email ) {
	// create view object
	Zend_Loader::loadClass('Zend_View');
	$html = new Zend_View();
	$html->setScriptPath(APPLICATION_PATH . '/modules/vendor/views/scripts/email/');
	$mail = new Zend_Mail('utf-8');
	//render view 
	$data = array('address' => $txn_address_name, 'plantype' => $plan_name, 'amount' => $plan_amount, 'txnid' => $txn_txn_id, 'txnpayerid' => $txn_payer_id , 'txnpaymentdate' => $txn_payment_date, 'txnpayeremail' => $txn_payer_email);
	$html->pamentinfo = $data;
	$bodyText = $html->render('paymentUseremail.phtml');
	$mail->addTo($txn_payer_email, '');
	$mail->setSubject('Quinceanera Payment Email');	      
	$mail->setFrom('admin@quinceara.com', 'Email from quinceanera.');
	$mail->setBodyHtml($bodyText); 
	$mail->send();
    }

    public function PaymentMailToAdmin($txn_address_name, $plan_name,$plan_amount,$txn_txn_id,$txn_payer_id,$txn_payment_date,
	$txn_payer_email,$txn_address_name,$txn_address_country,$txn_address_state,$txn_address_city,$txn_address_street) {
	// create view object
      	Zend_Loader::loadClass('Zend_View');
      	$html = new Zend_View();
      	$html->setScriptPath(APPLICATION_PATH . '/modules/vendor/views/scripts/email/');
        $mail = new Zend_Mail('utf-8');
 	$data = array('name' => $txn_address_name, 'plantype' => $plan_name, 'amount' => $plan_amount, 'txnid' => $txn_txn_id, 'txnpayerid' => $txn_payer_id , 'txnpaymentdate' => $txn_payment_date, 'txnpayeremail' => $txn_payer_email, 'country' => $txn_address_country, 'state' => $txn_address_state, 'city' => $txn_address_city, 'street' => $txn_address_street);
 	$html->pamentinfo = $data;
	$bodyText = $html->render('paymentAdminemail.phtml');
        $mail->addTo('', '');
        $mail->setSubject('Quinceanera User Subscribed');
        $mail->setFrom('admin@quinceara.com', 'Email from quinceanera.');
	$mail->setBodyHtml($bodyText);
	$mail->send();
    }

    public function paymentFailureAction() {
	$this->_redirector->gotoSimple('subscription-failure', 'index', null );
    }

    public function vlAction() { 
	$db=Zend_Registry::get("db"); 
	//$sql = "SELECT * FROM cities where name LIKE '$location%'"; 
	$sql = "SELECT * FROM cities "; 
	$result = $db->fetchAll($sql, array());
	$this->view->data = array("data"=>$result);
    }

    public function subscriptionFailureAction() { 


    }
    public function sendEmailVendorFree($email,$id){

		$fullBaseUrl = $this->view->serverUrl() . $this->view->baseUrl();
		$id = base64_encode($id);
		$fullBaseUrl .= "/vendor/index/activation/key/".$id;
		$url = $fullBaseUrl;
		$html_body = "After Clicking The Link Below You'r Acccout will be Activated and you will be logged in to your Dashboard <br><br> <a target='_blank' href='".$url."'>$url</a>";

		$mail = new Zend_Mail();
	    $mail->setBodyHtml($html_body);
	    $mail->setFrom('admin@quinceara.com', 'Email from quinceanera.');
	    $mail->addTo($email, 'Account Activation');
	    $mail->setSubject('Vendor Account Activation');
	    $mail->send();


    }
   public function deleteemailAction(){
    	$db = Zend_Registry::get("db");
    	$sql = 'DELETE FROM user WHERE email = ?';      //find email id from user table
		$user = $db->query($sql, 'mss.vikrantbhalla@gmail.com');
    	$sql = 'DELETE FROM vendor WHERE email = ?';      //find email id from user table
		$user = $db->query($sql, 'mss.vikrantbhalla@gmail.com');
    }

    public function activationAction(){
		$db = Zend_Registry::get("db");   // select the database instance		
	    $parms = $this->_request->getParam('key');
	    $userId = base64_decode($parms); 
		$sql = 'UPDATE user SET status = 1 WHERE id = ?';      //find email id from user table
		$user = $db->query($sql, $userId);
		$UserSession = Zend_Registry::get('VendorSession');
		$sql = 'SELECT * FROM user WHERE id = ?';      //find email id from user table
		$user = $db->fetchAll($sql, $userId);
		$UserSession->userId = $userId;
		$UserSession->userStatus = 1;
		$UserSession->userRole = 2;
		$UserSession->userEmail = $user[0]->email;	    
		$urlOptions = array('module'=>'vendor', 'controller'=>'dashboard', 'action'=>'index');
		$this->_helper->redirector->gotoRoute($urlOptions);
	    	//die('In Activation');
    }
 
    public function vendorAccountAction() {
    	
    	//echo "<pre>"; print_r($_COOKIE["planid"]); die;

	$db = Zend_Registry::get("db");   // select the database instance
	$VendorSession = new Zend_Session_Namespace('vendor');
	$free_email = $_COOKIE["email"];
	unset($_COOKIE['email']);

	$data = array('paypal_email'=> $free_email); 
	$this->view->data = $data;
	
	// ******************************************** signup for Free user Start 
	if($this->getRequest()->isPost()) { // Post Action

		$email = $this->getRequest()->getPost('email');  
		$will_travel = $this->getRequest()->getPost('will_travel');  
		$insured = $this->getRequest()->getPost('insured');  
		$years_in_buss = $this->getRequest()->getPost('years_in_buss');  
		$sql = 'SELECT * FROM user WHERE email = ?';      //find email id from user table
		$user = $db->fetchAll($sql, $email);
		
		if(!count($user)){ // if email not present 
					//list($city, $state, $country) = explode(", ", $this->getRequest()->getPost('city_state_country'));
					//$location = explode(", ", $this->getRequest()->getPost('city_state_country'));
					//if(!$location[1]){
					//    $location[1]='';
					//}
					//if(!$location[2]){
					//    $location[2]='';
					//}
					$bio = $this->getRequest()->getPost('bio');
					$company = $this->getRequest()->getPost('company');
					$username = $this->getRequest()->getPost('username');
					$password = $this->getRequest()->getPost('password');
					$city = ""; //$this->getRequest()->getPost('city');
					$state = ""; //$this->getRequest()->getPost('state');
					$country = ""; //$this->getRequest()->getPost('country');
					$location = $this->getRequest()->getPost('location');
					
					$userTbl = array(
						'username'=>$username,
						'email'=>$email,
						'password'=>md5($password),
						'role'=>2,
						'status'=>1,
						'bio'=>$bio
						);

					$user_ins = $db->insert('user', $userTbl); 
					$category = $_COOKIE['category'];
					$lastInsertId = $db->lastInsertId();
					$plan_start_date = date('Y-m-d h:m:s', time()); 
					//************************** New user end date Start
					$sql = 'SELECT * FROM plans WHERE id = ?';      //find email id from user table
					$plan_data = $db->fetchAll($sql, $_COOKIE["planid"],2);
					$plan_data = json_encode($plan_data[0]);
					if($_COOKIE["planperiod"]>0 && $_COOKIE["planprice"]>0){ // with trial period
						$account_duration = $_COOKIE["planperiod"];
						$newdate = strtotime ( '+'.$account_duration.' days' , strtotime ( $plan_start_date ) ) ;
						$plan_end_date = date ( 'Y-m-d h:m:s' , $newdate );
						$plan_id = $_COOKIE["planid"];
					}else{
						$plan_end_date	= "0000-00-00 00:00:00";
						$plan_id = 1;
					}


					//************************** New user end date End

					// **************************** if Payment Transaction is held 
					/*	$transaction_id = $_SESSION["pp_transaction_id"];
						$status = $_SESSION["pp_status"];
						$amount = $_SESSION["pp_amt"];
						$account_duration = $_SESSION["account_duration"];
						if(isset($_COOKIE["account_duration_temp"])){
							$account_duration_temp = $_COOKIE["account_duration_temp"]; // this is temprary will be removed when trafic increases on site
						}
						unset($_SESSION["pp_transaction_id"]);
			     		unset($_SESSION["pp_status"]);
			    		unset($_SESSION["pp_amt"]);
			    		unset($_SESSION["account_duration"]);
			    		unset($_COOKIE["account_duration_temp"]);
					if($transaction_id != "" && $amount != "" && $account_duration != "" )
					{
						
						$newdate = strtotime ( '+'.$account_duration.' month' , strtotime ( $plan_start_date ) ) ;
						$plan_end_date = date ( 'Y-m-d h:m:s' , $newdate );

						// ************************** SAVING PP RETURN DATA *****************
						$transaction_data = array(
				    		'transaction_id' => $transaction_id,
				    		'status' => $status,
				    		'amount' => $amount,
				    		'payment_date' => $plan_start_date,
				    		'user_id' => $lastInsertId
			    		);
			    		$insertPayment = $db->insert('pp_transactions', $transaction_data); 


					}elseif($account_duration_temp!=""){ // this is temprary will be removed when trafic increases on site

						$plan_end_date = date('Y-m-d h:m:s', strtotime($plan_start_date. ' + 90 days'));	
					}else{ // For simple free user 

						$plan_end_date = date('Y-m-d h:m:s', strtotime($plan_start_date. ' + 30 days'));	
					}*/
			        


					$vendorTbl = array(
						'user_id'=>$lastInsertId,
						'email'=>$email,
						'category'=>$category ,
						'name'=>$username,
						'plan_start_date'=> $plan_start_date,
						'plan_end_date'=>$plan_end_date,
						'plan_data' => $plan_data,
						'about'=>$bio,
						'plan_id'=>$plan_id,
						'country'=>$country,
						'city' => $city,
						'state' => $state,
						'country' => $country,
						'will_travel' => $will_travel,
						'insured' => $insured,
						'years_in_buss' => $years_in_buss,
						'public'=>1,
						'languages' => 'English',
						'company_name'=>$company,
						'location'=>$location
						);

					$vendor_ins = $db->insert('vendor',$vendorTbl); 
					
					//$this->sendEmailVendorFree($email,$lastInsertId); // no email confirmation
					if($lastInsertId){
						$VendorSession->msg = "Your Account Has Been Created .<a id ='login'> Click here to login.</a>";
						$VendorSession->class = 'alert alert-success ';
			    
					}else{
						$VendorSession->msg = 'Something Went Wrong . Please try again later.';
						$VendorSession->class = 'alert alert-danger';
					}

			}else{ // if email present
				$VendorSession->msg = 'Email Already Present';
				$VendorSession->class = 'alert alert-danger';

			}	

	}else{ // if no data posted in the form
				$VendorSession->msg = '';
				$VendorSession->class = '';

	}

	// ******************************************** signup for Free user END 


	/* old code for signup commented    
	
	$paypal_email = $_COOKIE["PaypalEmail"];   // get the value of email in PaypalEmail cokie

	if($paypal_email) {
	    $sql4 = 'SELECT * FROM vendor WHERE email = ?';   //find paid plan email id from vendor table
	    $vendor4 = $db->fetchAll($sql4, $paypal_email);
	    $plan = $vendor4[0]->plan_id;
	}
	$free_email = $_COOKIE["email"];
	if($free_email) {
	    $sql5 = 'SELECT * FROM vendor WHERE email = ?';   //find free plan email id from vendor table
	    $vendor5 = $db->fetchAll($sql5, $free_email);
	    $free_plan = $vendor5[0]->plan_id;
	}
   
	unset($_COOKIE['PaypalEmail']);
	unset($_COOKIE['email']);

	if($free_plan =='1') {
	    $data = array('paypal_email'=> $free_email);      //send emailid to view file
	} else {
	    $data = array('paypal_email'=> $paypal_email);
	}
	
	$this->view->data = $data;

	if($this->getRequest()->isPost()) {
	    
	    // initialise exception handler no handle server breakdowns
	    try {
		// related variables required to create vendor's account
		$email = $this->getRequest()->getPost('email');
		print($email);  
		list($city, $state, $country) = explode(", ", $this->getRequest()->getPost('city_state_country'));
		echo $bio = $this->getRequest()->getPost('bio');
		echo $company = $this->getRequest()->getPost('company');
		echo $username = $this->getRequest()->getPost('username');
		echo $password = $this->getRequest()->getPost('password');
		echo $city;
		echo $state;
		echo $country;
		
		$updateVendor = array(
					'name'=>$username,
					'city'=>$city,
					'state'=>$state,
					'country'=>$country,
					'company_name'=>$company,
					'about'=>$bio
				    );
		print_r($updateVendor);
		// die;
		if( $updateVendor ) {
		    $db->update('vendor', $updateVendor, 'email='.$email);
		    //print("Done");
		} else {
		    //print("No Done");
		}
		//die;
		// related variables to create vendor's account ends
		$UserSession = Zend_Registry::get('UserSession'); // write email in session variable
		$UserSession->userEmail = $email;
    
		$sql = 'SELECT * FROM user WHERE email = ?';      //find email id from user table
		$user = $db->fetchAll($sql, $email);
		
		$sql1 = 'SELECT * FROM vendor WHERE email = ?';   //find email id from vendor table
		$vendor = $db->fetchAll($sql1, $email);
    
		if(!$user && $vendor){
		    //echo "user low vendor high"; die; 
		    //die("fist if ");
		    $vendor_id   = $vendor[0]->id;
		    // $name     = $vendor[0]->name;
		    $first_name  = $vendor[0]->first_name;
		    $last_name   = $vendor[0]->last_name;
		    $address     = $vendor[0]->address;
		    $zipcode     = $vendor[0]->zipcode;
		    $date_joined = $vendor[0]->plan_start_date; 
	
		    $data = array(
			'email'=>$email,
			'password'=>md5($password),
			'role' => '2',
			'first_name'   => $first_name,
			'last_name'    => $last_name,
			'address'      => $address,
			'pincode'      => $zipcode,
			'date_joined'  => $date_joined,
			'status'       =>'1'
		    );
		    $db->insert('user', $data);
		    $user_id = $db->lastInsertId('user');
		    $data1 = array('user_id'   => $user_id  ); //insert user_id in vendor table 
		    $updateUser = $db->update('vendor', $data1, 'id ='.$vendor_id);
		    $urlOptions = array('module'=>'vendor', 'controller'=>'dashboard', 'action'=>'index');
		    $this->_helper->redirector->gotoRoute($urlOptions);
		} elseif ($user && !$vendor){
		    // echo "user high vendor low";
		
		    $user_id = $user[0]->id; // if user exists update record
		    $data = array(
				'password' => md5($password),
				'role'         => '2',
				'status'       => '1'
			    );
		    $updateUser = $db->update('user', $data, 'id ='.$user_id);
		    $urlOptions = array('module'=>'vendor', 'controller'=>'dashboard', 'action'=>'index');
		    $this->_helper->redirector->gotoRoute($urlOptions);
		} elseif(!$user && !$vendor){
		    // echo "user low vendor low";
		
		    $urlOptions = array('module'=>'vendor', 'controller'=>'index', 'action'=>'');
		    $this->_helper->redirector->gotoRoute($urlOptions);
		} else {
		    //echo "user high vendor high";
		
		    $vendor_id   = $vendor[0]->id;
		    //$name        = $vendor[0]->name;
		    $first_name  = $vendor[0]->first_name;
		    $last_name   = $vendor[0]->last_name;
		    $address     = $vendor[0]->address;
		    $zipcode     = $vendor[0]->zipcode;
		    $date_joined = $vendor[0]->plan_start_date;
		    $user_id = $user[0]->id; // if user exists update record
		    $data = array(
				    'password'     => md5($password),
				    'role'         => '2',
				    //'name'         => $name,
				    'first_name'   => $first_name,
				    'last_name'    => $last_name,
				    'address'      => $address,
				    'pincode'      => $zipcode,
				    'date_joined'  => $date_joined,
				    'status'       =>'1'
				);
		    $updateUser = $db->update('user', $data, 'id ='.$user_id);
		    $data1 = array('user_id'   => $user_id  ); //insert user_id in vendor table 
		    $updateUser = $db->update('vendor', $data1, 'id ='.$vendor_id);
		    $urlOptions = array('module'=>'vendor', 'controller'=>'dashboard', 'action'=>'index');
		    $this->_helper->redirector->gotoRoute($urlOptions);
		}
	    } catch ( Exception $e ) {
		print $e;
	    }
	    
	}*/
    }
}


