<?php

class Vendor_PortfolioController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_helper->layout->setLayout('v');
        $this->_redirector = $this->_helper->getHelper('Redirector');
    }

    public function indexAction()
    {
        Zend_Session::rememberMe(604800);
        $db=Zend_Registry::get("db"); 
        $UserSession = new Zend_Session_Namespace('VendorSession');
        if( !isset( $UserSession->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , 'default', array() );
        }
        
        if($this->getRequest()->isGET()) {
            $media = $db->fetchAll("SELECT * FROM media where section in ('vendorvideo', 'vendorgallery', 'vendorvideoimage') and owner = ? and status= ?", array($UserSession->userId, 1), 2);
            $this->view->data = array('portfolio' => $media);
        }
    }

    public function configurationAction()
    {
        Zend_Session::rememberMe(604800);
        $db=Zend_Registry::get("db"); 
        $UserSession = new Zend_Session_Namespace('VendorSession');
        if( !isset( $UserSession->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , 'default', array() );
        }

	if( $this->getRequest()->isGET() ) {

		

	}
        
        if( $this->getRequest()->isPOST() ) {
            $fax =  (isset($_POST['fax'])) ? $_POST['fax'] : '';
            $web =  (isset($_POST['website'])) ? $_POST['website'] : '';
            $desc = (isset($_POST['comp_desc'])) ? $_POST['comp_desc'] : '';
            $data = array(
                'company_name'    => $_POST['company_name'],
                'first_name'      => $_POST['contact_name'],
                'address'         => $_POST['address'],
                'country'         => $_POST['country'],
                'city'            => $_POST['city'],
                'state'           => $_POST['state'],
                'phone'           => (int)$_POST['phone'],
                'fax'             => $fax,
                'website'         => $web,
                'zipcode'         => $_POST['zip_code'],
                'about'           => $desc,
		'service_cost'	  => $_POST['service_price']
            );     
            $update = $db->update('vendor', $data, 'user_id = '.$UserSession->userId);
            $geocode    =   file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.trim(urlencode($_POST['address'])).',+'.trim(urlencode($_POST['city'])).',+'.trim(urlencode($_POST['state'])).',+'.trim(urlencode($_POST['country'])).'&sensor=false');
            $output     = json_decode($geocode); //Store values in variable
            if($output->status == 'OK'){ // Check if address is available or not
                $latitude = $output->results[0]->geometry->location->lat; //Returns Latitude
                $longitude = $output->results[0]->geometry->location->lng; // Returns Longitude
            }
            else {
                $latitude = '';
                $longitude = '';
            }
            if($update) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'lat' => $latitude, 'longi' => $longitude, 'data'=> 'Your information saved successfully !!!', 'resp'=> 'success');
		$this->view->data = $data;
            }else{
                $data = array('method'=>$this->getRequest()->getMethod(), 'lat' => '', 'longi' => '', 'data'=> 'error', 'resp'=> 'error');
		$this->view->data = $data;
            }
        }
    }



}

