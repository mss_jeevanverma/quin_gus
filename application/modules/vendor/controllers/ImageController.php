<?php

class Vendor_ImageController extends Zend_Controller_Action
{
    protected $_redirector = null;

    public function init()
    {
        $this->_helper->layout->setLayout('portfolio');
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('make-me-follow', 'html')
                    ->initContext();
        
    }

    public function indexAction() {

        $request = new Zend_Controller_Request_Http;        
        if($request->isGet()) {    
            $db=Zend_Registry::get("db");
            $getParams = Zend_Controller_Front::getInstance()->getRequest();
            $params = $getParams->getParams();
            $result = $db->fetchAll("select * from media where id=?", array($params['id']), 2);
            if($result){
                $this->view->data = array("image" => $result[0]);
            }else{
                $this->view->data = NULL;
            }
        }
    }
    
    public function makeMeFollowAction(){
        $request = new Zend_Controller_Request_Http;        
        if($request->isPOST()) {
            $UserSession = new Zend_Session_Namespace('UserSession');
            if(!empty($UserSession->userId)){
                $user_id = $UserSession->userId;
                die("needToLogin");
            }else{
                die("needToLogin");
            }
            
        }
    }
    
}

