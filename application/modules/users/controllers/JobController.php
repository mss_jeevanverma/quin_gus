<?php

class Users_JobController extends Zend_Controller_Action
{
    protected $_redirector = null;
    public function init()
    {
        /* Initialize action controller here */
	$this->_redirector = $this->_helper->getHelper('Redirector');
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
	$ajaxContext->addActionContext('new', 'html')
		    ->addActionContext('edit', 'html')
		    ->addActionContext('operationonapplier', 'html')
                    ->addActionContext('del', 'html')
		    ->addActionContext('jobapplication', 'html')
	            ->initContext();
    }
    
    //@ centerlised controller to index all the job created by the user 
    public function indexAction() {
        
        $db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('VendorSession');
			
	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
        
        // create zend object to handle the main http request
        $request = new Zend_Controller_Request_Http;
        try {
         
            //@ handle the main get list all jobs created by me, sorted by recent on top
            if( $request->isGET() ) {
            $already= [];
			$plan=$db->fetchAll("SELECT * FROM vendor as v Join plans as p On v.plan_id = p.id where v.user_id=? ", array($UserSession->userId), 2);
	
			$data_vendor = $db->fetchAll("select * from vendor where user_id = ?", array($UserSession->userId), 2);

            $data = $db->fetchAll("SELECT * FROM job WHERE category = ? AND accept = ? ORDER BY id DESC", array($data_vendor[0]['category'],0), 2);
            
	            foreach ( $data as $value ) {
	            	// Already Applied job
	            	$alreadyApplied = $db->fetchAll("select * from appliers where job_id=? and applier_id=? ", array($value['id'], $UserSession->userId), 2);
		
                    //if($alreadyApplied){
	            	array_push($already,$alreadyApplied);
	                //}
	            }
       
          
                if ( $data ) {
                    // forward data to the view
                    $this->view->data = array( 'data'=>$data,'plan'=>$plan,'applied'=>$already );
		    
                }else{
                 $this->view->data = array('plan'=>$plan );
                }
            }
            
            // @ handle the main post request of the controller method
            if( $request->isPOST() ) {
                
                echo "In post request"; die;
                
            }
            
            
        } catch ( Exception $e ) {
            
        }
        
    }

    //@ centerlised controller to create new jobs 
    public function newAction() {
    	
        $db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('UserSession');
	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
        
        // create zend object to handle the main http request
        $request = new Zend_Controller_Request_Http;
        
        try {
            
            // @ handle the main post request of the controller method
            if( $request->isGET() ) {
                $data = $db->fetchAll("select * from vendor_category order by id DESC", array(), 2);
                if ( $data ) {
                    // forward data to the view
                    $this->view->data = array( 'data'=>$data );
                }                
            }
            
            // @ handle the main post request of the controller method
            if( $request->isPOST() ) {
                
                //@ calculate the post variable
                $serviceCategory = $this->getRequest()->getPost('serviceCategory', null);
                $jobTitle   = $this->getRequest()->getPost('jobTitle', null);
                $startDate  = $this->getRequest()->getPost('startDate', null);
                $endDate    = $this->getRequest()->getPost('endDate', null);
                //$rating     = $this->getRequest()->getPost('rating', null);
                $myBudget    = $this->getRequest()->getPost('myBudget', null);
                $jobDescription = $this->getRequest()->getPost('jobDescription', null);
                $locations	= $this->getRequest()->getPOST('locations', null);
                $address	= $this->getRequest()->getPOST('address', null);
                $starttime	= $this->getRequest()->getPOST('starttime', null);
                $endtime	= $this->getRequest()->getPOST('endtime', null);
                $termAndConditions	= $this->getRequest()->getPOST('termAndConditions', null);
                
                // if ( !is_numeric( $myBudget ) || $myBudget < 0 ) {
                //     print(" Budget format invalid !!! "); exit;
                // }
                
                // check if all fields have set
                if( $serviceCategory && $jobTitle && $startDate && $endDate && $myBudget && $jobDescription && $locations && $starttime &&  $endtime && $termAndConditions) {
                    
                    // check job post date difference
                    list($m1,$d1,$y1) = explode('-', $startDate);
                   
                    //$date1 = date("d/m/Y", mktime(0, 0, 0, $d1, $m1, $y1));
                    $date1_str1 = $d1."-".$m1."-".$y1;
                    $date1 = $d1."/".$m1."/".$y1;
                    // check job post date difference
                    list($m2,$d2,$y2)=explode('-', $endDate);
                    //$date2 = date("d/m/Y", mktime(0, 0, 0, $d2, $m2, $y2));
                    $date2_str2 = $d2."-".$m2."-".$y2;
                    $date2 = $d2."/".$m2."/".$y2;
                    // substract two date values to get absolute value of the difference
                    $diff = abs(strtotime($date2_str2) - strtotime($date1_str1));
                    // @ check if end date is greater than start date
                    if( strtotime($date2_str2) < strtotime($date1_str1) ) {
                        print(" Job's end date is less than job start date, kindly choose end date greater than start date !!!!"); exit;  
                    }
                    
                    $data = array(
                              'title'=>$jobTitle,
                              'description'=>$jobDescription,
                              'category'=>$serviceCategory,
                              'user_id'=>$UserSession->userId,
                              'budget'=>$myBudget,
                              'start_on'=>$startDate,
                              'end_on'=>$endDate,
                              'added_on'=>date("Y-m-d H:i:s"),
                              'updated_on'=>date("Y-m-d H:i:s"),
                              'locations'=>$locations,
                              'address'=>$address,
                              'starttime'=>$starttime,
                              'endtime'=>$endtime,
                              'termAndConditions'=>$termAndConditions                              
                              );
                }
                
                if ( !@$data ) {
                    // all values are empty (where "empty" means == false)
                    print(" All * fields are required !!! ");
		      $data = $db->fetchAll("select * from vendor_category order by id DESC", array(), 2);
		      $this->view->data = array( 'data'=>$data );
		    exit;
                }
		
		if( $jobId = $this->getRequest()->getPost('jobId', null) ) {
		    //print($jobId); 
		    
		    //print_r($data); die;
		    if ( $db->update('job', $data, 'id='.$jobId.'') ){
			print (" Job Updated successfully !!!!");
             $this->sendUpdateMessage($jobId,$jobTitle);
			 $this->view->data = array( 'data'=>$data );
			exit;
		    }
		}
                
                if( $db->insert( "job", $data ) ) { // inserting data into to job table and check if it is a success
                	
                	$this->sendSystemMessage($serviceCategory); // send messsage to all vendors with this Category
                    print("Job created successfully !!!!"); exit;    
                } else {
                    print (" Unable to save the job please try again !!!!"); exit;
                }
            }
            
        } catch ( Exception $e ) {
            
            print( $e ); exit;
        }
        
    }
    
    public function sendUpdateMessage($jobId,$jobTitle){
        $db = Zend_Registry::get("db");
        $UserSession = new Zend_Session_Namespace('UserSession');
        try{
            $get_appliers_id  = $db->fetchAll('select applier_id from appliers where job_id=?',array($jobId),2);
            $user_id = $UserSession->userId;
            foreach ($get_appliers_id as $applierId) {
                 $get_convo = $db->fetchAll('select id from conversation where  (job_thread=? and with_who=? and by_who=?) or (job_thread=? and with_who=? and by_who=?) ',array("job_".$jobId,$applierId['applier_id'],$user_id,"job_".$jobId,$user_id,$applierId['applier_id']),2);
                    if(count($get_convo)) //*********** if conversation exists
                    {
                        $data = array(
                          'conv_id'=>$get_convo[0]["id"],
                          'sent_to'=>$applierId['applier_id'],
                          'by_from'=>$user_id,
                          'message'=>"Job Id  #".$jobId." Updated by user.",
                          'date' => date("Y-m-d H:i:s")
                          );
                        $db->insert("messages",$data);
                    }else{ // *************** if conversation not present 
                        $con_data = array(
                            'name'=>$jobTitle,
                            'with_who'=>$applierId['applier_id'],
                            'by_who'=>$user_id,
                            'job_thread'=> "job_".$jobId,
                            'date'=>date("Y-m-d H:i:s"),
                        );
                        if( $db->insert("conversation", $con_data) ) {
                            $lastInsertedId = $db->lastInsertId();
                            $data = array(
                                'conv_id'=>$lastInsertedId,
                                'sent_to'=>$applierId['applier_id'],
                                'by_from'=>$user_id,
                                'message'=>"Job Id #".$jobId." Updated by user.",
                                'date' => date("Y-m-d H:i:s")
                            );
                            $db->insert("messages", $data);                    
                        }
                    }
            }
            die();
        } catch (Exception $e){
            print($e); exit;
        }
    }

    public function sendSystemMessage($serviceCategory){ // function to send system genrated messages.
    	$db=Zend_Registry::get("db");
    	$get_to_ids = $db->fetchAll('SELECT user_id FROM vendor where category = ? ', array($serviceCategory) , 2);

    	foreach($get_to_ids as $to_id)
    		{
    			$to_id=$to_id['user_id'];
    			$thread_name = "system_0_".$to_id;
    			$get_convo = $db->fetchAll('SELECT * FROM conversation WHERE job_thread = ?', array($thread_name) , 2);
                    if(count($get_convo)) //*********** if conversation exists
                    {
                        $data = array(
                          'conv_id'=>$get_convo[0]["id"],
                          'sent_to'=>$to_id,
                          'by_from'=>'0',
                          'message'=>"A New Job Has been Updated . Please Check Your Jobs Section.",
                          'date' => date("Y-m-d H:i:s")
                          );
                        if ( $db->insert("messages", $data) ) {
                            //print ("Success, your message has been sent"); //exit; 
                        } else {
                            //print ("Error, Try again"); //exit;
                        }

                    }else{ // *************** if conversation not present 
                        $con_data = array(
                            'name'=>'System_Message',
                            'with_who'=>$to_id,
                            'by_who'=>'0',
                            'job_thread'=> $thread_name,
                            'date'=>date("Y-m-d H:i:s"),
                        );
                        if( $db->insert("conversation", $con_data) ) {
                            $lastInsertedId = $db->lastInsertId();
                            $data = array(
                                'conv_id'=>$lastInsertedId,
                                'sent_to'=>$to_id,
                                'by_from'=>'0',
                                'message'=>"A New Job Has been Updated . Please Check Your Jobs Section.",
                                'date' => date("Y-m-d H:i:s")
                            );
                            if ( $db->insert("messages", $data) ) {
                               // print ("Success, your message has been sent"); //exit; 
                            } else {
                                //print ("Error, Try again"); //exit;
                            }
                        }
                    }
                } // for each of vendors ids

                    
    	
    }
    
    //@ centerlised controller to edit existing jobs 
    public function editAction() {
        $db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('UserSession');
	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
	
	// create zend object to handle the main http request
        $request = new Zend_Controller_Request_Http;
	
	// get the list of all parameters in query string
	$params = Zend_Controller_Front::getInstance()->getRequest()->getParams();
	
	// begin the try catch block
	try {
	    
	    $jobId = $params['id'];
	    $owner = $params['owner'];
	    $data = $db->fetchAll("select * from job where id=? and user_id=?", array($jobId,$owner), 2);
	    if( $data ) {
		$this->view->data = array('data'=>$data);
	    } else {
		print "No Job selected !! or the job you are looking for not found !!"; exit;
	    }
	    
	} catch( Exception $e ) {
	    print $e;
	}
    }
    
    //@ centerlised controller to delete jobs 
    public function delAction() {   
        $db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('UserSession');
	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
	
	// create zend object to handle the main http request
        $request = new Zend_Controller_Request_Http;
	
	try {	    
	    if( $request->isGET() ) {
		$id = $request->get('data', null );		
		$deleteJob = $db->delete('job', 'id = '.$id.'');
		if( $deleteJob ) {
		    $deleteApplier = $db->delete('appliers', 'job_id = '.$id.''); // working here delete job
		} else {
		    print("Failure, can't delete this job !"); exit;   
		}
		$this->view->data = array('data'=>$deleteApplier, 'job_appliers'=>$id, 'status'=>$deleteJob); // @ working here to do some basic operations on applier.
	    }
	    
	} catch( Exception $e ) {
	    print $e;
	}
    }
    
    // @ actions on the applier
    public function operationonapplierAction() { 
	$db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('UserSession');
	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
        
        // create zend object to handle the main http request
        $request = new Zend_Controller_Request_Http;
	try {
	    
	    if( $request->isGET() ) {
		$user_id = $UserSession->userId;
		//$operation = $request->getPost('data', null ); sendmessage_9_1
		list($type, $job, $applier) = explode("_", $request->get('data', null ));
        $job_thread ='job_'.$job;
		$data = $db->fetchAll("select * from vendor as v, user as u where v.user_id=? and v.user_id=u.id", array($applier), 2);
		$messages = $db->fetchAll("select m.id,m.conv_id,c.job_thread, m.sent_to,m.by_from,m.message,m.date,u.first_name, u.last_name,u.email from messages as m left join conversation as c on m.conv_id = c.id left join user as u on m.by_from = u.id  where  (c.job_thread = ? and m.sent_to = ? and m.by_from = ?) or (c.job_thread = ? and m.sent_to = ? and m.by_from = ?) and m.status in (0,1)", array($job_thread,$applier,$user_id,$job_thread,$user_id,$applier) , 2);
		
         if(count($messages) > 0 ){
			$jobMess = $messages;
		 }else{
			$jobMess = NULL;
		 } 
        $job_id_title = $db->fetchAll("select title from job where id=?",array($job),2);
         if($job_id_title){
            $job_title = $job_id_title;
         }else{
            $job_title = NULL;
         }

		if( $data ) {
		    $this->view->data = array('data'=>$data, 'type'=>$type, 'job'=> $job,'job_title'=>$job_title, 'messages' => $jobMess); // @ working here to do some basic operations on applier.
		} else {
		    print("Vendor not found !!"); exit;   
		}		
	    }
	    
	    if( $request->isPost() ) {
		 
		$conversation = $this->getRequest()->getPost('conversationOn');
        $applierMessage = $this->getRequest()->getPost('applierMessage');
		$withWho = $this->getRequest()->getPost('withWho');
		$job_thread = $this->getRequest()->getPost('job_thread');
		$byWho = $UserSession->userId;
		
		if( $conversation &&  $applierMessage && $withWho && $byWho ) { // check if all fields are set
		    $convo =  $this->getRequest()->getPost('conv_id');  //$db->fetchAll("select * from conversation where name=?", array($conversation), 2);
		    if( $convo ) { // check if conversation already exist
			$data = array(
				  'conv_id'=>$convo,
				  'sent_to'=>$withWho,
				  'by_from'=>$byWho,
				  'message'=>$applierMessage,
				  'date' => date("Y-m-d H:i:s")	
				  );
			if ( $db->insert("messages", $data) ) {
			    print ("Success, your message has been sent"); exit; 
			} else {
			    print ("Error, Try again"); exit;
			};
		    } else { // If it is new thread insert new conversation and fire message to user
			$data = array(
				  'name'=>$conversation,
				  'with_who'=>$withWho,
				  'by_who'=>$byWho,
				  'job_thread'=> $job_thread,
				  'date'=>date("Y-m-d H:i:s"),
				  );
			if( $db->insert("conversation", $data) ) {
			    
			    $lastInsertedId = $db->lastInsertId();
			    $data = array(
				  'conv_id'=>$lastInsertedId,
				  'sent_to'=>$withWho,
				  'by_from'=>$byWho,
				  'message'=>$applierMessage,
				  'date' => date("Y-m-d H:i:s")
				  );
			    if ( $db->insert("messages", $data) ) {
				print ("Success, your message has been sent"); exit; 
			    } else {
				print ("Error, Try again"); exit;
			    };
			}
		    }
		} else {
		    print("All fields are required !!"); exit; 
		}
	    }
	    
	} catch ( Exception $e ) {
	    print $e;
	}
    }
    
    //@ centerlised controller to edit existing jobs 
    public function jobapplicationAction() {
        $db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('VendorSession');
	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
	
	// create zend object to handle the main http request
        $request = new Zend_Controller_Request_Http;
	
	// get the list of all parameters in query string
	$params = Zend_Controller_Front::getInstance()->getRequest()->getParams();
	
	// try catch begins
	try {
	    // to display send job application form in sidebar
	    if( $request->isGET() ) {
		
		$jobId = $params['data']; 
		
		
		// check if this user has already applied for the job
		// $alreadyApplied = $db->fetchAll("select * from appliers where job_id=? and applier_id=?", array($jobId, $UserSession->userId), 2);
		// if ( $alreadyApplied ) {
		    
		//     print("You have already applied for this JOB, you can edit or reply from your dashboard if you want to !!"); exit;
		// }
		
		// Apply for this job as below
		$job_applier = $db->fetchAll("select v.user_id, jo.id, jo.title ,jo.start_on , jo.end_on , jo.budget , jo.locations , jo.address , jo.starttime ,jo.endtime from vendor as v, job as jo where v.user_id=? and jo.id=?", array($UserSession->userId, $jobId), 2);
		$appliers_qotes = $db->fetchAll("select a.*, q.id as quote_id , q.quote_content , q.quote_budget from appliers as a , quotes as q where a.job_id=? and a.applier_id=? and q.job_id=? and q.vendor_id=?", array($jobId, $UserSession->userId,$jobId, $UserSession->userId), 2);
		if( $job_applier ) {
		    $this->view->data = array('data'=>$job_applier,'quotes'=>$appliers_qotes);
		} else {
		    print ("Error, can apply for this job. You can notify admin about this  generating support ticket !!, or try again"); 
		}
	    }
	    
	    // to display send job application form in sidebar
	    if( $request->isPOST() ) {
		
		// @ get the all post variables
		$quoteContent = $this->getRequest()->getPost('quoteContent');
		$jobId = $this->getRequest()->getPost('jobId');
		$applierId = $this->getRequest()->getPost('applierId');
        $quote_budget = $this->getRequest()->getPost('budget'); 
       
		$alreadyApplied = $db->fetchAll("select * from quotes where job_id=? and vendor_id=?", array($jobId, $UserSession->userId), 2);
		if ( $alreadyApplied ) {
			
		    $data = array(
		    	'updated_on'=>date("Y-m-d H:i:s"),
		    	'quote_content'=>$quoteContent,
		    	'quote_budget'=>$quote_budget
		    	); 
           
            $n = $db->update('quotes', $data, 'id ='.$alreadyApplied[0]['id']);
            if($n){
            	print("Application has been updated, successfully"); exit;
            }
            else{
                 print ("Error, can edit for this job. You can notify admin about this  generating support ticket !!, or try again"); exit;
            }
		    //print("You have already applied for this JOB, you can edit or reply from your dashboard if you want to !!"); exit;
		}
    
		// @ check if all parameters are valid
		if( $quoteContent && $jobId && $applierId  && $quote_budget ) {
		    
		    $data = array(
				  'vendor_id'=>$applierId,
				  'job_id'=>$jobId,
				  'quote_content'=>$quoteContent,
				  'added_on'=>date("Y-m-d H:i:s"),
				  'updated_on'=>date("Y-m-d H:i:s"),
				  'quote_budget'=>$quote_budget
				  );
		    $n = $db->insert("quotes", $data);
		    if( $n ) {
			$data = array(
				      'applier_id'=>$applierId,
				      'job_id'=>$jobId,
				      'added_on'=>date("Y-m-d H:i:s"),
				      'updated_on'=>date("Y-m-d H:i:s")				      
				      );
			$application = $db->insert("appliers", $data);
			if( $application ) {
			    
			    print("Application has been sent, successfully"); exit;
			    
			}
		    }
		    
		} else {
		    print("kindly fill the required fields"); exit;
		}
		
	    }
	    
	} catch ( Exception $e ) {
	    print $e;
	}
	
    }
}
    
?>
