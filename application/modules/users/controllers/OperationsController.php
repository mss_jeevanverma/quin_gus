<?php

class Users_OperationsController extends Zend_Controller_Action
{

    public function init() {
        
        /* Initialize action controller here */
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('index', 'html')
                    ->addActionContext('callbackquote', 'html')
                    ->addActionContext('appointment', 'html')
                    ->addActionContext('favourite', 'html')
                    ->addActionContext('likes', 'html')
                    ->initContext();
    }

    public function indexAction() {
        // action body
        
    }
    
    public function callbackquoteAction() {
        
        $db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('UserSession');
	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
        
        if($this->getRequest()->isPOST()) {
            if(isset($UserSession->userId)){
	
                $data = array(
                    'user_id'        => $_POST['vendor_member'],
                    'operation_by'   => $UserSession->userId,
                    'operation_on'   => $_POST['vendor_member'],
                    'terms'          => $_POST['radiog_dark'],
                    'added_on'       => date("Y-m-d H:i:s"),
                    'updated_on'     => date("Y-m-d H:i:s")
                );  

                $insert = $db->insert('user_operations', $data);
                if($insert){
                    $eventDate = strtotime( $_POST['event_date'] );
                    $apointDate = strtotime( $_POST['appointmentDate'] );
		    $city=explode(',',$_POST['location']);
                    if($_POST['radiog_dark'] == 'quote'){
		
                        $quoteData = array(
                            'vendor_id'       => $_POST['vendor_member'],
			    'user_id'   => $UserSession->userId,
                            'cell_no'         => $_POST['cell_no'],
                            'event_date'      => date('Y-m-d',$eventDate),
                            'guests'          => $_POST['guests'],
                            'location'        => $city[1].",".$city[2],
                            'city'            => $city[0],
                            'quote_content'   => $_POST['comments'],
                            'added_on'        => date("Y-m-d H:i:s"),
                            'updated_on'      => date("Y-m-d H:i:s")
                        );
                    }else{
		        
                        $quoteData = array(
                            'vendor_id'       => $_POST['vendor_member'],
			    'user_id'   => $UserSession->userId,
                            'start_time'      => $_POST['start_time'],
                            'end_time'        => $_POST['end_time'],
                            'apoint_date'     => date('Y-m-d',$apointDate),
                            'event_date'      => date('Y-m-d',$eventDate),
                            'guests'          => $_POST['guests'],
                            'location'        => $city[1].",".$city[2],
                            'city'            => $city[0],
                            'quote_content'   => $_POST['comments'],
                            'added_on'        => date("Y-m-d H:i:s"),
                            'updated_on'      => date("Y-m-d H:i:s")
                        );
                    }
                    $quote = $db->insert('quotes', $quoteData);
                    if($quote){
                        $this->_helper->json(
                            array(
                                'message' => 'Your Quote has been sent Successfully !! ' ,
                                'resp' => 'success'
                            )
                        );
                        return;
                    }else {
                        $this->_helper->json(
                            array(
                                'message' => 'error' ,
                                'resp' => 'error'
                            )
                        );
                        return;
                    }

                }
            }
            echo "session expire";
	}
        
    }
    
    public function appointmentAction() {
        // action body
        
    }

    public function favouriteAction() {
        // print_r($_POST);die;
        $db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('UserSession');
	if( !isset( $UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
        if(isset($UserSession->userId)){
            if( $_POST['check'] == 1){
                $data = array(
                    'user_id'        => $_POST['vendor_member'],
                    'operation_by'   => $UserSession->userId,
                    'operation_on'   => $_POST['vendor_member'],
                    'terms'          => $_POST['terms'],
                    'added_on'       => date("Y-m-d H:i:s"),
                    'updated_on'     => date("Y-m-d H:i:s")
                );  
                $insert = $db->insert('user_operations', $data);
                if($insert){
                    $favoriteData = array(
                        'user_id'         => $UserSession->userId,
                        'favorite_id'     => $_POST['vendor_member'],
                        'added_on'        => date("Y-m-d H:i:s"),
                        'updated_on'      => date("Y-m-d H:i:s")
                    );
                    $favorite = $db->insert('favorites', $favoriteData);
                    if($favorite){
                        $this->_helper->json(
                            array(
                                'message' => 'added to favorites' ,
                                'resp'    => 'success',
                                'image'   => 'add'
                            )
                        );
                        return;
                    }else {
                        $this->_helper->json(
                            array(
                                'message' => 'error' ,
                                'resp' => 'error',
                                'image'   => ''
                            )
                        );
                        return;
                    }
                }
            } else {
                $deluser = $db->delete( 'user_operations', 'operation_by = '.$UserSession->userId. ' AND operation_on='. $_POST['vendor_member'] );
                if($deluser){
                    $delete = $db->delete( 'favorites', 'user_id = '.$UserSession->userId. ' AND favorite_id='. $_POST['vendor_member'] );
                    if($delete){
                        $this->_helper->json(
                            array(
                                'message' => 'removed from favorites' ,
                                'resp'    => 'success',
                                'image'   => 'remove'
                            )
                        );
                        return;
                    }else{
                        $this->_helper->json(
                            array(
                            'message' => 'added to favorite' ,
                            'resp'    => 'success',
                            'image'   => ''
                            )
                        );
                        return;
                    }
                    
                }
                
            }
        }
    }
    
    public function likesAction() {
        $db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('UserSession');
	if( !isset( $UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
        if($this->getRequest()->isGET()) {
	    $select = 'SELECT count(*) as checks FROM likes WHERE user_id =? AND liked_id = ?';
            $result1 = $db->fetchAll( $select, array($UserSession->userId , $_GET['vendor'] ) );
           
            if($result1[0]->checks != 0){
                $data = array( 'method'=>$this->getRequest()->getMethod(), 'id' => $_GET['id'], 'vendor' => $_GET['vendor'] , 'check' => 'second' );
                $this->view->data = $data;

            }else{
                $data = array( 'method'=>$this->getRequest()->getMethod(), 'id' => $_GET['id'], 'vendor' => $_GET['vendor'] , 'check' => 'first' );
                $this->view->data = $data;
            }
        }
        if($this->getRequest()->isPOST()) {
            if(isset($UserSession->userId)){
                $data = array(
                    'user_id'        => $_POST['vendor_member'],
                    'operation_by'   => $UserSession->userId,
                    'operation_on'   => $_POST['vendor_member'],
                    'terms'          => 'like',
                    'added_on'       => date("Y-m-d H:i:s"),
                    'updated_on'     => date("Y-m-d H:i:s")
                );  
                $insert = $db->insert('user_operations', $data);
                if($insert){
                    $likeData = array(
                        'user_id'         => $UserSession->userId,
                        'liked_id'        => $_POST['vendor_member'],
                        'rating'          => $_POST['rate'],
                        'added_on'        => date("Y-m-d H:i:s"),
                        'updated_on'      => date("Y-m-d H:i:s")
                    );
                    $like = $db->insert('likes', $likeData);
                    if($like){
                        $sql = 'SELECT count(*) as likew, SUM(rating) as rates FROM likes WHERE liked_id =?';
                        $result = $db->fetchAll($sql, $_POST['vendor_member']);
                        if($result[0]->likew){
                            $count = $result[0]->likew;
                        }else{
                            $count = 0;
                        }
                        $updated = array(
                                  'rating'      => $result[0]->rates/$result[0]->likew,
                                  'like'        => $count,
                                );
			$updateUser = $db->update('vendor', $updated, 'user_id ='.$_POST['vendor_member']);

                        $this->_helper->json(
                            array(
                                'message' => 'rated successfully',
                                'resp'    => 'success',
                                'count'   =>  $count
                            )
                        );
                        return;
                    }
                }
            }
        }
    }

}

