<?php

class Admin_IdeasController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        $this->_helper->layout->setLayout('admin');
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('index', 'html')
                    ->addActionContext('view', 'html')
		    ->addActionContext('media-status' , 'html')
                    ->initContext();
    }

    public function indexAction() {
        // action body
        $request = new Zend_Controller_Request_Http;
        $db=Zend_Registry::get("db");
        
        // get default session namespace
	Zend_Session::rememberMe(604800); // Week
        $sess = new Zend_Session_Namespace('Default');
        if( !isset($sess->user ) ){
            $this->_redirector->gotoSimple('index', 'login' , null );
        }
        
        if($request->isGet()) {                
            // normal get method
            try{                
                $result = $db->fetchAll("select * from media where section=? order by id DESC", array('venueidea'), 2);
                if( $result ) {                    
                    $this->view->data = array('pages'=>$result);                    
                } else {                    
                    $this->view->data = NULL;                    
                }            
            } catch (Zend_Db_Adapter_Exception $e) {
                // perhaps a failed login credential, or perhaps the RDBMS is not running
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;        
            } catch (Zend_Exception $e) {
                // perhaps factory() failed to load the specified Adapter class
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
            } 
        }
        
        if($request->isPost()) {
            try{
                $data = array(
                    'name' => $_POST['name'],
                    'price' => $_POST['price'],
                    'duration' => $_POST['duration'],
                    'site' => $_POST['site'],
                    'pictures_for_slider' => $_POST['pictures_for_slider'],
                    'portfolio_pictures' => $_POST['portfolio_pictures'],
                    'you_tube_videos_upload' => $_POST['you_tube_videos_upload'],
                    'storage_space' => $_POST['storage_space'],
                    'extra' => $_POST['extra'],
                );
                $result = $db->fetchAll("SELECT * FROM plans where name=?", array($_POST['name']), 2);
                if( $result ) {
                    $n = $db->update('plans', $data, 'id='.$result[0]['id'].'');
                } else {
                    $n = $db->insert('plans', $data);
                }
            } catch (Zend_Db_Adapter_Exception $e) {
                // perhaps a failed login credential, or perhaps the RDBMS is not running
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
        
            } catch (Zend_Exception $e) {
                // perhaps factory() failed to load the specified Adapter class
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
            }
        }
    }

    //@jeevan to add new venue idea
    public function addAction() {
	
	
    }
    
    // @mssjeevan support profile
    public function delAction() {
	
	// get default session namespace
	Zend_Session::rememberMe(604800); // Week
        $sess = new Zend_Session_Namespace('Default');
	if( !isset($sess->user ) ){
            $urlOptions = array('module'=>'admin', 'controller'=>'ideas', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }
        
        if($this->getRequest()->isGET()) {
            $request = new Zend_Controller_Request_Http;
	    // pretend this is a sophisticated database query
            try{                
                $db=Zend_Registry::get("db");
                if( $id = $request->get('id') ) {
                    $n = $db->delete('media', 'id = '.$id.'');
                    
                    if ( $n ) {
                        $this->view->data = array('data'=>'Order deleted successfully !');
                        $urlOptions = array('module'=>'admin', 'controller'=>'ideas', 'action'=>'index');
                        $this->_helper->redirector->gotoRoute($urlOptions);
                    } else {
                        $this->view->data = array('data'=>'Unable to delete order, kindly retry !');
                    }
                }                
            } catch (Exception $e ) {
                $this->view->data = array('data'=>$e);
            }
        }
	
    }

}

