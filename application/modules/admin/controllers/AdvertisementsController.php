<?php

class Admin_AdvertisementsController extends Zend_Controller_Action
{

    public function init() {        
        /* Initialize action controller here */
        $this->_helper->layout->setLayout('admin');
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('index', 'html')
                    ->addActionContext('del', 'html')
                    ->initContext();
    }
    
    
    public function indexAction() {
        
        // action body
        $request = new Zend_Controller_Request_Http;
        $db=Zend_Registry::get("db");
        
        // get default session namespace
	//Zend_Session::rememberMe(604800); // Week
        $sess = new Zend_Session_Namespace('Default');
        if( !isset($sess->user ) ){
            $this->_redirector->gotoSimple('index', 'login' , null );
        }
        
        // @ exception handler
        try {
            // @jeevan get request handler
            if( $request->isGet() ) {
               // $media = $db->fetchAll("select ads.id, ads.media_id, ads.title,m.id as ref, m.owner, m.type from advertisements as ads, media as m where m.status= ? and m.section=? and ads.media_id=m.id", array(1,'advertisement'), 2);
             //print_r($media);die;
		$media = $db->fetchAll("select * from media where section=? and status=?", array('advertisement', 1), 2);
		
if( $media ) { // if there advertisements exists in the table
                    $this->view->data = array('data'=>$media);
                } else {
                    $this->view->data = array('data'=>'');
                }
            }
            
            // @jeevan post request handler
            if( $request->isPost() ) {
		
                $id        = $this->getRequest()->getPost('advertisement'); // @ posted advertisement title
                $media_id  = $this->getRequest()->getPost('media_id'); // @ posted remote_url
          
		// @ check if advertisement and media ids exists
		if( $id && $media_id ) {
		    
		    $data = array(
				  'media_id'=>$media_id
				  );
		    if( $data ) {
			// @ set the media id for the advertisment id
			if( $db->update("advertisements", $data, "id=".$id."") ) {
			    print ( "Advertisement, updated successfully !" ); exit; 
			}
		    } else {
			print(" Error, please try again !"); exit;
		    }
		} else {		    
		    print (" Error, please try again "); exit;
		}
                
            }
            
        } catch ( Exception $e ) {
            print $e; exit;            
        }
        
    }
    
    // @jeevan add new and edit advertisements from admin panel
    public function addAction() {
        
        // action body
        $request = new Zend_Controller_Request_Http;  
        $db=Zend_Registry::get("db");
        
        // get default session namespace
        $sess = new Zend_Session_Namespace('Default');
        if( !isset($sess->user ) ){
            $this->_redirector->gotoSimple('index', 'login' , null );
        }
        
        // @ exception handler to handle advertisement exceptions
        try { 
            if( $request->isGet() ) {
                $params = Zend_Controller_Front::getInstance()->getRequest()->getParams(); // @ query string variable to edit the existing advertisement

		$id = $params['id'];
                if( $id ) {       
                    // @fetch the advertisement in edit mode
                    $advertisement = $db->fetchAll("select * from advertisements where id=?", array($id), 2);

                    if( $advertisement ) {
                        //@ forward add data to view
                        $this->view->data = array('data'=>$advertisement);
                    }
                } else {
                    //@ forward add data to view
                    $this->view->data = array('data'=>'');
                }
            }
            
            if( $request->isPost() ) { // @the post request handler
                
                $title       	= $this->getRequest()->getPost('title'); // @ posted advertisement title
                $remote_url  	= $this->getRequest()->getPost('remote_url'); // @ posted remote_url
                $description 	= $this->getRequest()->getPost('description'); // @ posted advertisement description
                $add_code    	= $this->getRequest()->getPost('add_code'); // @ posted add_code
                $type        	= $this->getRequest()->getPost('type'); // @ posted advertisement type
                $company_info        = $this->getRequest()->getPost('company_info'); // @ posted advertisement type
                
                // @server side validation
                if( $title && $remote_url && $description && $add_code && $type ) {
                    
                    $id      = $this->getRequest()->getPost('add_id'); // @ posted advertisement type 
                    if( $id ) {
                        // @ if need to update new advertisement
                        $data = array(
                                      'title'=>$title,
					'media_id'=>$media_id,
                                      'remote_url'=>$remote_url,
                                      'description'=>$description,
                                      'add_code'=>$add_code,
                                      'type'=>$type,
				      'company_info'=>$company_info,
                                      'added_on'=>date("Y-m-d H:i:s"),
                                      'updated_on'=>date("Y-m-d H:i:s")
                                      );
                        if( $db->update("advertisements",$data,'id='.$id.'') ) {
                            print ("Success, advertisement updated successfully !!"); exit;
                        } else {
                            print("Error, please try again !!"); exit;
                        }
                    } else {
                        // @ if need to add new advertisement
                        $data = array(
                                      'title'=>$title,
					'media_id'=>$media_id,
                                      'remote_url'=>$remote_url,
                                      'description'=>$description,
                                      'add_code'=>$add_code,
                                      'type'=>$type,
                                      'added_on'=>date("Y-m-d H:i:s"),
				      'updated_on'=>date("Y-m-d H:i:s")
                                      );
                        if( $db->insert("advertisements", $data) ){
                            print ("Success, advertisement added successfully !!"); exit;
                        } else {
                            print("Error, please try again !!"); exit;
                        }
                    }                    
                    
                } else {
                    print "Kindly fill out all the ewquired fields !!"; die;
                }
                
            }
            
        } catch( Exception $e ){
            print $e; exit;   
        }
        
    }
    
    // @jeevan delete advertisements from admin panel
    public function delAction() {
        
        // get default session namespace
        $sess = new Zend_Session_Namespace('Default');
	if( !isset($sess->user ) ){
            $urlOptions = array('module'=>'admin', 'controller'=>'login', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }
        
        if($this->getRequest()->isGET()) {
            $request = new Zend_Controller_Request_Http;
	    // pretend this is a sophisticated database query
	    
	     
	    // @delete advertisement from table handler
            try{                
                $db=Zend_Registry::get("db");
                if( $id = $_GET['id'] ) {
		    
		    // @ to delte advertisement from database
                    $n = $db->delete('advertisements', 'id = '.$id.'');
                    
		    // @ check if advertisement deltted or not
                    if ( $n ) {
                        $this->view->data = array('data'=>'Order deleted successfully !');
                        $urlOptions = array('module'=>'admin', 'controller'=>'advertisements', 'action'=>'');
                        $this->_helper->redirector->gotoRoute($urlOptions);
                    } else {
                        $this->view->data = array('data'=>'Unable to delete order, kindly retry !');
                    }
                }                
            } catch (Exception $e ) {
                $this->view->data = array('data'=>$e);
            }
        }
    }
}    
?>
