<?php

class Admin_OrdersController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
	$this->_helper->layout->setLayout('admin');
	$this->_redirector = $this->_helper->getHelper('Redirector');
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
	$ajaxContext->addActionContext('ov', 'html')
	            ->addActionContext('oe', 'html')
		    //->addActionContext('od', 'html')
	            ->initContext();
    }

    public function indexAction()
    {
        // action body
        // get default session namespace
	Zend_Session::rememberMe(604800); // Week
        $sess = new Zend_Session_Namespace('Default');
	
	// check if user is not logged in
	if( !isset($sess->user ) ){
            $urlOptions = array('module'=>'admin', 'controller'=>'login', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }
        if($this->getRequest()->isGET()) {
                $request = new Zend_Controller_Request_Http;
		// pretend this is a sophisticated database query
		try {
		    $db=Zend_Registry::get("db");
                    if( $id = $request->get('id') ) {
                        
                        $result = $db->fetchAll("SELECT * FROM vendor where id=?", array($id), 2);
                        $vendor = $db->fetchAll("SELECT * FROM user where email=?", array($result[0]['email']), 2);
                        if ( !$result ) {
                            print("User with this email and password does not exist !");
                        } else {
                            $plan = $db->fetchAll("SELECT * FROM plans where id=?", array($result[0]['plan_id']), 2);
                            $transaction = $db->fetchAll("SELECT * FROM transaction where vendor_id=?", array($id), 2);
                            $this->view->data = array('params'=>$id,'data'=>$result, 'vendor'=>$vendor, 'plan'=>$plan, 'transaction'=>$transaction);
                        }
                        
                    } else {
                        $result = $db->fetchAll("SELECT * FROM vendor Order By id DESC", array(), 2);
                        if ( !$result ) {
                            print("User with this email and password does not exist !");
                        } else {
                            $this->view->data = $result;
                        }
                    }
		} catch (Exception $e) {
		        // handle exceptions yourself
		        echo $e;
		}
	}
        if($this->getRequest()->isPOST()) {
                $request = new Zend_Controller_Request_Http;
		// pretend this is a sophisticated database query
		try {
		    $db=Zend_Registry::get("db");
                    if( $id = $request->get('id') ) {
                        $accountStatus = $this->getRequest()->getPost('account-status', null);
                        $data = array(
                                'status'      => $accountStatus
                        );
                        $result = $db->fetchAll("SELECT * FROM user where id=?", array($id), 2);
                        if ( $result ) {                            
                            $n = $db->update('user', $data, 'id = '.$id.'');
                            if ($n) {
                                //echo "Database updated"; die;
                                $tr = new Zend_Mail_Transport_Sendmail('-freturn_to_me@example.com');
                                Zend_Mail::setDefaultTransport($tr);
                                $mail = new Zend_Mail();
                                $mail->setBodyHtml('Your Account status has been changed to: '.$result[0]['status'].' !!!');
                                $mail->setFrom('admin.quinceanera@gmail.com', 'Email from quinceanera.');
                                $mail->addTo($result[0]['email'], '');
                                $mail->setSubject('quinceanera password updated');
                                $mail->send();
                                echo "<div id='notification' class='alert alert-danger'>";
                                    echo "Account Status updated successfully.";
                                echo "</div>";
                            } else {
                                echo "<div id='notification' class='alert alert-danger'>";
                                    echo mysql_error();
                                echo "</div>";
                            }                            
                        } else {                            
                            echo "<div id='notification' class='alert alert-danger'>";
                                echo "User not found !!!!";
                            echo "</div>";
                        }
                    } else {
                        
                    }
		} catch (Exception $e) {
		        // handle exceptions yourself
		        echo $e;
		}
	}
    }
    
    // @mssjeevan support profile
    public function odAction() {
	// get default session namespace
	Zend_Session::rememberMe(604800); // Week
        $sess = new Zend_Session_Namespace('Default');
	if( !isset($sess->user ) ){
            $this->_redirector->gotoSimple('index', 'login' , null );
        }        
        if($this->getRequest()->isGET()) {
            $request = new Zend_Controller_Request_Http;
	    // pretend this is a sophisticated database query
            try{                
                $db=Zend_Registry::get("db");
                if( $id = $request->get('id') ) {
                    $n = $db->delete('vendor', 'id = '.$id.'');
		    if( $n ) {
			$urlOptions = array('module'=>'admin', 'controller'=>'orders', 'action'=>'index');
			$this->_helper->redirector->gotoRoute($urlOptions);
		    }
                }                
            } catch (Exception $e ) {
                $this->view->data = array('data'=>$e);
            }
        }	
    }
}

