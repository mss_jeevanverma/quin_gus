<?php

class Admin_IndexController extends Zend_Controller_Action
{

    protected $_redirector = null;
    public function init() {	
        /* Initialize action controller here */
	$this->_helper->layout->setLayout('admin');
	$this->_redirector = $this->_helper->getHelper('Redirector');
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
	$ajaxContext->addActionContext('avp', 'html')
	            ->addActionContext('aep', 'html')
		    ->addActionContext('as', 'html')
	            ->initContext();
    }

    // @mssjeevan load admin dashboard
    public function indexAction() {
	$db=Zend_Registry::get("db");
        $sess = new Zend_Session_Namespace('Default');
	
	// check if user is not logged in
	if( !isset($sess->user ) ) {
            $this->_redirector->gotoSimple('index', 'login' , null );
        }
	$select = $db->select();
	$select->from(array('u' => 'user'), array('email', 'first_name', 'last_name'))
	       ->joinInner(array('m' => 'messages'), 'm.by_from = u.id  AND m.sent_to='.$sess->user.' AND m.status= 0 group by conv_id', array('by_from', 'message', 'date', 'status', 'conv_id', 'COUNT(*) as count' ));
	$unseenMessages = $db->fetchAll($select);
	
	$bookingCount = $db->fetchAll("SELECT COUNT(*) as bookingCount FROM bookings", '', 2);
	$OrdersCount = $db->fetchAll("SELECT COUNT(*) as orderCount FROM vendor", '', 2);

	if ( $bookingCount ){
		$counter = $bookingCount[0]['bookingCount'];
	}else{
		$counter = NULL;
	}
	if ( $unseenMessages ) {
		$mess = $unseenMessages;     
	} else {
		$mess = NULL;
        }
	if( $OrdersCount ) {
		$Order = $OrdersCount[0]['orderCount'];
	}else{
		$Order = NULL;
	}
	$this->view->data = array( 'messages'=> $mess, 'bookingCount' => $counter , 'OrderCount' => $Order);
    }

    // @mssjeevan view admin profile
    public function avpAction() {
	
	// get default session namespace
	Zend_Session::rememberMe(604800); // Week
        $sess = new Zend_Session_Namespace('Default');
	if( !isset($sess->user ) ){
            $this->_redirector->gotoSimple('index', 'login' , null );
        }
	
	if($this->getRequest()->isGET()) {

		// pretend this is a sophisticated database query
		try {
		    $db=Zend_Registry::get("db");
		    $result = $db->fetchAll("SELECT * FROM user where id=? and role=10 and admin=1", array($sess->user), 2);
		    $media = $db->fetchAll("SELECT * FROM media where owner=?", array($sess->user), 2);
		    if ( !$result ) {
		        print("User with this email and password does not exist !");
		    } else {
			$this->view->data = array('data'=>$result, 'media'=>$media);
		    }
		} catch (Exception $e) {
		        // handle exceptions yourself
		        echo $e;
		}
	}
	
    }

    // @mssjeevan edit profile
    public function aepAction() {
	
	// get default session namespace
	Zend_Session::rememberMe(604800); // Week
        $sess = new Zend_Session_Namespace('Default');
	if( !isset($sess->user ) ){
            $this->_redirector->gotoSimple('index', 'login' , null );
        }
	
	if($this->getRequest()->isGET()) {
		try {
		   
		    $db=Zend_Registry::get("db");
		    $result = $db->fetchAll("SELECT * FROM user where id=? and role=10 and admin=1", array($sess->user), 2);
		    $media = $db->fetchAll("SELECT * FROM media where owner=?", array($sess->user), 2);
		    if ( !$result ) {
		        print("User with this email and password does not exist !");
		    } else {
		        $this->view->data = array('data'=>$result, 'media'=>$media);
		    } 
		    
		} catch (Exception $e) {
		    // handle exceptions yourself
		    echo $e;
		}
	}
	
	if($this->getRequest()->isPOST()) {	    
		//$db=Zend_Registry::get("db");
		// pretend this is a sophisticated database query
		//echo $_POST['bio']; die;
		try {		   
		    $db=Zend_Registry::get("db");
		    $data = array(
                                'first_name'      => $_POST['first_name'],
                                'last_name' => $_POST['last_name'],
				'date_of_birth' => $_POST['date_of_birth'],
				'gender' => $_POST['gender'],
				'age' => $_POST['age'],
				'bio' => $_POST['bio'],
				'address' => $_POST['address'],
				'pincode' => $_POST['pincode'],
				'phone' => (int)$_POST['phonenumber'],
				'email' => $_POST['email'],
				'fb_name' => $_POST['fb_name'],
				'tw_name' => $_POST['tw_name'],
				'sky_name' => $_POST['sky_name'],
				'gplus_name' => $_POST['gplus_name'],
				'website' => $_POST['website']
                            );     
		    $n = $db->update('user', $data, 'id = '.$sess->user.'');
		    $urlOptions = array('module'=>'admin', 'controller'=>'index', 'action'=>'aep');
                    $this->_helper->redirector->gotoRoute($urlOptions); 
		    
		} catch (Exception $e) {
		    // handle exceptions yourself
		    echo $e;
		}
	}
	
    }

    // @mssjeevan support profile
    public function asAction() {
	
	// get default session namespace
	Zend_Session::rememberMe(604800); // Week
        $sess = new Zend_Session_Namespace('Default');
	if( !isset($sess->user ) ){
            $this->_redirector->gotoSimple('index', 'login' , null );
        }
	
	if($this->getRequest()->isGET()) {

		// pretend this is a sophisticated database query
    		$data = array('red','green','blue','yellow');
    		$this->view->data = $data;			

	}
	
    }

}

