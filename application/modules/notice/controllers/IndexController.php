<?php

class Notice_IndexController extends Zend_Controller_Action {
    
    public function init() {
        $this->_helper->layout->setLayout('admin');
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('notify', 'html')
                    ->initContext();
    }
    
    /**
     * Default Method 
     */     
    public function indexAction() {
        
        
    }
    
    /**
     * Main notification handler controller
     */    
    public function notifyAction() {
        try{
            $data = array();
            $db=Zend_Registry::get("db");
            Zend_Db_Table::getDefaultAdapter();
            $messages = new Zend_Db_Table('messages');
            $UserSession = Zend_Registry::get('VendorSession');
            $user_id = $UserSession->userId;

            $messageNotifications   = $db->fetchAll("SELECT * FROM messages as m, media as me, user as u WHERE m.sent_to =? and m.status =? and ( me.section=? and me.owner=m.by_from or u.id=m.by_from ) GROUP BY m.id", array($user_id, 0,'user-avatar'), 2);

            $systemNotifications    = $db->fetchAll("SELECT * FROM messages as m WHERE m.sent_to =? and m.status =? and m.by_from=?", array( $user_id, 0, 0 ), 2);
           for ($i=0;$i<count($messageNotifications);$i++){
                $messageNotifications[$i]['date']=date("m-d-Y h:i:s",strtotime($messageNotifications[$i]['date']));
           }
            $data['messageNotifications']   = $messageNotifications;
            $data['systemNotifications']    = $systemNotifications;
           
            $data = json_encode($data);
            die($data);
        } catch (Zend_Db_Adapter_Exception $e) {
            // perhaps a failed login credential, or perhaps the RDBMS is not running
            print $e;        
        } catch (Zend_Exception $e) {
            // perhaps factory() failed to load the specified Adapter class
            print $e;
        }
    }
}

?>