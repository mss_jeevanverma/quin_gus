<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    protected function _initDoctype()
    {
        // define views path
        $this->bootstrap('view');
        $this->bootstrap('session');
        $view = $this->getResource('view');
        $view->doctype('XHTML1_STRICT');
    }
    
        // Define routing of the application in main routing file
    protected function _initRoutes()
    {
        $router = Zend_Controller_Front::getInstance()->getRouter();
        include APPLICATION_PATH . "/configs/routing.php";
    }
    // Define method for session. 
    protected function _initSession() {

        $UserSession = new Zend_Session_Namespace('UserSession');
        $UserSession->setExpirationSeconds(864000);
        Zend_Registry::set('UserSession', $UserSession);

	$VendorSession = new Zend_Session_Namespace('VendorSession');
        $VendorSession->setExpirationSeconds(864000);
        Zend_Registry::set('VendorSession', $VendorSession);	
    }
    
    public function _initDb(){

        $resource = $this->getPluginResource("db");
        $db = $resource->getDbAdapter();
        $db->setFetchMode(Zend_Db::FETCH_OBJ);

        Zend_Db_Table_Abstract::setDefaultAdapter($db);
        Zend_Registry::set("db", $db);
    }

}

