<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>


<body <?php body_class(); ?>>
<div id="page">
<div class="mode_header">
<div class="mode_header_content">
	<div class="container_24">
<div id="header" class="grid_24 clearfix omega alpha">
                	<div id="header_left" class="grid_8 alpha">
				<div class="free_shipping">
					
				</div>
                        </div>
				<?php if ( get_header_image() ) : ?>
					<div id="header_content" class="grid_8">
						<a title="Eggthemes WeddingDay Prestashop Theme" id="header_logo"
						   href="<?php echo esc_url( home_url( '/' ) ); ?>">
							<img class="logo" alt="Eggthemes WeddingDay Prestashop Theme" src="<?php header_image(); ?>">
						</a>
					</div>
				<?php endif; ?>	
					
<div id="header_right" class="grid_8 omega">
<div id="header_user">
<ul id="header_nav">
		<li id="your_account">
						<a title="View my customer account" rel="nofollow" href="javascript:void(0);">
								<surfmarktag>My Account</surfmarktag>
						</a>
		</li>
		<li id="header_user_info">
								<surfmarktag> Welcome guest! </surfmarktag>
						<a class="login" title="Login to your customer account" rel="nofollow" href="javascript:void(0);">
								<surfmarktag>Login</surfmarktag>
						</a>
		</li>
		
<li id="shopping_cart">

</li>
</ul>
</div>


	<div id="search_block_top">
		<form id="searchbox" method="get" action="">
			<p>
			<label for="search_query_top"> </label>
			<input type="hidden" value="search" name="controller">
			<input type="hidden" value="position" name="orderby">
			<input type="hidden" value="desc" name="orderway">
			<input id="search_query_top" class="search_query ac_input" type="text" placeholder="Search" value=""
			       name="search_query" autocomplete="off">
			<input class="button" type="submit" value="Search" name="submit_search">
			</p>
		</form>
	</div>
	
<div id="cart_block" class="block exclusive">
	<h4 class="title_block">
		<a title="View my shopping cart" rel="nofollow" href="javascript:void(0);">
				<surfmarktag>Cart</surfmarktag>
		</a>
	</h4>
<div class="block_content">
	<div id="cart_block_summary" class="collapsed">
	</div>
<div id="cart_block_list" class="expanded">
		<p id="cart_block_no_products">
<div id="cart-prices">
		<p id="cart-buttons">
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<?php
		// nav menu start
?>
<div class="cs_both_mode">
	<div class="mode_megamenu">
    	<div class="container_24">
        
            <div style="display: block;" class="cs_mega_menu" id="menu">
                <ul class="ul_mega_menu">
			<li class=" menu_item menu_first level-1 parent">
				
				<!--<a class="title_menu_parent" href="<?php echo esc_url( home_url( '/' ) ); ?>">-->
				<!--<?php _e( 'Skip to content', 'twentyfourteen' ); ?></a>-->
				
				
					<surfmarktag>
						<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
					</surfmarktag>
				
                

			<!--<div class="search-toggle">-->
			<!--	<a href="#search-container" class="screen-reader-text"><?php _e( 'Search', 'twentyfourteen' ); ?></a>-->
			<!--</div>-->

			<!--<nav id="primary-navigation" class="site-navigation primary-navigation" role="navigation">-->
			<!--	<!--<button class="menu-toggle"><?php _e( 'Primary Menu', 'twentyfourteen' ); ?></button>-->
			<!--	<a class="screen-reader-text skip-link" href="#content"><?php _e( 'Skip to content', 'twentyfourteen' ); ?></a>-->
			<!--	<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>-->
			<!--</nav>-->
		</div>

	<!--
	<div id="search-container" class="search-box-wrapper hide">
			<div class="search-box">
				<?php //get_search_form(); ?>
			</div>
		</div>
	-->
	</header><!-- #masthead -->


