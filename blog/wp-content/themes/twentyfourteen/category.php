<?php
/**
 * The template for displaying Category pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header();
?>

<div class="cs_mode_contain">
<div class="mode_container">
<div class="container_24">
<div class="two_column grid_24 omega alpha" id="columns">
 <div id="left_column" class="grid_4  alpha fadeInLeft animated" data-animate="fadeInLeft" data-delay="0">
	<?php
		    get_sidebar();
	?>
</div>
<div id="center_column" class="grid_20 omega fadeInRight animated" data-delay="0" data-animate="fadeInRight">
<h3 class="title_blog"><?php single_cat_title(''); ?>.</h3>


			<?php if ( have_posts() ) : ?>
		<!--<h1 class="archive-title"><?php printf( __( 'Category Archives: %s', 'twentyfourteen' ),single_cat_title( '', false ) ); ?></h1>-->
<br>
	<?php 
		// Show an optional term description.
		$term_description = term_description();
		if ( ! empty( $term_description ) ) :
			printf( '<div class="taxonomy-description">%s</div>', $term_description );
		endif;
	?>
	<?php
		// Start the Loop.
		while ( have_posts() ) : the_post();
		/*
		 * Include the post format-specific template for the content. If you want to
		 * use this in a child theme, then include a file called called content-___.php
		 * (where ___ is the post format) and that will be used instead.
		 */
		?>
		<br>
				<a title="<?php the_title(); ?>"   href=" <?php the_permalink(); ?> ">
				<?php the_post_thumbnail('', array('class' => 'alignleft')); ?>
				</a>
		<br><strong>	
		<?php
		the_title();
		?></strong>	
		<br><br>
		<?php
		$content = get_the_content();
		$trimmed_content = wp_trim_words( $content, 100, '<br><a href="'. get_permalink() .'"> ...Read More</a>' );
		echo $trimmed_content;

	?>
		<div class="author">
		<p class="comment"><br>
	<?php
		comments_number('0', '1', '%');
	?>
		<span> Comment </span></p><br>
		<p>Posted by <span>
	<?php
		the_author();
	?>
</span></p></div>
	<?php
		endwhile;
		// Previous/next page navigation.
		twentyfourteen_paging_nav();

		else :
			// If no content, include the "No posts found" template.
			get_template_part( 'content', 'none' );
		endif;
?>
</div>
</div>
</div>
</div>
</div>
</div>

<?php
get_footer();
