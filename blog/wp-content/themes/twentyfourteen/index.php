<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
	get_header(); 
?>
<div class="cs_mode_contain">
<div class="mode_container">
<div class="container_24">
<div class="two_column grid_24 omega alpha" id="columns">
<div id="left_column" class="grid_4  alpha fadeInLeft animated" data-animate="fadeInLeft" data-delay="0">
<?php
	get_sidebar();
?>
</div>
<div id="center_column" class="grid_20 omega fadeInRight animated" data-animate="fadeInRight" data-delay="0">
<h1 class="h_blog">
	<surfmarktag></surfmarktag>
</h1>
<h1>
	<surfmarktag>Welcome to our blog</surfmarktag>
</h1>
<ul id="product_list" class="product_grid">
	<?php
	// Set the page
	$paged = ( isset( $_GET['pg'] )  )? intval( $_GET['pg'] ) : 1;
	$geta=query_posts( array( 'post_type' => 'post', 'paged' => $paged, 'posts_per_page' => 6) );
	$count = 0;
	if ( have_posts() ) :
	
		// Start the Loop.
	while ( have_posts() ) : the_post();

			/*
			 * Include the post format-specific template for the content. If you want to
			 * use this in a child theme, then include a file called called content-___.php
			 * (where ___ is the post format) and that will be used instead.
			 */
?>
<li class="ajax_block_product  clearfix omega alpha">
	<div class="post_name">
	<h3><a title=" <?php the_title(); ?> "   href=" <?php the_permalink(); ?> ">
	<?php
	if (strlen($post->post_title) > 35) {
			echo substr(the_title($before = '', $after = '', FALSE), 0, 35) . '...'; } else {
				the_title();
	}
	echo "</a>"."</h3></div>";
	?>
		<div class="post_image">              
			<a title=" <?php the_title(); ?> "   href=" <?php the_permalink(); ?> ">
			<?php the_post_thumbnail('', array('class' => 'alignleft')); ?>
			</a>
		</div>
		<div class="post_description">
		<?php
		$content = get_the_content();
		$trimmed_content = wp_trim_words( $content, 40, '<a href="'. get_permalink() .'"> ...Read More</a>' );
		echo $trimmed_content;
		?>
		</div>
		<div class="author">
			<p class="comment"><?php	comments_number('0', '1', '%'); ?><span> Comment </span></p>
			<p>Posted by <span><?php   the_author();   ?></span></p>
		</div>
</li>
<?php
$count++;
endwhile;
echo "</ul>";
?>

<div class="count_post">Display <?php echo $count; ?> Aritcles</div>

<div id="pagination" class="pagination">
<ul class="pagination">
<li class="current">
<?php
   if ( $wp_query->max_num_pages > 1 ) : ?>
                        <?php for ( $i = 1; $i <= $wp_query->max_num_pages; $i ++ ) { 
                            $link = $i == 1 ? remove_query_arg( 'pg' ) : add_query_arg( 'pg', $i );
                            ?>&nbsp; &nbsp;
			<li>
                            <?php echo '<a href="' . $link . '"' . ( $i == $paged ? '' : '' ) . '><span>' .$i . '</span> </a>'; ?>
			</li>
				<?php
                        } 
	 
?>
</li>
</ul>
</div>
<?php endif ?>
<?php
else :
		// If no content, include the "No posts found" template.
		get_template_part( 'content', 'none' );
endif;

?>
</div>
</div>
</div>
</div>
</div>
<?php
get_footer();