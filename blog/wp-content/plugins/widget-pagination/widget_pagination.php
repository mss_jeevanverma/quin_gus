<?php
/*
Plugin Name: Widget Paginator
Plugin URI: http://wgpag.jana-sieber.de/
Description: This plugin lets you add a styleable pagination for the widgets Archives, Categories, Links, Meta, Pages, Recent Posts and Recent Comments.
Version: 0.8
Author: [jasie] and [larsu]
Author URI: http://wordpress.org/support/profile/jasie
License: GPL2
Text Domain: widget_pagination
Domain Path: /lang
*/

/*  Copyright 2011-2012  Jana Sieber <jana@jana-sieber.de>
						 Lars Uebernickel <lars@uebernic.de>

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License, version 2, as
	published by the Free Software Foundation.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
?>
<?php
define("WGPAG_VERSION", "0.8");

if (!class_exists('WidgetPagination')) {

	class WidgetPagination {

		private $widgets = array(
			'widget_links'           => 'Links',
			'widget_categories'      => 'Categories',
			'widget_archive'         => 'Archive',
			'widget_recent_entries'  => 'Recent Posts',
			'widget_recent_comments' => 'Recent Comments',
			'widget_meta'            => 'Meta',
			'widget_pages'           => 'Pages');

		private $styles = array(
			'color'            => 'Text Colour',
			'border-color'     => 'Border Colour',
			'background-color' => 'Background Colour',
			'font-size'        => 'Font Size');

		function __construct() {

			// In dashboard
			if ( is_admin() ) {
				add_action('admin_init', array($this, 'wgpag_admin_init')); //register_setting
				add_action('admin_init', array($this, 'wgpag_load_plugin_textdomain'));
				add_action('admin_menu', array($this, 'wgpag_admin_menu'));
				register_activation_hook(__FILE__, array(&$this, 'initialize_defaults')); //registers a plugin function to be run when the plugin is activated

				// Widget Pagination update options
				if ( isset($_POST['wgpag']) && $_POST['wgpag']=='true' ) {

					$options = get_option('wgpag_options');

					// Update db options with the matching post value
					foreach ($_POST as $name => $value) {
						if (preg_match('/^wgpag-(items_per_page_.*)/', $name, $matches))
								$options[$matches[1]] = $value;
						else if (preg_match('/^wgpag-(item_style_.*)/', $name, $matches))
								$options[$matches[1]] = $value;
						else if (preg_match('/^wgpag-(cur_item_style_.*)/', $name, $matches))
								$options[$matches[1]] = $value;
						else if (preg_match('/^wgpag-(hover_item_style_.*)/', $name, $matches))
								$options[$matches[1]] = $value;
					}

					if (!empty($_POST['wgpag-list_item_style']))
						$options['list_item_style'] = $_POST['wgpag-list_item_style'];

					if (!empty($_POST['wgpag-pag_option_ptsh']))
						$options['pag_option_ptsh'] = $_POST['wgpag-pag_option_ptsh'];
					if (!empty($_POST['wgpag-pag_option_prev']))
						$options['pag_option_prev'] = $_POST['wgpag-pag_option_prev'];
					if (!empty($_POST['wgpag-pag_option_next']))
						$options['pag_option_next'] = $_POST['wgpag-pag_option_next'];
					if (!empty($_POST['wgpag-pag_option_prevnext_threshold']))
						$options['pag_option_prevnext_threshold'] = $_POST['wgpag-pag_option_prevnext_threshold'];
					if (!empty($_POST['wgpag-pag_option_hor_align']))
						$options['pag_option_hor_align'] = $_POST['wgpag-pag_option_hor_align'];
					if (isset($_POST['wgpag-pag_option_margin_top']))
						$options['pag_option_margin_top'] = $_POST['wgpag-pag_option_margin_top'];
					if (isset($_POST['wgpag-pag_option_margin_bottom']))
						$options['pag_option_margin_bottom'] = $_POST['wgpag-pag_option_margin_bottom'];
					if (isset($_POST['wgpag-pag_option_autoscroll_speed']))
						$options['pag_option_autoscroll_speed'] = $_POST['wgpag-pag_option_autoscroll_speed'];

					update_option('wgpag_options', $options);
				}
			}

			// Not in dashboard
			if ( !is_admin() ) {
				add_action('init', array($this, 'wgpag_page_init') );
			}
		}

		/**
		 * Initialize defaults
		 * @author Jana Sieber
		 */
		function initialize_defaults() {
			$options = array();

			// Elements per page
			foreach ($this->widgets as $w => $name)
				$options['items_per_page_' . $w] = '';

			// Pagination options
			$options['pag_option_ptsh'] = '7';
			$options['pag_option_prev'] = '<';
			$options['pag_option_next'] = '>';
			$options['pag_option_hor_align']     = 'center';
			$options['pag_option_margin_top']    = '0.5em';
			$options['pag_option_margin_bottom'] = '';
			$options['pag_option_autoscroll_speed'] = 0;

			// Styling options
			foreach ($this->styles as $s => $name) {
				$options['item_style_' . $s]       = '';
				$options['cur_item_style_' . $s]   = '';
				$options['hover_item_style_' . $s] = '';
			}
			$options['item_style_border-color']         = '#F1F1F1';
			$options['cur_item_style_background-color'] = '#F1F1F1';
			$options['list_item_style']                 = '';

			// override defaults with potential old options
			$current_options = get_option('wgpag_options');
			if ($current_options)
				$options = array_merge($options, $current_options);

			// ... and options from a previous release
			$old_option_names = array(
				'wgpag-items_per_page_links',
				'wgpag-items_per_page_categories',
				'wgpag-items_per_page_archive',
				'wgpag-items_per_page_archives',
				'wgpag-items_per_page_recent_entries',
				'wgpag-items_per_page_recent_comments',
				'wgpag-items_per_page_meta',
				'wgpag-items_per_page_pages',
				'wgpag-items_per_page_widget_links',
				'wgpag-items_per_page_widget_categories',
				'wgpag-items_per_page_widget_archive',
				'wgpag-items_per_page_widget_archives',
				'wgpag-items_per_page_widget_recent_entries',
				'wgpag-items_per_page_widget_recent_comments',
				'wgpag-items_per_page_widget_meta',
				'wgpag-items_per_page_widget_pages',
				'wgpag-pag_option_ptsh',
				'wgpag-pag_option_prev',
				'wgpag-pag_option_next',
				'wgpag-pag_option_hor_align',
				'wgpag-pag_option_margin_top',
				'wgpag-pag_option_margin_bottom',
				'wgpag-cur_item_style_color',
				'wgpag-cur_item_style_border-color',
				'wgpag-cur_item_style_background-color',
				'wgpag-cur_item_style_font-size',
				'wgpag-item_style_color',
				'wgpag-item_style_border-color',
				'wgpag-item_style_background-color',
				'wgpag-item_style_font-size',
				'wgpag-hover_item_style_color',
				'wgpag-hover_item_style_border-color',
				'wgpag-hover_item_style_background-color',
				'wgpag-hover_item_style_font-size',
				'wgpag-list_item_style');

			foreach ($old_option_names as $option_name) {
				$val = get_option($option_name);
				if ($val !== false) {
					$options[substr($option_name, 6)] = $val;
					delete_option($option_name);
				}
			}

			update_option('wgpag_options', $options);
		}

		/**
		 * Initialize WgPag
		 * @author Jana Sieber
		 */
		function wgpag_page_init() {
			wp_register_script('wgpag', plugins_url('/js/wgpag.js',__FILE__),
							   array('jquery'), WGPAG_VERSION );
			wp_register_style('wgpag', plugins_url('/css/wgpag.css',__FILE__),
							  '', WGPAG_VERSION );

			wp_register_style('wgpag-options', plugins_url('/css/wgpag-options.css',__FILE__),
							  'wgpag' );

			add_filter('style_loader_tag', array($this, 'wgpag_replace_option_style'), 10, 2);
			wp_enqueue_style('wgpag');
			wp_enqueue_style('wgpag-options');
			wp_enqueue_script('wgpag');
			wp_enqueue_script('jquery');

			$options = get_option('wgpag_options');

			// send to js
			$widgets = array();
			foreach ($this->widgets as $w => $name) {
				$widgets[] = array(
					'selector' => '.' . $w,
					'count'    => (int)$options['items_per_page_' . $w]
				);
			}

			$options = array(
				'widgets'            => $widgets,
				'pages_to_show'      => $options['pag_option_ptsh'],
				'prev_label'         => $options['pag_option_prev'],
				'next_label'         => $options['pag_option_next'],
				'prevnext_threshold' => $options['pag_option_prevnext_threshold'],
				'hor_align'          => $options['pag_option_hor_align'],
				'autoscroll_speed'   => $options['pag_option_autoscroll_speed']);

			wp_localize_script('wgpag', 'wgpag_options',
				array('l10n_print_after' => 'wgpag_options = ' . json_encode($options) . ';'));
		}

		/**
		 * Initialize WgPag admin (register settings)
		 * @author Jana Sieber
		 */
		function wgpag_admin_init() {
			wp_register_style('colorpicker',
					plugins_url('/css/jquery.colorpicker.css',__FILE__));
			wp_register_style('wgpag-style',
					plugins_url('/css/admin.css',__FILE__),
							  '', WGPAG_VERSION );
			wp_deregister_script('colorpicker');
			wp_register_script('colorpicker',
					plugins_url('/js/jquery.colorpicker.js',__FILE__),
					array('jquery'));
			wp_register_script('wgpag-script',
					plugins_url('/js/admin.js', __FILE__),
					array('jquery','colorpicker'), WGPAG_VERSION );
			load_plugin_textdomain( 'wgpag', false, 'widget_pagination/lang' );
			//XXX register_setting( 'widget_pagination', 'new_option_name' );
		}

		/**
		 * Initialize WgPag admin panel
		 * @author Jana Sieber
		 */
		function wgpag_admin_menu() {
			$page = add_submenu_page('themes.php',
									 'Widget Paginator',
									 'Widget Paginator',
									 'manage_options',
									 __FILE__,
									 array($this, 'wgpag_options_page'));
			add_action('admin_print_styles-'.$page,  array($this, 'wgpag_admin_styles'));
			add_action('admin_print_scripts-'.$page, array($this, 'wgpag_admin_scripts'));
		}

		/**
		 * Initialize internationalisation
		 * @author Jana Sieber
		 */
		function wgpag_load_plugin_textdomain() {
			load_plugin_textdomain( 'wgpag', false, dirname( plugin_basename( __FILE__ ) ) . '/lang/');
		}

		/**
		 * Load WgPag admin styles
		 * @author Jana Sieber
		 */
		function wgpag_admin_styles() {
			wp_enqueue_style('wgpag-style');
			wp_enqueue_style('colorpicker');
		}

		/**
		 * Load WgPag admin scripts
		 * @author Jana Sieber
		 */
		function wgpag_admin_scripts() {
			wp_enqueue_script('wgpag-script');
		}

		/**
		 * Provide markup for WgPag settings page
		 * @author Jana Sieber
		 */
		function wgpag_options_page() {
			$options = get_option('wgpag_options');
			require_once ('inc/wgpag-options.php');
		}

		/**
		 *
		 * @return type
		 */
		private function item_css () {
			$selectors = array(
				'item_style' => 'a',
				'hover_item_style' => 'a:hover',
				'cur_item_style' => '.br-current'
			);
			$str = '';
			$options = get_option('wgpag_options');
			foreach($selectors as $name => $selector) {
				$str .= ".br-pagination $selector { ";
				foreach ($this->styles as $s => $title) {
					$optval = $options["${name}_$s"];
					if (!empty($optval))
						$str .= "$s: $optval; ";
				}
				$str .= "}";
			}

			$liststyle =  $options['list_item_style'];
			$str .= ".widget-container li { list-style-type: $liststyle; }";

			$horalign =  $options['pag_option_hor_align'];
			$str .= ".br-pagination { text-align: $horalign; }";

			$margintop =  $options['pag_option_margin_top'];
			$str .= ".br-pagination { margin-top: $margintop; }";

			$marginbottom =  $options['pag_option_margin_bottom'];
			$str .= ".br-pagination { margin-bottom: $marginbottom; }";

			return $str;
		}

		/**
		 * Replace wgpag-options css with an inline style which pulls
		 * the style from the options database.
		 * @author Lars Uebernickel
		 */
		function wgpag_replace_option_style ($tag, $handle) {
			if ($handle == 'wgpag-options') {
				return '<style type="text/css">' . $this->item_css() . '</style>';
			}
			else
				return $tag;
		}
	}
}

// Init Widget Pagination
$wgpag = new WidgetPagination();
?>
