<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'quinceanera');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '9&j!6c+(s!B4!1Y:_LnRv8{`BI;1&v$}AKeBnHnih[1*EgAGSr,_V4,j6`V2hl4#');
define('SECURE_AUTH_KEY',  'C}zCB9kF+}^45LqmEfrF~]q^2:64n>cSolMBN7WIOrn-wte,7!0d`N6g1J8zof{m');
define('LOGGED_IN_KEY',    'hUxh|-ub3/LIX7n=A7m:p7)#j76mCg/Tv#y5w^r7>a&+o|*|[T|bns;h)5p}X-L4');
define('NONCE_KEY',        'o-Zsq+2REgR6eHNK6kAEs>^~> AU~el>`U+L*+#U-1GYKz9_Vd@BVlb~^VVUUd/5');
define('AUTH_SALT',        '?Z}D].DkbsQ]d8_G%=mEJm)HraQD_>GX>s4uYr2z3;Sk^;6A%u/[nl@beizF-(T#');
define('SECURE_AUTH_SALT', '__H}3Ae7M,j#zXml!KxLIB~.H_{<{.B{s&Q9w`Ko?N_S61G/=$SLiiC_fM+ApC7,');
define('LOGGED_IN_SALT',   '!]q)$0S-]P>|t~.1AxpQ>hbT<=/Q%X^2}64`,D_d4*G-&ci|]_v>csmKX>NOug4h');
define('NONCE_SALT',       '6PW4w)6MF~ao2H:#%uSo]V=INJI2zrbix4: `9^{$O!!Vhkj]0>X(k>#2CQYPMWW');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

